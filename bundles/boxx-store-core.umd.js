(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/common/http'), require('@boxx/core'), require('rxjs/operators'), require('@ngrx/store'), require('@ngrx/effects'), require('rxjs')) :
    typeof define === 'function' && define.amd ? define('@boxx/store-core', ['exports', '@angular/core', '@angular/common/http', '@boxx/core', 'rxjs/operators', '@ngrx/store', '@ngrx/effects', 'rxjs'], factory) :
    (global = global || self, factory((global.boxx = global.boxx || {}, global.boxx['store-core'] = {}), global.ng.core, global.ng.common.http, global['boxx-core'], global.rxjs.operators, global['ngrx-store'], global['ngrx-effects'], global.rxjs));
}(this, (function (exports, core, http, core$1, operators, store, effects, rxjs) { 'use strict';

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation.

    Permission to use, copy, modify, and/or distribute this software for any
    purpose with or without fee is hereby granted.

    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
    REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
    AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
    INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
    LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
    OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
    PERFORMANCE OF THIS SOFTWARE.
    ***************************************************************************** */
    /* global Reflect, Promise */

    var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };

    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    var __assign = function() {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };

    function __rest(s, e) {
        var t = {};
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
            t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === "function")
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    }

    function __decorate(decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    }

    function __param(paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); }
    }

    function __metadata(metadataKey, metadataValue) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
    }

    function __awaiter(thisArg, _arguments, P, generator) {
        function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }

    function __generator(thisArg, body) {
        var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f) throw new TypeError("Generator is already executing.");
            while (_) try {
                if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
                if (y = 0, t) op = [op[0] & 2, t.value];
                switch (op[0]) {
                    case 0: case 1: t = op; break;
                    case 4: _.label++; return { value: op[1], done: false };
                    case 5: _.label++; y = op[1]; op = [0]; continue;
                    case 7: op = _.ops.pop(); _.trys.pop(); continue;
                    default:
                        if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                        if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                        if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                        if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                        if (t[2]) _.ops.pop();
                        _.trys.pop(); continue;
                }
                op = body.call(thisArg, _);
            } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
            if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
        }
    }

    function __createBinding(o, m, k, k2) {
        if (k2 === undefined) k2 = k;
        o[k2] = m[k];
    }

    function __exportStar(m, exports) {
        for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) exports[p] = m[p];
    }

    function __values(o) {
        var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
        if (m) return m.call(o);
        if (o && typeof o.length === "number") return {
            next: function () {
                if (o && i >= o.length) o = void 0;
                return { value: o && o[i++], done: !o };
            }
        };
        throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
    }

    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m) return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
        }
        catch (error) { e = { error: error }; }
        finally {
            try {
                if (r && !r.done && (m = i["return"])) m.call(i);
            }
            finally { if (e) throw e.error; }
        }
        return ar;
    }

    function __spread() {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read(arguments[i]));
        return ar;
    }

    function __spreadArrays() {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
        for (var r = Array(s), k = 0, i = 0; i < il; i++)
            for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                r[k] = a[j];
        return r;
    };

    function __await(v) {
        return this instanceof __await ? (this.v = v, this) : new __await(v);
    }

    function __asyncGenerator(thisArg, _arguments, generator) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var g = generator.apply(thisArg, _arguments || []), i, q = [];
        return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
        function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
        function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
        function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
        function fulfill(value) { resume("next", value); }
        function reject(value) { resume("throw", value); }
        function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
    }

    function __asyncDelegator(o) {
        var i, p;
        return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
        function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
    }

    function __asyncValues(o) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var m = o[Symbol.asyncIterator], i;
        return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
        function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
        function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
    }

    function __makeTemplateObject(cooked, raw) {
        if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
        return cooked;
    };

    function __importStar(mod) {
        if (mod && mod.__esModule) return mod;
        var result = {};
        if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
        result.default = mod;
        return result;
    }

    function __importDefault(mod) {
        return (mod && mod.__esModule) ? mod : { default: mod };
    }

    function __classPrivateFieldGet(receiver, privateMap) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to get private field on non-instance");
        }
        return privateMap.get(receiver);
    }

    function __classPrivateFieldSet(receiver, privateMap, value) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to set private field on non-instance");
        }
        privateMap.set(receiver, value);
        return value;
    }

    var CategoryModel = /** @class */ (function () {
        function CategoryModel(data) {
            this.active = data.active;
            this.categoryImages = data.categoryImages;
            this.description = data.description;
            this.id = data.id;
            this.name = data.name;
            this.parentId = data.parentId;
            this.resourceUri = data.resourceUri;
            this.shopId = data.shopId;
            this.slug = data.slug;
        }
        CategoryModel.fromApi = function (data) {
            var categoryImages = [];
            if (Array.isArray(data.category_images)) {
                categoryImages = data.category_images;
            }
            else if (typeof data.category_images === 'object') {
                Object.keys(data.category_images).forEach(function (key) {
                    categoryImages.push(data.category_images[key].url);
                });
            }
            return new CategoryModel({
                active: data.active,
                categoryImages: categoryImages,
                description: data.description,
                id: data.id,
                name: data.name,
                parentId: data.parent_id,
                resourceUri: data.resource_uri,
                shopId: data.shop_id,
                slug: data.slug
            });
        };
        CategoryModel.empty = function () {
            return new CategoryModel({
                active: null,
                categoryImages: [],
                description: null,
                id: null,
                name: null,
                parentId: null,
                resourceUri: null,
                shopId: null,
                slug: null,
            });
        };
        return CategoryModel;
    }());

    var ImageModel = /** @class */ (function () {
        function ImageModel(data) {
            this.absolutePath = data.absolutePath;
            this.added = data.added;
            this.alt = data.alt;
            this.baseUrl = data.baseUrl;
            this.checksum = data.checksum;
            this.filesize = data.filesize;
            this.height = data.height;
            this.id = data.id;
            this.image = data.image;
            this.isGlobal = data.isGlobal;
            this.lastIncrement = data.lastIncrement;
            this.mimetype = data.mimetype;
            this.modified = data.modified;
            this.resourceUri = data.resourceUri;
            this.siteId = data.siteId;
            this.sizes = data.sizes;
            this.type = data.type;
            this.userId = data.userId;
            this.versions = data.versions;
            this.whitelabelId = data.whitelabelId;
            this.width = data.width;
        }
        ImageModel.fromApi = function (data) {
            var versions = data.versions.map(function (v) { return ({
                absolutePath: v.absolute_path,
                filesize: v.filesize,
                height: v.height,
                id: v.id,
                image: v.image,
                imageId: v.image_id,
                isGlobal: v.is_global,
                mimetype: v.mimetype,
                resourceUri: v.resource_uri,
                type: v.type,
                width: v.width,
            }); });
            return new ImageModel({
                absolutePath: data.absolute_path,
                added: data.added,
                alt: data.alt,
                baseUrl: data.base_url,
                checksum: data.checksum,
                filesize: data.filesize,
                height: data.height,
                id: data.id,
                image: data.image,
                isGlobal: data.is_global,
                lastIncrement: data.last_increment,
                mimetype: data.mimetype,
                modified: data.modified,
                resourceUri: data.resource_uri,
                siteId: data.site_id,
                sizes: data.sizes,
                type: data.type,
                userId: data.user_id,
                versions: versions,
                whitelabelId: data.whitelabel_id,
                width: data.width,
            });
        };
        ImageModel.empty = function () {
            return new ImageModel({
                absolutePath: null,
                added: null,
                alt: null,
                baseUrl: null,
                checksum: null,
                filesize: null,
                height: null,
                id: null,
                image: null,
                isGlobal: null,
                lastIncrement: null,
                mimetype: null,
                modified: null,
                resourceUri: null,
                siteId: null,
                sizes: null,
                type: null,
                userId: null,
                versions: null,
                whitelabelId: null,
                width: null,
            });
        };
        return ImageModel;
    }());

    var OrderModel = /** @class */ (function () {
        function OrderModel(data) {
            this.buyerEmail = data.buyerEmail;
            this.buyerName = data.buyerName;
            this.id = data.id;
            this.price = data.price;
            this.resourceUri = data.resourceUri;
            this.sessionId = data.sessionId;
            this.shipToAddress = data.shipToAddress;
            this.shipToCity = data.shipToCity;
            this.shipToCountry = data.shipToCountry;
            this.shipToPostalCode = data.shipToPostalCode;
            this.shipToState = data.shipToState;
            this.shippingCost = data.shippingCost;
            this.shippingId = data.shippingId;
            this.shippingName = data.shippingName;
            this.siteId = data.siteId;
            this.status = data.status;
            this.timestamp = data.timestamp;
            this.transactionId = data.transactionId;
            this.whitelabelId = data.whitelabelId;
        }
        OrderModel.fromApi = function (data) {
            return new OrderModel({
                buyerEmail: data.buyer_email,
                buyerName: data.buyer_name,
                id: data.id,
                price: data.price,
                resourceUri: data.resource_uri,
                sessionId: data.session_id,
                shipToAddress: data.ship_to_address,
                shipToCity: data.ship_to_city,
                shipToCountry: data.ship_to_country,
                shipToPostalCode: data.ship_to_postal_code,
                shipToState: data.ship_to_state,
                shippingCost: data.shipping_cost,
                shippingId: data.shipping_id,
                shippingName: data.shipping_name,
                siteId: data.site_id,
                status: data.status,
                timestamp: data.timestamp,
                transactionId: data.transaction_id,
                whitelabelId: data.whitelabel_id
            });
        };
        OrderModel.empty = function () {
            return new OrderModel({
                buyerEmail: null,
                buyerName: null,
                id: null,
                price: null,
                resourceUri: null,
                sessionId: null,
                shipToAddress: null,
                shipToCity: null,
                shipToCountry: null,
                shipToPostalCode: null,
                shipToState: null,
                shippingCost: null,
                shippingId: null,
                shippingName: null,
                siteId: null,
                status: null,
                timestamp: null,
                transactionId: null,
                whitelabelId: null
            });
        };
        return OrderModel;
    }());

    var ProductModel = /** @class */ (function () {
        function ProductModel(data) {
            this.active = data.active;
            this.added = data.added;
            this.asin = data.asin;
            this.autoStock = data.autoStock;
            this.brand = data.brand;
            this.categories = data.categories;
            this.description = data.description;
            this.excerpt = data.excerpt;
            this.height = data.height;
            this.id = data.id;
            this.image = data.image;
            this.imageId = data.imageId;
            this.length = data.length;
            this.msrpPrice = data.msrpPrice;
            this.outgoingUrl = data.outgoingUrl;
            this.price = data.price;
            this.primaryImageUrl = data.primaryImageUrl;
            this.productImages = data.productImages;
            this.published = data.published;
            this.quantity = data.quantity;
            this.resourceUri = data.resourceUri;
            this.salePrice = data.salePrice;
            this.shopId = data.shopId;
            this.sku = data.sku;
            this.slug = data.slug;
            this.status = data.status;
            this.title = data.title;
            this.upc = data.upc;
            this.variations = data.variations;
            this.vendor = data.vendor;
            this.weight = data.weight;
            this.weightUnit = data.weightUnit;
            this.wholesalePrice = data.wholesalePrice;
            this.width = data.width;
        }
        ProductModel.fromApi = function (data) {
            var categories = data.categories.map(function (c) { return CategoryModel.fromApi(c); });
            var productImages;
            if (data.product_images) {
                if (!Array.isArray(data.product_images)) {
                    productImages = data.product_images;
                }
                else {
                    data.product_images.forEach(function (img, idx) { return productImages[idx] = img; });
                }
            }
            var variations = data.variations.map(function (v) { return ProductVariationModel.fromApi(v); });
            return new ProductModel({
                active: data.active,
                added: data.added,
                asin: data.asin,
                autoStock: data.auto_stock,
                brand: data.brand,
                categories: data.categories.map(function (c) { return CategoryModel.fromApi(c); }),
                description: data.description,
                excerpt: data.excerpt,
                height: data.height,
                id: data.id,
                image: data.image ? { id: data.image.id, url: data.image.base_url + '/' + data.image.absolute_path } : null,
                imageId: data.image_id,
                length: data.length,
                msrpPrice: data.msrp_price,
                outgoingUrl: data.outgoing_url,
                price: data.price,
                primaryImageUrl: data.primary_image_url,
                productImages: productImages,
                published: data.published,
                quantity: data.quantity,
                resourceUri: data.resource_uri,
                salePrice: data.sale_price,
                shopId: data.shop_id,
                sku: data.sku,
                slug: data.slug,
                status: data.status,
                title: data.title,
                upc: data.upc,
                variations: variations,
                vendor: data.vendor,
                weight: data.weight,
                weightUnit: data.weight_unit,
                wholesalePrice: data.wholesale_price,
                width: data.width,
            });
        };
        ProductModel.empty = function () {
            return new ProductModel({
                active: null,
                added: null,
                asin: null,
                autoStock: null,
                brand: null,
                categories: null,
                description: null,
                excerpt: null,
                height: null,
                id: null,
                image: null,
                imageId: null,
                length: null,
                msrpPrice: null,
                outgoingUrl: null,
                price: null,
                primaryImageUrl: null,
                productImages: null,
                published: null,
                quantity: null,
                resourceUri: null,
                salePrice: null,
                shopId: null,
                sku: null,
                slug: null,
                status: null,
                title: null,
                upc: null,
                variations: null,
                vendor: null,
                weight: null,
                weightUnit: null,
                wholesalePrice: null,
                width: null,
            });
        };
        return ProductModel;
    }());
    var ProductVariationModel = /** @class */ (function () {
        function ProductVariationModel(data) {
            this.id = data.id;
            this.name = data.name;
            this.options = data.options;
            this.required = data.required;
            this.resourceUri = data.resourceUri;
            this.shop = data.shop;
            this.shopId = data.shopId;
        }
        ProductVariationModel.fromApi = function (data) {
            var options = data.options.map(function (o) { return ({
                id: o.id,
                name: o.name,
                order: o.order,
                price: o.price,
                variationId: o.variation_id
            }); });
            return new ProductVariationModel({
                id: data.id,
                name: data.name,
                options: options,
                required: data.required,
                resourceUri: data.resource_uri,
                shop: data.shop,
                shopId: data.shop_id
            });
        };
        ProductVariationModel.empty = function () {
            return new ProductVariationModel({
                id: null,
                name: null,
                options: null,
                required: null,
                resourceUri: null,
                shop: null,
                shopId: null
            });
        };
        return ProductVariationModel;
    }());

    var VariationModel = /** @class */ (function () {
        function VariationModel(data) {
            this.id = data.id;
            this.name = data.name;
            this.options = data.options;
            this.required = data.required;
            this.shop = data.shop;
            this.shopId = data.shopId;
        }
        VariationModel.fromApi = function (data) {
            var options = data.options.map(function (o) { return ({
                id: o.id,
                name: o.name,
                order: o.order,
                price: o.price,
                variationId: o.variation_id
            }); });
            return new VariationModel({
                id: data.id,
                name: data.name,
                options: options,
                required: data.required,
                resourceUri: data.resource_uri,
                shop: data.shop,
                shopId: data.shop_id,
            });
        };
        VariationModel.empty = function () {
            return new VariationModel({
                id: null,
                name: null,
                options: null,
                required: null,
                resourceUri: null,
                shop: null,
                shopId: null,
            });
        };
        return VariationModel;
    }());

    var STORE_REPOSITORY = new core.InjectionToken('StoreRepository');

    var StoreRepository = /** @class */ (function () {
        function StoreRepository(appSettings, httpClient) {
            this.appSettings = appSettings;
            this.httpClient = httpClient;
        }
        // --------------------- CATEGORIES: -------------------------
        StoreRepository.prototype.loadCategories = function (pagination) {
            if (pagination === void 0) { pagination = ''; }
            return this.httpClient.get(this.getBaseUrl() + "/categories" + pagination);
        };
        StoreRepository.prototype.loadCategoryById = function (id) {
            return this.httpClient.get(this.getBaseUrl() + "/categories/" + id);
        };
        StoreRepository.prototype.createCategory = function (payload) {
            var body = this.createHttParams(payload);
            return this.httpClient.post(this.getBaseUrl() + "/create_category", body);
        };
        StoreRepository.prototype.deleteCategory = function (id) {
            return this.httpClient.delete(this.getBaseUrl() + "/delete_category/" + id);
        };
        // --------------------- ORDERS: -------------------------
        StoreRepository.prototype.loadOrders = function (pagination) {
            if (pagination === void 0) { pagination = ''; }
            return this.httpClient.get(this.getBaseUrl() + "/orders" + pagination);
        };
        StoreRepository.prototype.loadOrderById = function (id) {
            return this.httpClient.get(this.getBaseUrl() + "/orders/" + id);
        };
        StoreRepository.prototype.updateOrder = function (id, payload) {
            var body = this.createHttParams(payload);
            return this.httpClient.post(this.getBaseUrl() + "/update_order/" + id, body);
        };
        // --------------------- PRODUCTS: -------------------------
        StoreRepository.prototype.loadProducts = function () {
            return this.httpClient.get(this.getBaseUrl() + "/products");
        };
        StoreRepository.prototype.loadProductById = function (id) {
            return this.httpClient.get(this.getBaseUrl() + "/products/" + id);
        };
        StoreRepository.prototype.createProduct = function (payload) {
            var body = this.createHttParams(payload);
            return this.httpClient.post(this.getBaseUrl() + "/create_product", body);
        };
        StoreRepository.prototype.updateProduct = function (id, payload) {
            var body = this.createHttParams(payload);
            return this.httpClient.put(this.getBaseUrl() + "/update_product/" + id, body);
        };
        StoreRepository.prototype.deleteProduct = function (id) {
            return this.httpClient.delete(this.getBaseUrl() + "/delete_product/" + id);
        };
        // ---------------- PRODUCT VARIATIONS: ---------------------
        StoreRepository.prototype.loadProductsVariations = function (pagination) {
            if (pagination === void 0) { pagination = ''; }
            return this.httpClient.get(this.getBaseUrl() + "/product_variations" + pagination);
        };
        StoreRepository.prototype.loadProductVariationByVariationId = function (id) {
            return this.httpClient.get(this.getBaseUrl() + "/product_variations/" + id);
        };
        StoreRepository.prototype.createProductVariation = function (payload) {
            var body = this.createHttParams(payload);
            return this.httpClient.post(this.getBaseUrl() + "/create_product_variation", body);
        };
        StoreRepository.prototype.deleteProductVariation = function (id) {
            return this.httpClient.delete(this.getBaseUrl() + "/delete_product_variation/" + id);
        };
        // --------------------- IMAGES: -------------------------
        StoreRepository.prototype.loadSiteImages = function (pagination) {
            if (pagination === void 0) { pagination = ''; }
            return this.httpClient.get(this.getBaseUrl() + "/site_images" + pagination);
        };
        StoreRepository.prototype.loadSiteImageById = function (id) {
            return this.httpClient.get(this.getBaseUrl() + "/site_images/" + id);
        };
        StoreRepository.prototype.createSiteImage = function (payload) {
            var body = this.createHttParams(payload);
            return this.httpClient.post(this.getBaseUrl() + "/create_site_image", body);
        };
        StoreRepository.prototype.deleteSiteImage = function (id) {
            return this.httpClient.delete(this.getBaseUrl() + "/delete_site_image/" + id);
        };
        StoreRepository.prototype.createHttParams = function (payload) {
            var params = new http.HttpParams();
            for (var key in payload) {
                if (payload.hasOwnProperty(key)) {
                    (typeof payload[key] === 'object') ?
                        params = params.append(key, JSON.stringify(payload[key]))
                        :
                            params = params.append(key, payload[key]);
                }
            }
            return params.toString();
        };
        StoreRepository.prototype.getBaseUrl = function () {
            return this.appSettings.baseUrl() + "/api/" + this.appSettings.instance() + "/v1/shop";
        };
        StoreRepository.ctorParameters = function () { return [
            { type: core$1.AbstractAppConfigService, decorators: [{ type: core.Inject, args: [core$1.APP_CONFIG_SERVICE,] }] },
            { type: http.HttpClient }
        ]; };
        StoreRepository = __decorate([
            core.Injectable(),
            __param(0, core.Inject(core$1.APP_CONFIG_SERVICE))
        ], StoreRepository);
        return StoreRepository;
    }());

    var STORE_SERVICE = new core.InjectionToken('StoreService');

    var StoreService = /** @class */ (function () {
        function StoreService(repository) {
            this.repository = repository;
        }
        // --------------------- CATEGORIES: -------------------------
        StoreService.prototype.loadCategories = function () {
            return this.repository.loadCategories().pipe(operators.map(function (response) { return response.data.map(function (c) { return CategoryModel.fromApi(c); }); }), operators.catchError(function (error) { throw error; }));
        };
        StoreService.prototype.loadCategoryById = function (id) {
            return this.repository.loadCategoryById(id).pipe(operators.map(function (response) { return CategoryModel.fromApi(response.data); }), operators.catchError(function (error) { throw error; }));
        };
        StoreService.prototype.createCategory = function (payload) {
            return this.repository.createCategory(payload).pipe(operators.map(function (response) { return CategoryModel.fromApi(response.data); }), operators.catchError(function (error) { throw error; }));
        };
        StoreService.prototype.deleteCategory = function (payload) {
            return this.repository.deleteCategory(payload).pipe(operators.map(function (resp) { return resp.status === 'success'; }), operators.catchError(function (error) { throw error; }));
        };
        // --------------------- ORDERS: -------------------------
        StoreService.prototype.loadOrders = function () {
            return this.repository.loadOrders().pipe(operators.map(function (response) { return response.data.map(function (o) { return OrderModel.fromApi(o); }); }), operators.catchError(function (error) { throw error; }));
        };
        StoreService.prototype.loadOrderById = function (id) {
            return this.repository.loadOrderById(id).pipe(operators.map(function (response) { return OrderModel.fromApi(response.data); }), operators.catchError(function (error) { throw error; }));
        };
        StoreService.prototype.updateOrder = function (id, payload) {
            return this.repository.updateOrder(id, payload).pipe(operators.map(function (response) { return OrderModel.fromApi(response.data); }), operators.catchError(function (error) { throw error; }));
        };
        // --------------------- PRODUCTS: -------------------------
        StoreService.prototype.loadProducts = function () {
            return this.repository.loadProducts().pipe(operators.map(function (response) { return response.data.map(function (p) { return ProductModel.fromApi(p); }); }), operators.catchError(function (error) { throw error; }));
        };
        StoreService.prototype.loadProductById = function (id) {
            return this.repository.loadProductById(id).pipe(operators.map(function (response) { return ProductModel.fromApi(response.data); }), operators.catchError(function (error) { throw error; }));
        };
        StoreService.prototype.createProduct = function (payload) {
            return this.repository.createProduct(payload).pipe(operators.map(function (response) { return ProductModel.fromApi(response.data); }), operators.catchError(function (error) { throw error; }));
        };
        StoreService.prototype.updateProduct = function (id, payload) {
            return this.repository.updateProduct(id, payload).pipe(operators.map(function (response) { return ProductModel.fromApi(response.data); }), operators.catchError(function (error) { throw error; }));
        };
        StoreService.prototype.deleteProduct = function (id) {
            return this.repository.deleteProduct(id).pipe(operators.map(function (resp) { return resp.status === 'success'; }), operators.catchError(function (error) { throw error; }));
        };
        // ---------------- PRODUCT VARIATIONS: ---------------------
        StoreService.prototype.loadProductsVariations = function () {
            return this.repository.loadProductsVariations().pipe(operators.map(function (resp) { return resp.data.map(function (v) { return VariationModel.fromApi(v); }); }), operators.catchError(function (error) { throw error; }));
        };
        StoreService.prototype.loadProductVariationById = function (id) {
            return this.repository.loadProductVariationByVariationId(id).pipe(operators.map(function (resp) { return VariationModel.fromApi(resp.data); }), operators.catchError(function (error) { throw error; }));
        };
        StoreService.prototype.createProductVariation = function (payload) {
            return this.repository.createProductVariation(payload).pipe(operators.map(function (resp) { return VariationModel.fromApi(resp.data); }), operators.catchError(function (error) { throw error; }));
        };
        StoreService.prototype.deleteProductVariation = function (id) {
            return this.repository.deleteProductVariation(id).pipe(operators.map(function (resp) { return resp.status === 'success'; }), operators.catchError(function (error) { throw error; }));
        };
        // --------------------- IMAGES: -------------------------
        StoreService.prototype.loadSiteImages = function () {
            return this.repository.loadSiteImages().pipe(operators.map(function (resp) { return resp.data.map(function (i) { return ImageModel.fromApi(i); }); }), operators.catchError(function (error) { throw error; }));
        };
        StoreService.prototype.loadSiteImageById = function (id) {
            throw new Error('Method not implemented');
        };
        StoreService.prototype.createSiteImage = function (payload) {
            throw new Error('Method not implemented');
        };
        StoreService.prototype.deleteSiteImage = function (id) {
            throw new Error('Method not implemented');
        };
        StoreService.ctorParameters = function () { return [
            { type: undefined, decorators: [{ type: core.Inject, args: [STORE_REPOSITORY,] }] }
        ]; };
        StoreService.ɵprov = core.ɵɵdefineInjectable({ factory: function StoreService_Factory() { return new StoreService(core.ɵɵinject(STORE_REPOSITORY)); }, token: StoreService, providedIn: "root" });
        StoreService = __decorate([
            core.Injectable({
                providedIn: 'root'
            }),
            __param(0, core.Inject(STORE_REPOSITORY))
        ], StoreService);
        return StoreService;
    }());


    (function (StoreImagesActionTypes) {
        StoreImagesActionTypes["LoadImagesBegin"] = "[STORE] Load Images Begin";
        StoreImagesActionTypes["LoadImagesSuccess"] = "[STORE] Load Images Success";
        StoreImagesActionTypes["LoadImagesFail"] = "[STORE] Load Images Fail";
        StoreImagesActionTypes["LoadImageByIdBegin"] = "[STORE] Load Image By Id Begin";
        StoreImagesActionTypes["LoadImageByIdSuccess"] = "[STORE] Load Image By Id Success";
        StoreImagesActionTypes["LoadImageByIdFail"] = "[STORE] Load Image By Id Fail";
        StoreImagesActionTypes["CreateImageBegin"] = "[STORE] Create Image Begin";
        StoreImagesActionTypes["CreateImageSuccess"] = "[STORE] Create Image Success";
        StoreImagesActionTypes["CreateImageFail"] = "[STORE] Create Image Fail";
        StoreImagesActionTypes["DelteImageBegin"] = "[STORE] Load Delete  Begin";
        StoreImagesActionTypes["DelteImageSuccess"] = "[STORE] Load Delete Success";
        StoreImagesActionTypes["DelteImageFail"] = "[STORE] Delete Image Fail";
        StoreImagesActionTypes["ClearImagesSuccessErrorData"] = "[STORE] Clear Images Success/Error Data";
    })(exports.StoreImagesActionTypes || (exports.StoreImagesActionTypes = {}));
    var LoadImagesBeginAction = store.createAction(exports.StoreImagesActionTypes.LoadImagesBegin);
    var LoadImagesSuccessAction = store.createAction(exports.StoreImagesActionTypes.LoadImagesSuccess, store.props());
    var LoadImagesFailAction = store.createAction(exports.StoreImagesActionTypes.LoadImagesFail, store.props());
    var LoadImageByIdBeginAction = store.createAction(exports.StoreImagesActionTypes.LoadImageByIdBegin, store.props());
    var LoadImageByIdSuccessAction = store.createAction(exports.StoreImagesActionTypes.LoadImageByIdSuccess, store.props());
    var LoadImageByIdFailAction = store.createAction(exports.StoreImagesActionTypes.LoadImageByIdFail, store.props());
    var CreateImageBeginAction = store.createAction(exports.StoreImagesActionTypes.CreateImageBegin, store.props());
    var CreateImageSuccessAction = store.createAction(exports.StoreImagesActionTypes.CreateImageSuccess, store.props());
    var CreateImageFailAction = store.createAction(exports.StoreImagesActionTypes.CreateImageFail, store.props());
    var DeleteImageBeginAction = store.createAction(exports.StoreImagesActionTypes.DelteImageBegin, store.props());
    var DeleteImageSuccessAction = store.createAction(exports.StoreImagesActionTypes.DelteImageSuccess, store.props());
    var DeleteImageFailAction = store.createAction(exports.StoreImagesActionTypes.DelteImageFail, store.props());
    var ClearImagesSuccessErrorDataAction = store.createAction(exports.StoreImagesActionTypes.ClearImagesSuccessErrorData);


    (function (StoreOrdersActionTypes) {
        StoreOrdersActionTypes["LoadOrdersBegin"] = "[STORE] Load Orders Begin";
        StoreOrdersActionTypes["LoadOrdersSuccess"] = "[STORE] Load Orders Success";
        StoreOrdersActionTypes["LoadOrdersFail"] = "[STORE] Load Orders Fail";
        StoreOrdersActionTypes["LoadOrderByIdBegin"] = "[STORE] Load Order By Id Begin";
        StoreOrdersActionTypes["LoadOrderByIdSuccess"] = "[STORE] Load Order By Id Success";
        StoreOrdersActionTypes["LoadOrderByIdFail"] = "[STORE] Load Order By Id Fail";
        StoreOrdersActionTypes["UpdateOrderBegin"] = "[STORE] Update Order Begin";
        StoreOrdersActionTypes["UpdateOrderSuccess"] = "[STORE] Update Order Success";
        StoreOrdersActionTypes["UpdateOrderFail"] = "[STORE] Update Order Fail";
        StoreOrdersActionTypes["ClearOrdersSuccessErrorData"] = "[STORE] Clear Orders Success/Error Data";
    })(exports.StoreOrdersActionTypes || (exports.StoreOrdersActionTypes = {}));
    var LoadOrdersBeginAction = store.createAction(exports.StoreOrdersActionTypes.LoadOrdersBegin);
    var LoadOrdersSuccessAction = store.createAction(exports.StoreOrdersActionTypes.LoadOrdersSuccess, store.props());
    var LoadOrdersFailAction = store.createAction(exports.StoreOrdersActionTypes.LoadOrdersFail, store.props());
    var LoadOrderByIdBeginAction = store.createAction(exports.StoreOrdersActionTypes.LoadOrderByIdBegin, store.props());
    var LoadOrderByIdSuccessAction = store.createAction(exports.StoreOrdersActionTypes.LoadOrderByIdSuccess, store.props());
    var LoadOrderByIdFailAction = store.createAction(exports.StoreOrdersActionTypes.LoadOrderByIdFail, store.props());
    var UpdateOrderBeginAction = store.createAction(exports.StoreOrdersActionTypes.UpdateOrderBegin, store.props());
    var UpdateOrderSuccessAction = store.createAction(exports.StoreOrdersActionTypes.UpdateOrderSuccess, store.props());
    var UpdateOrderFailAction = store.createAction(exports.StoreOrdersActionTypes.UpdateOrderFail, store.props());
    var ClearOrdersSuccessErrorDataAction = store.createAction(exports.StoreOrdersActionTypes.ClearOrdersSuccessErrorData);


    (function (StoreProductsActionTypes) {
        StoreProductsActionTypes["LoadProductsBegin"] = "[STORE] Load Products Begin";
        StoreProductsActionTypes["LoadProductsSuccess"] = "[STORE] Load Products Success";
        StoreProductsActionTypes["LoadProductsFail"] = "[STORE] Load Products Fail";
        StoreProductsActionTypes["LoadProductByIdBegin"] = "[STORE] Load Product By Id Begin";
        StoreProductsActionTypes["LoadProductByIdSuccess"] = "[STORE] Load Product By Id Success";
        StoreProductsActionTypes["LoadProductByIdFail"] = "[STORE] Load Product By Id Fail";
        StoreProductsActionTypes["CreateProductBegin"] = "[STORE] Create Product Begin";
        StoreProductsActionTypes["CreateProductSuccess"] = "[STORE] Create Product Success";
        StoreProductsActionTypes["CreateProductFail"] = "[STORE] Create Product Fail";
        StoreProductsActionTypes["UpdateProductBegin"] = "[STORE] Update Product Begin";
        StoreProductsActionTypes["UpdateProductSuccess"] = "[STORE] Update Product Success";
        StoreProductsActionTypes["UpdateProductFail"] = "[STORE] Update Product Fail";
        StoreProductsActionTypes["DeleteProductBegin"] = "[STORE] Delete Product Begin";
        StoreProductsActionTypes["DeleteProductSuccess"] = "[STORE] Delete Product Success";
        StoreProductsActionTypes["DeleteProductFail"] = "[STORE] Delete Product Fail";
        StoreProductsActionTypes["ClearProductsSuccessErrorData"] = "[STORE] Clear Products Success/Error Data";
    })(exports.StoreProductsActionTypes || (exports.StoreProductsActionTypes = {}));
    var LoadProductsBeginAction = store.createAction(exports.StoreProductsActionTypes.LoadProductsBegin);
    var LoadProductsSuccessAction = store.createAction(exports.StoreProductsActionTypes.LoadProductsSuccess, store.props());
    var LoadProductsFailAction = store.createAction(exports.StoreProductsActionTypes.LoadProductsFail, store.props());
    var LoadProductByIdBeginAction = store.createAction(exports.StoreProductsActionTypes.LoadProductByIdBegin, store.props());
    var LoadProductByIdSuccessAction = store.createAction(exports.StoreProductsActionTypes.LoadProductByIdSuccess, store.props());
    var LoadProductByIdFailAction = store.createAction(exports.StoreProductsActionTypes.LoadProductByIdFail, store.props());
    var CreateProductBeginAction = store.createAction(exports.StoreProductsActionTypes.CreateProductBegin, store.props());
    var CreateProductSuccessAction = store.createAction(exports.StoreProductsActionTypes.CreateProductSuccess, store.props());
    var CreateProductFailAction = store.createAction(exports.StoreProductsActionTypes.CreateProductFail, store.props());
    var UpdateProductBeginAction = store.createAction(exports.StoreProductsActionTypes.UpdateProductBegin, store.props());
    var UpdateProductSuccessAction = store.createAction(exports.StoreProductsActionTypes.UpdateProductSuccess, store.props());
    var UpdateProductFailAction = store.createAction(exports.StoreProductsActionTypes.UpdateProductFail, store.props());
    var DeleteProductBeginAction = store.createAction(exports.StoreProductsActionTypes.DeleteProductBegin, store.props());
    var DeleteProductSuccessAction = store.createAction(exports.StoreProductsActionTypes.DeleteProductSuccess, store.props());
    var DeleteProductFailAction = store.createAction(exports.StoreProductsActionTypes.DeleteProductFail, store.props());
    var ClearProductsSuccessErrorDataAction = store.createAction(exports.StoreProductsActionTypes.ClearProductsSuccessErrorData);


    (function (StoreVariationsActionTypes) {
        StoreVariationsActionTypes["LoadVariationsBegin"] = "[STORE] Load Variations Begin";
        StoreVariationsActionTypes["LoadVariationsSuccess"] = "[STORE] Load Variations Success";
        StoreVariationsActionTypes["LoadVariationsFail"] = "[STORE] Load Variations Fail";
        StoreVariationsActionTypes["LoadVariationByIdBegin"] = "[STORE] Load Variation By Id Begin";
        StoreVariationsActionTypes["LoadVariationByIdSuccess"] = "[STORE] Load Variation By Id Success";
        StoreVariationsActionTypes["LoadVariationByIdFail"] = "[STORE] Load Variation By Id Fail";
        StoreVariationsActionTypes["CreateVariationBegin"] = "[STORE] Create Variation Begin";
        StoreVariationsActionTypes["CreateVariationSuccess"] = "[STORE] Create Variation Success";
        StoreVariationsActionTypes["CreateVariationFail"] = "[STORE] Create Variation Fail";
        StoreVariationsActionTypes["DeleteVariationBegin"] = "[STORE] Delete Variation Begin";
        StoreVariationsActionTypes["DeleteVariationSuccess"] = "[STORE] Delete Variation Success";
        StoreVariationsActionTypes["DeleteVariationFail"] = "[STORE] Delete Variation Fail";
        StoreVariationsActionTypes["ClearVariationsSuccessErrorData"] = "[STORE] Clear Variations Success/Error Data";
    })(exports.StoreVariationsActionTypes || (exports.StoreVariationsActionTypes = {}));
    var LoadVariationsBeginAction = store.createAction(exports.StoreVariationsActionTypes.LoadVariationsBegin);
    var LoadVariationsSuccessAction = store.createAction(exports.StoreVariationsActionTypes.LoadVariationsSuccess, store.props());
    var LoadVariationsFailAction = store.createAction(exports.StoreVariationsActionTypes.LoadVariationsFail, store.props());
    var LoadVariationByIdBeginAction = store.createAction(exports.StoreVariationsActionTypes.LoadVariationByIdBegin, store.props());
    var LoadVariationByIdSuccessAction = store.createAction(exports.StoreVariationsActionTypes.LoadVariationByIdSuccess, store.props());
    var LoadVariationByIdFailAction = store.createAction(exports.StoreVariationsActionTypes.LoadVariationByIdFail, store.props());
    var CreteVariationBeginAction = store.createAction(exports.StoreVariationsActionTypes.CreateVariationBegin, store.props());
    var CreteVariationSuccessAction = store.createAction(exports.StoreVariationsActionTypes.CreateVariationSuccess, store.props());
    var CreteVariataionFailAction = store.createAction(exports.StoreVariationsActionTypes.CreateVariationFail, store.props());
    var DelteVariationBeginAction = store.createAction(exports.StoreVariationsActionTypes.DeleteVariationBegin, store.props());
    var DelteVariationSuccessAction = store.createAction(exports.StoreVariationsActionTypes.DeleteVariationSuccess, store.props());
    var DelteVariationFailAction = store.createAction(exports.StoreVariationsActionTypes.DeleteVariationFail, store.props());
    var ClearVariationsSuccessErrorDataAction = store.createAction(exports.StoreVariationsActionTypes.ClearVariationsSuccessErrorData);


    (function (StoreCategoriesActionTypes) {
        StoreCategoriesActionTypes["LoadCategoriesBegin"] = "[STORE] Load Categories Begin";
        StoreCategoriesActionTypes["LoadCategoriesSuccess"] = "[STORE] Load Categories Success";
        StoreCategoriesActionTypes["LoadCategoriesFail"] = "[STORE] Load Categories Fail";
        StoreCategoriesActionTypes["LoadCategoryByIdBegin"] = "[STORE] Load LoadCategory By Id Begin";
        StoreCategoriesActionTypes["LoadCategoryByIdSuccess"] = "[STORE] Load Categorie By Id Success";
        StoreCategoriesActionTypes["LoadCategoryByIdFail"] = "[STORE] Load Categorie By Id Fail";
        StoreCategoriesActionTypes["CreateCategoryBegin"] = "[STORE] Create Category Begin";
        StoreCategoriesActionTypes["CreateCategorySuccess"] = "[STORE] Create Category Success";
        StoreCategoriesActionTypes["CreateCategoryFail"] = "[STORE] Create Category Fail";
        StoreCategoriesActionTypes["DeleteCategoryBegin"] = "[STORE] Delete Category Begin";
        StoreCategoriesActionTypes["DeleteCategorySuccess"] = "[STORE] Delete Category Success";
        StoreCategoriesActionTypes["DeleteCategoryFail"] = "[STORE] Delete Category Fail";
        StoreCategoriesActionTypes["ClearCategoriesSuccessErrorData"] = "[STORE] Clear Categories Success/Error Data";
    })(exports.ɵa || (exports.ɵa = {}));
    var LoadCategoriesBeginAction = store.createAction(exports.ɵa.LoadCategoriesBegin);
    var LoadCategoriesSuccessAction = store.createAction(exports.ɵa.LoadCategoriesSuccess, store.props());
    var LoadCategoriesFailAction = store.createAction(exports.ɵa.LoadCategoriesFail, store.props());
    var LoadCategorieByIdBeginAction = store.createAction(exports.ɵa.LoadCategoryByIdBegin, store.props());
    var LoadCategorieByIdSuccessAction = store.createAction(exports.ɵa.LoadCategoryByIdSuccess, store.props());
    var LoadCategorieByIdFailAction = store.createAction(exports.ɵa.LoadCategoryByIdFail, store.props());
    var CreateCategoryBeginAction = store.createAction(exports.ɵa.CreateCategoryBegin, store.props());
    var CreateCategorySuccessAction = store.createAction(exports.ɵa.CreateCategorySuccess, store.props());
    var CreateCategoryFailAction = store.createAction(exports.ɵa.CreateCategoryFail, store.props());
    var DeleteCategoryBeginAction = store.createAction(exports.ɵa.DeleteCategoryBegin, store.props());
    var DeleteCategorySuccessAction = store.createAction(exports.ɵa.DeleteCategorySuccess, store.props());
    var DeleteCategoryFailAction = store.createAction(exports.ɵa.DeleteCategoryFail, store.props());
    var ClearCategoriesSuccessErrorDataAction = store.createAction(exports.ɵa.ClearCategoriesSuccessErrorData);

    var getStoreState = store.createFeatureSelector('store');
    var ɵ0 = function (state) { return state; };
    // ------- STORE GLOBAL STATE:
    var getStorePageState = store.createSelector(getStoreState, ɵ0);
    var ɵ1 = function (state) { return state.categories; };
    // --------------------- CATEGORIES: -------------------------
    var getStoreCategoriesState = store.createSelector(getStorePageState, ɵ1);
    var ɵ2 = function (categories) { return categories.isLoading; };
    var getCategoriesIsloading = store.createSelector(getStoreCategoriesState, ɵ2);
    var ɵ3 = function (categories) { return categories.data; };
    var getCategoriesData = store.createSelector(getStoreCategoriesState, ɵ3);
    var ɵ4 = function (categories) { return categories.hasBeenFetched; };
    var getCategoriesHasBeenFetched = store.createSelector(getStoreCategoriesState, ɵ4);
    var ɵ5 = function (categories) { return categories.error; };
    var getCategoriesError = store.createSelector(getStoreCategoriesState, ɵ5);
    var ɵ6 = function (categories) { return categories.success; };
    var getCategoriesSuccess = store.createSelector(getStoreCategoriesState, ɵ6);
    var ɵ7 = function (state) { return state.orders; };
    // --------------------- ORDERS: -------------------------
    var getStoreOrdersState = store.createSelector(getStorePageState, ɵ7);
    var ɵ8 = function (orders) { return orders.isLoading; };
    var getOrdersIsLoading = store.createSelector(getStoreOrdersState, ɵ8);
    var ɵ9 = function (orders) { return orders.data; };
    var getOrdersData = store.createSelector(getStoreOrdersState, ɵ9);
    var ɵ10 = function (orders) { return orders.hasBeenFetched; };
    var getOrdersHasBeenFetched = store.createSelector(getStoreOrdersState, ɵ10);
    var ɵ11 = function (orders) { return orders.error; };
    var getOrdersError = store.createSelector(getStoreOrdersState, ɵ11);
    var ɵ12 = function (orders) { return orders.success; };
    var getOrdersSuccess = store.createSelector(getStoreOrdersState, ɵ12);
    var ɵ13 = function (state) { return state.products; };
    // --------------------- PRODUCTS: -------------------------
    var getStoreProductsState = store.createSelector(getStorePageState, ɵ13);
    var ɵ14 = function (products) { return products.isLoading; };
    var getStoreProductsIsLoading = store.createSelector(getStoreProductsState, ɵ14);
    var ɵ15 = function (products) { return products.data; };
    var getStoreProductsData = store.createSelector(getStoreProductsState, ɵ15);
    var ɵ16 = function (products) { return products.hasBeenFetched; };
    var getStoreProductsHasBeenFetched = store.createSelector(getStoreProductsState, ɵ16);
    var ɵ17 = function (products) { return products.error; };
    var getStoreProductsError = store.createSelector(getStoreProductsState, ɵ17);
    var ɵ18 = function (products) { return products.success; };
    var getStoreProductsSuccess = store.createSelector(getStoreProductsState, ɵ18);
    var ɵ19 = function (state) { return state.variations; };
    // --------------------- VARIATIONS: -------------------------
    var getStoreVariationsState = store.createSelector(getStorePageState, ɵ19);
    var ɵ20 = function (variations) { return variations.isLoading; };
    var getStoreVariationsIsLoading = store.createSelector(getStoreVariationsState, ɵ20);
    var ɵ21 = function (variations) { return variations.data; };
    var getStoreVariationsData = store.createSelector(getStoreVariationsState, ɵ21);
    var ɵ22 = function (variations) { return variations.hasBeenFetched; };
    var getStoreVariationsHasBeenFetched = store.createSelector(getStoreVariationsState, ɵ22);
    var ɵ23 = function (variations) { return variations.error; };
    var getStoreVariationsError = store.createSelector(getStoreVariationsState, ɵ23);
    var ɵ24 = function (variations) { return variations.success; };
    var getStoreVariationsSuccess = store.createSelector(getStoreVariationsState, ɵ24);
    var ɵ25 = function (state) { return state.images; };
    // --------------------- IMAGES: -------------------------
    var getStoreImagesState = store.createSelector(getStorePageState, ɵ25);
    var ɵ26 = function (images) { return images.isLoading; };
    var getStoreImagesIsLoading = store.createSelector(getStoreImagesState, ɵ26);
    var ɵ27 = function (images) { return images.data; };
    var getStoreImagesData = store.createSelector(getStoreImagesState, ɵ27);
    var ɵ28 = function (images) { return images.hasBeenFetched; };
    var getStoreImagesHasBeenFetched = store.createSelector(getStoreImagesState, ɵ28);
    var ɵ29 = function (images) { return images.error; };
    var getStoreImagesError = store.createSelector(getStoreImagesState, ɵ29);
    var ɵ30 = function (images) { return images.success; };
    var getStoreImagesSuccess = store.createSelector(getStoreImagesState, ɵ30);

    var StorePageStore = /** @class */ (function () {
        function StorePageStore(store) {
            var _this = this;
            this.store = store;
            // ----------- CATEGORIES METHODS:
            this.LoadCategories = function () { return _this.store.dispatch(LoadCategoriesBeginAction()); };
            this.loadCategoryById = function (id) { return _this.store.dispatch(LoadCategorieByIdBeginAction({ id: id })); };
            this.CreateCategory = function (payload) { return _this.store.dispatch(CreateCategoryBeginAction({ payload: payload })); };
            this.DeleteCategory = function (id) { return _this.store.dispatch(DeleteCategoryBeginAction({ id: id })); };
            // ----------- ORDERS METHODS:
            this.LoadOrders = function () { return _this.store.dispatch(LoadOrdersBeginAction()); };
            this.loadOrderById = function (id) { return _this.store.dispatch(LoadOrderByIdBeginAction({ id: id })); };
            this.updateOrder = function (id, payload) { return _this.store.dispatch(UpdateOrderBeginAction({ id: id, payload: payload })); };
            // ----------- PRODUCTS METHODS:
            this.LoadProducts = function () { return _this.store.dispatch(LoadProductsBeginAction()); };
            this.CreateProduct = function (payload) { return _this.store.dispatch(CreateProductBeginAction({ payload: payload })); };
            this.UpdateProduct = function (id, payload) { return _this.store.dispatch(UpdateProductBeginAction({ id: id, payload: payload })); };
            this.DeleteProduct = function (id) { return _this.store.dispatch(DeleteProductBeginAction({ id: id })); };
            // ----------- VARIATIONS METHODS:
            this.LoadVariations = function () { return _this.store.dispatch(LoadVariationsBeginAction()); };
            this.CreateVariation = function (payload) { return _this.store.dispatch(CreteVariationBeginAction({ payload: payload })); };
            this.DeleteVariation = function (payload) { return _this.store.dispatch(DelteVariationBeginAction({ payload: payload })); };
            // ----------- IMAGE METHODS:
            this.LoadImages = function () { return _this.store.dispatch(LoadImagesBeginAction()); };
            this.LoadImageById = function (id) { return _this.store.dispatch(LoadImageByIdBeginAction({ id: id })); };
            this.CreateImage = function (payload) { return _this.store.dispatch(CreateImageBeginAction({ payload: payload })); };
            this.DeleteImage = function (id) { return _this.store.dispatch(DeleteImageBeginAction({ id: id })); };
        }
        Object.defineProperty(StorePageStore.prototype, "IsLoadingCategories$", {
            get: function () { return this.store.select(getCategoriesIsloading); },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(StorePageStore.prototype, "CategoriesData$", {
            get: function () { return this.store.select(getCategoriesData); },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(StorePageStore.prototype, "CategoriesHasBeenFetched$", {
            get: function () { return this.store.select(getCategoriesHasBeenFetched); },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(StorePageStore.prototype, "CategoriesError$", {
            get: function () { return this.store.select(getCategoriesError); },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(StorePageStore.prototype, "CategoriesSuccess$", {
            get: function () { return this.store.select(getCategoriesSuccess); },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(StorePageStore.prototype, "IsLoadingOrders$", {
            get: function () { return this.store.select(getOrdersIsLoading); },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(StorePageStore.prototype, "OrdersData$", {
            get: function () { return this.store.select(getOrdersData); },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(StorePageStore.prototype, "OrdersHasBeenFetched$", {
            get: function () { return this.store.select(getOrdersHasBeenFetched); },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(StorePageStore.prototype, "OrdersError$", {
            get: function () { return this.store.select(getOrdersError); },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(StorePageStore.prototype, "OrdersSuccess$", {
            get: function () { return this.store.select(getOrdersSuccess); },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(StorePageStore.prototype, "IsLoadingProducts$", {
            get: function () { return this.store.select(getStoreProductsIsLoading); },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(StorePageStore.prototype, "ProductsData$", {
            get: function () { return this.store.select(getStoreProductsData); },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(StorePageStore.prototype, "ProductsHasBeenFetched$", {
            get: function () { return this.store.select(getStoreProductsHasBeenFetched); },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(StorePageStore.prototype, "ProductsError$", {
            get: function () { return this.store.select(getStoreProductsError); },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(StorePageStore.prototype, "ProductsSuccess$", {
            get: function () { return this.store.select(getStoreProductsSuccess); },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(StorePageStore.prototype, "IsLoadingVariations$", {
            get: function () { return this.store.select(getStoreVariationsIsLoading); },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(StorePageStore.prototype, "VariationsData$", {
            get: function () { return this.store.select(getStoreVariationsData); },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(StorePageStore.prototype, "VariationsHasBeenFetched$", {
            get: function () { return this.store.select(getStoreVariationsHasBeenFetched); },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(StorePageStore.prototype, "VariationsError$", {
            get: function () { return this.store.select(getStoreVariationsError); },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(StorePageStore.prototype, "VariationsSuccess$", {
            get: function () { return this.store.select(getStoreVariationsSuccess); },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(StorePageStore.prototype, "IsLoadingImages$", {
            get: function () { return this.store.select(getStoreImagesIsLoading); },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(StorePageStore.prototype, "ImagesData$", {
            get: function () { return this.store.select(getStoreImagesData); },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(StorePageStore.prototype, "ImagesHasBeenFetched$", {
            get: function () { return this.store.select(getStoreImagesHasBeenFetched); },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(StorePageStore.prototype, "ImagesError$", {
            get: function () { return this.store.select(getStoreImagesError); },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(StorePageStore.prototype, "ImagesSuccess$", {
            get: function () { return this.store.select(getStoreImagesSuccess); },
            enumerable: true,
            configurable: true
        });
        StorePageStore.ctorParameters = function () { return [
            { type: store.Store }
        ]; };
        StorePageStore = __decorate([
            core.Injectable()
        ], StorePageStore);
        return StorePageStore;
    }());

    var StoreEffects = /** @class */ (function () {
        function StoreEffects(actions, storePageStore, service) {
            var _this = this;
            this.actions = actions;
            this.storePageStore = storePageStore;
            this.service = service;
            // --------------------------- CATEGORIES: ---------------------------------
            this.LoadCategories$ = effects.createEffect(function () { return _this.actions.pipe(effects.ofType(LoadCategoriesBeginAction), operators.switchMap(function () {
                return _this.service.loadCategories().pipe(operators.flatMap(function (payload) { return [
                    LoadCategoriesSuccessAction({ payload: payload }),
                    ClearCategoriesSuccessErrorDataAction()
                ]; }), operators.catchError(function (errors) {
                    console.error('StoreEffects.LoadCategories$ ERROR', errors);
                    return rxjs.of([
                        LoadCategoriesFailAction({ errors: errors }),
                        ClearCategoriesSuccessErrorDataAction()
                    ]).pipe(operators.switchMap(function (d) { return d; }));
                }));
            })); });
            this.LoadCategoryById$ = effects.createEffect(function () { return _this.actions.pipe(effects.ofType(LoadCategorieByIdBeginAction), operators.switchMap(function (action) {
                return _this.service.loadCategoryById(action.id).pipe(operators.flatMap(function (payload) { return [
                    LoadCategorieByIdSuccessAction({ payload: payload }),
                    ClearCategoriesSuccessErrorDataAction()
                ]; }), operators.catchError(function (errors) {
                    console.error('StoreEffects.LoadCategoryById$ ERROR', errors);
                    return rxjs.of([
                        LoadCategorieByIdFailAction({ errors: errors }),
                        ClearCategoriesSuccessErrorDataAction()
                    ]).pipe(operators.switchMap(function (d) { return d; }));
                }));
            })); });
            this.CreateCategory$ = effects.createEffect(function () { return _this.actions.pipe(effects.ofType(CreateCategoryBeginAction), operators.withLatestFrom(_this.storePageStore.CategoriesData$), operators.switchMap(function (_a) {
                var _b = __read(_a, 2), action = _b[0], categoriesData = _b[1];
                return _this.service.createCategory(action.payload).pipe(operators.flatMap(function (data) {
                    var payload = __spread([data], categoriesData);
                    return [
                        CreateCategorySuccessAction({ payload: payload }),
                        ClearCategoriesSuccessErrorDataAction()
                    ];
                }), operators.catchError(function (errors) {
                    console.error('StoreEffects.CreateCategory$ ERROR', errors);
                    return rxjs.of([
                        CreateCategoryFailAction({ errors: errors }),
                        ClearCategoriesSuccessErrorDataAction()
                    ]).pipe(operators.switchMap(function (d) { return d; }));
                }));
            })); });
            this.DeleteCategory$ = effects.createEffect(function () { return _this.actions.pipe(effects.ofType(DeleteCategoryBeginAction), operators.withLatestFrom(_this.storePageStore.CategoriesData$), operators.switchMap(function (_a) {
                var _b = __read(_a, 2), action = _b[0], categoriesData = _b[1];
                return _this.service.deleteCategory(action.id).pipe(operators.flatMap(function () {
                    var payload = categoriesData.filter(function (c) { return c.id !== action.id; });
                    return [
                        DeleteCategorySuccessAction({ payload: payload }),
                        ClearCategoriesSuccessErrorDataAction()
                    ];
                }), operators.catchError(function (errors) {
                    console.error('StoreEffects.DeleteCategory$ ERROR', errors);
                    return rxjs.of([
                        DeleteCategoryFailAction({ errors: errors }),
                        ClearCategoriesSuccessErrorDataAction()
                    ]).pipe(operators.switchMap(function (d) { return d; }));
                }));
            })); });
            // --------------------------- ORDERS: ---------------------------------
            this.LoadOrders$ = effects.createEffect(function () { return _this.actions.pipe(effects.ofType(LoadOrdersBeginAction), operators.switchMap(function () {
                return _this.service.loadOrders().pipe(operators.flatMap(function (data) { return [
                    LoadOrdersSuccessAction({ payload: data }),
                    ClearOrdersSuccessErrorDataAction()
                ]; }), operators.catchError(function (errors) {
                    console.error('StoreEffects.LoadOrders$ ERROR', errors);
                    return rxjs.of([
                        LoadOrdersFailAction({ errors: errors }),
                        ClearOrdersSuccessErrorDataAction()
                    ]).pipe(operators.switchMap(function (d) { return d; }));
                }));
            })); });
            this.LoadOrderById$ = effects.createEffect(function () { return _this.actions.pipe(effects.ofType(LoadOrderByIdBeginAction), operators.switchMap(function (action) {
                return _this.service.loadOrderById(action.id).pipe(operators.flatMap(function (payload) { return [
                    LoadOrderByIdSuccessAction({ payload: payload }),
                    ClearOrdersSuccessErrorDataAction()
                ]; }), operators.catchError(function (errors) {
                    console.error('StoreEffects.LoadOrderById$ ERROR', errors);
                    return rxjs.of([
                        LoadOrderByIdFailAction({ errors: errors }),
                        ClearOrdersSuccessErrorDataAction()
                    ]).pipe(operators.switchMap(function (d) { return d; }));
                }));
            })); });
            this.UpdateOrderById$ = effects.createEffect(function () { return _this.actions.pipe(effects.ofType(UpdateOrderBeginAction), operators.withLatestFrom(_this.storePageStore.OrdersData$), operators.switchMap(function (_a) {
                var _b = __read(_a, 2), action = _b[0], ordersData = _b[1];
                return _this.service.updateOrder(action.id, action.payload).pipe(operators.flatMap(function (data) {
                    var payload = __spread([data], ordersData.filter(function (o) { return o.id === action.id; }));
                    return [
                        UpdateOrderSuccessAction({ payload: payload }),
                        ClearOrdersSuccessErrorDataAction()
                    ];
                }), operators.catchError(function (errors) {
                    console.error('StoreEffects.UpdateOrderById$ ERROR', errors);
                    return rxjs.of([
                        UpdateOrderFailAction({ errors: errors }),
                        ClearOrdersSuccessErrorDataAction()
                    ]).pipe(operators.switchMap(function (d) { return d; }));
                }));
            })); });
            // --------------------------- PRODUCTS: ---------------------------------
            this.LoadProducts$ = effects.createEffect(function () { return _this.actions.pipe(effects.ofType(LoadProductsBeginAction), operators.switchMap(function () {
                return _this.service.loadProducts().pipe(operators.flatMap(function (payload) { return [
                    LoadProductsSuccessAction({ payload: payload }),
                    ClearProductsSuccessErrorDataAction()
                ]; }), operators.catchError(function (errors) {
                    console.error('StoreEffects.LoadProducts$ ERROR', errors);
                    return rxjs.of([
                        LoadProductsFailAction({ errors: errors }),
                        ClearProductsSuccessErrorDataAction()
                    ]).pipe(operators.switchMap(function (d) { return d; }));
                }));
            })); });
            this.LoadProductById$ = effects.createEffect(function () { return _this.actions.pipe(effects.ofType(LoadProductByIdBeginAction), operators.switchMap(function (action) {
                return _this.service.loadProductById(action.id).pipe(operators.flatMap(function (payload) { return [
                    LoadProductByIdSuccessAction({ payload: payload }),
                    ClearProductsSuccessErrorDataAction()
                ]; }), operators.catchError(function (errors) {
                    console.error('StoreEffects.LoadProductById$ ERROR', errors);
                    return rxjs.of([
                        LoadProductByIdFailAction({ errors: errors }),
                        ClearProductsSuccessErrorDataAction()
                    ]).pipe(operators.switchMap(function (d) { return d; }));
                }));
            })); });
            this.CreateProduct$ = effects.createEffect(function () { return _this.actions.pipe(effects.ofType(CreateProductBeginAction), operators.withLatestFrom(_this.storePageStore.ProductsData$), operators.switchMap(function (_a) {
                var _b = __read(_a, 2), action = _b[0], productsData = _b[1];
                return _this.service.createProduct(action.payload).pipe(operators.flatMap(function (data) {
                    var payload = __spread([data], productsData);
                    return [
                        CreateProductSuccessAction({ payload: payload }),
                        ClearProductsSuccessErrorDataAction()
                    ];
                }), operators.catchError(function (errors) {
                    console.error('StoreEffects.CreateProducts$ ERROR', errors);
                    return rxjs.of([
                        CreateProductFailAction({ errors: errors }),
                        ClearProductsSuccessErrorDataAction()
                    ]).pipe(operators.switchMap(function (d) { return d; }));
                }));
            })); });
            this.UpdateProduct$ = effects.createEffect(function () { return _this.actions.pipe(effects.ofType(UpdateProductBeginAction), operators.withLatestFrom(_this.storePageStore.ProductsData$), operators.switchMap(function (_a) {
                var _b = __read(_a, 2), action = _b[0], productsData = _b[1];
                return _this.service.updateProduct(action.id, action.payload).pipe(operators.flatMap(function (data) {
                    var payload = __spread([data], productsData.filter(function (p) { return p.id !== action.id; }));
                    return [
                        UpdateProductSuccessAction({ payload: payload }),
                        ClearProductsSuccessErrorDataAction()
                    ];
                }), operators.catchError(function (errors) {
                    console.error('StoreEffects.UpdateProducts$ ERROR', errors);
                    return rxjs.of([
                        UpdateProductFailAction({ errors: errors }),
                        ClearProductsSuccessErrorDataAction()
                    ]).pipe(operators.switchMap(function (d) { return d; }));
                }));
            })); });
            this.DeleteProduct$ = effects.createEffect(function () { return _this.actions.pipe(effects.ofType(DeleteProductBeginAction), operators.withLatestFrom(_this.storePageStore.ProductsData$), operators.switchMap(function (_a) {
                var _b = __read(_a, 2), action = _b[0], productsData = _b[1];
                return _this.service.deleteProduct(action.id).pipe(operators.flatMap(function () {
                    var payload = productsData.filter(function (p) { return p.id !== action.id; });
                    return [
                        DeleteProductSuccessAction({ payload: payload }),
                        ClearProductsSuccessErrorDataAction()
                    ];
                }), operators.catchError(function (errors) {
                    console.error('StoreEffects.DeleteProduct$ ERROR', errors);
                    return rxjs.of([
                        DeleteProductFailAction({ errors: errors }),
                        ClearProductsSuccessErrorDataAction()
                    ]).pipe(operators.switchMap(function (d) { return d; }));
                }));
            })); });
            // --------------------------- PRODUCTS: ---------------------------------
            this.LoadVariations$ = effects.createEffect(function () { return _this.actions.pipe(effects.ofType(LoadVariationsBeginAction), operators.switchMap(function () {
                return _this.service.loadProductsVariations().pipe(operators.flatMap(function (data) { return [
                    LoadVariationsSuccessAction({ payload: data }),
                    ClearVariationsSuccessErrorDataAction()
                ]; }), operators.catchError(function (errors) {
                    console.error('StoreEffects.LoadVariations$ ERROR', errors);
                    return rxjs.of([
                        LoadVariationsFailAction({ errors: errors }),
                        ClearVariationsSuccessErrorDataAction()
                    ]).pipe(operators.switchMap(function (d) { return d; }));
                }));
            })); });
            this.LoadVariationById$ = effects.createEffect(function () { return _this.actions.pipe(effects.ofType(LoadVariationByIdBeginAction), operators.switchMap(function (action) {
                return _this.service.loadProductVariationById(action.id).pipe(operators.flatMap(function (payload) { return [
                    LoadVariationByIdSuccessAction({ payload: payload }),
                    ClearVariationsSuccessErrorDataAction()
                ]; }), operators.catchError(function (errors) {
                    console.error('StoreEffects.LoadVariationById$ ERROR', errors);
                    return rxjs.of([
                        LoadVariationByIdFailAction({ errors: errors }),
                        ClearVariationsSuccessErrorDataAction()
                    ]).pipe(operators.switchMap(function (d) { return d; }));
                }));
            })); });
            this.CreateVariation$ = effects.createEffect(function () { return _this.actions.pipe(effects.ofType(CreteVariationBeginAction), operators.withLatestFrom(_this.storePageStore.VariationsData$), operators.switchMap(function (_a) {
                var _b = __read(_a, 2), action = _b[0], variationsData = _b[1];
                return _this.service.createProductVariation(action.payload).pipe(operators.flatMap(function (data) {
                    var payload = __spread([data], variationsData);
                    return [
                        CreteVariationSuccessAction({ payload: payload }),
                        ClearVariationsSuccessErrorDataAction()
                    ];
                }), operators.catchError(function (errors) {
                    console.error('StoreEffects.CreateVariation$ ERROR', errors);
                    return rxjs.of([
                        CreteVariataionFailAction({ errors: errors }),
                        ClearVariationsSuccessErrorDataAction()
                    ]).pipe(operators.switchMap(function (d) { return d; }));
                }));
            })); });
            this.DeleteVariation$ = effects.createEffect(function () { return _this.actions.pipe(effects.ofType(DelteVariationBeginAction), operators.withLatestFrom(_this.storePageStore.VariationsData$), operators.switchMap(function (_a) {
                var _b = __read(_a, 2), action = _b[0], variationsData = _b[1];
                return _this.service.deleteProductVariation(action.id).pipe(operators.flatMap(function () {
                    var payload = variationsData.filter(function (v) { return v.id !== action.id; });
                    return [
                        DelteVariationSuccessAction({ payload: payload }),
                        ClearVariationsSuccessErrorDataAction()
                    ];
                }), operators.catchError(function (errors) {
                    console.error('StoreEffects.DeleteVariation$ ERROR', errors);
                    return rxjs.of([
                        DelteVariationFailAction({ errors: errors }),
                        ClearVariationsSuccessErrorDataAction()
                    ]).pipe(operators.switchMap(function (d) { return d; }));
                }));
            })); });
            // --------------------------- IMAGES: ---------------------------------
            this.LoadImages$ = effects.createEffect(function () { return _this.actions.pipe(effects.ofType(LoadImagesBeginAction), operators.switchMap(function () {
                return _this.service.loadSiteImages().pipe(operators.flatMap(function (payload) { return [
                    LoadImagesSuccessAction({ payload: payload }),
                    ClearImagesSuccessErrorDataAction()
                ]; }), operators.catchError(function (errors) {
                    console.error('StoreEffects.LoadImages$ ERROR', errors);
                    return rxjs.of([
                        LoadImagesFailAction({ errors: errors }),
                        ClearImagesSuccessErrorDataAction()
                    ]).pipe(operators.switchMap(function (d) { return d; }));
                }));
            })); });
            this.LoadImageById$ = effects.createEffect(function () { return _this.actions.pipe(effects.ofType(LoadImageByIdBeginAction), operators.switchMap(function (action) {
                return _this.service.loadSiteImageById(action.id).pipe(operators.flatMap(function (payload) { return [
                    LoadImageByIdSuccessAction({ payload: payload }),
                    ClearImagesSuccessErrorDataAction()
                ]; }), operators.catchError(function (errors) {
                    console.error('StoreEffects.LoadImageById$ ERROR', errors);
                    return rxjs.of([
                        LoadImageByIdFailAction({ errors: errors }),
                        ClearImagesSuccessErrorDataAction()
                    ]).pipe(operators.switchMap(function (d) { return d; }));
                }));
            })); });
            this.CreateImage$ = effects.createEffect(function () { return _this.actions.pipe(effects.ofType(CreateImageBeginAction), operators.withLatestFrom(_this.storePageStore.ImagesData$), operators.switchMap(function (_a) {
                var _b = __read(_a, 2), action = _b[0], imagesData = _b[1];
                return _this.service.createSiteImage(action.payload).pipe(operators.flatMap(function (data) {
                    var payload = __spread([data], imagesData);
                    return [
                        CreateImageSuccessAction({ payload: payload }),
                        ClearImagesSuccessErrorDataAction()
                    ];
                }), operators.catchError(function (errors) {
                    console.error('StoreEffects.CreateImage$ ERROR', errors);
                    return rxjs.of([
                        CreateImageFailAction({ errors: errors }),
                        ClearImagesSuccessErrorDataAction()
                    ]).pipe(operators.switchMap(function (d) { return d; }));
                }));
            })); });
            this.DeleteImage$ = effects.createEffect(function () { return _this.actions.pipe(effects.ofType(DeleteImageBeginAction), operators.withLatestFrom(_this.storePageStore.ImagesData$), operators.switchMap(function (_a) {
                var _b = __read(_a, 2), action = _b[0], imagesData = _b[1];
                return _this.service.deleteSiteImage(action.id).pipe(operators.flatMap(function () {
                    var payload = imagesData.filter(function (i) { return i.id !== action.id; });
                    return [
                        DeleteImageSuccessAction({ payload: payload }),
                        ClearImagesSuccessErrorDataAction()
                    ];
                }), operators.catchError(function (errors) {
                    console.error('StoreEffects.DeleteImage$ ERROR', errors);
                    return rxjs.of([
                        DeleteImageFailAction({ errors: errors }),
                        ClearImagesSuccessErrorDataAction()
                    ]).pipe(operators.switchMap(function (d) { return d; }));
                }));
            })); });
        }
        StoreEffects.ctorParameters = function () { return [
            { type: effects.Actions },
            { type: StorePageStore },
            { type: undefined, decorators: [{ type: core.Inject, args: [STORE_SERVICE,] }] }
        ]; };
        StoreEffects = __decorate([
            core.Injectable(),
            __param(2, core.Inject(STORE_SERVICE))
        ], StoreEffects);
        return StoreEffects;
    }());

    var ɵ0$1 = [], ɵ1$1 = [], ɵ2$1 = [], ɵ3$1 = [], ɵ4$1 = [];
    var initialState = {
        categories: {
            isLoading: false,
            data: ɵ0$1,
            selectedCategory: null,
            hasBeenFetched: false,
            error: null,
            success: null
        },
        orders: {
            isLoading: false,
            data: ɵ1$1,
            selectedOrder: null,
            hasBeenFetched: false,
            error: null,
            success: null
        },
        products: {
            isLoading: false,
            data: ɵ2$1,
            selectedProduct: null,
            hasBeenFetched: false,
            error: null,
            success: null
        },
        variations: {
            isLoading: false,
            data: ɵ3$1,
            selectedVariation: null,
            hasBeenFetched: false,
            error: null,
            success: null
        },
        images: {
            isLoading: false,
            data: ɵ4$1,
            selectedImage: null,
            hasBeenFetched: false,
            error: null,
            success: null
        }
    };
    var ɵ5$1 = function (state, action) { return (__assign(__assign({}, state), { categories: __assign(__assign({}, state.categories), { isLoading: true, error: null, success: null }) })); }, ɵ6$1 = function (state, action) { return (__assign(__assign({}, state), { categories: __assign(__assign({}, state.categories), { isLoading: false, hasBeenFetched: true, data: action.payload, error: null, success: { after: getStoreCategoriesActionType(action.type) } }) })); }, ɵ7$1 = function (state, action) { return (__assign(__assign({}, state), { categories: __assign(__assign({}, state.categories), { isLoading: false, hasBeenFetched: true, selectedCategory: action.payload, error: null, success: { after: getStoreCategoriesActionType(action.type) } }) })); }, ɵ8$1 = function (state, action) { return (__assign(__assign({}, state), { categories: __assign(__assign({}, state.categories), { isLoading: false, error: { after: getStoreCategoriesActionType(action.type), error: action.errors } }) })); }, ɵ9$1 = function (state) { return (__assign(__assign({}, state), { categories: __assign(__assign({}, state.categories), { error: null, success: null }) })); }, ɵ10$1 = function (state, action) { return (__assign(__assign({}, state), { orders: __assign(__assign({}, state.orders), { isLoading: true, error: null, success: null }) })); }, ɵ11$1 = function (state, action) { return (__assign(__assign({}, state), { orders: __assign(__assign({}, state.orders), { isLoading: false, hasBeenFetched: true, data: action.payload, error: null, success: { after: getStoreOrdersActionType(action.type) } }) })); }, ɵ12$1 = function (state, action) { return (__assign(__assign({}, state), { orders: __assign(__assign({}, state.orders), { isLoading: false, hasBeenFetched: true, selectedOrder: action.payload, error: null, success: { after: getStoreOrdersActionType(action.type) } }) })); }, ɵ13$1 = function (state, action) { return (__assign(__assign({}, state), { orders: __assign(__assign({}, state.orders), { isLoading: false, error: { after: getStoreOrdersActionType(action.type), error: action.errors }, success: null }) })); }, ɵ14$1 = function (state) { return (__assign(__assign({}, state), { orders: __assign(__assign({}, state.orders), { error: null, success: null }) })); }, ɵ15$1 = function (state, action) { return (__assign(__assign({}, state), { products: __assign(__assign({}, state.products), { isLoading: true, error: null, success: null }) })); }, ɵ16$1 = function (state, action) { return (__assign(__assign({}, state), { products: __assign(__assign({}, state.products), { isLoading: false, hasBeenFetched: true, data: action.payload, error: null, success: { after: getStoreProductsActionType(action.type) } }) })); }, ɵ17$1 = function (state, action) { return (__assign(__assign({}, state), { products: __assign(__assign({}, state.products), { isLoading: false, selectedProduct: action.payload, error: null, success: { after: getStoreProductsActionType(action.type) } }) })); }, ɵ18$1 = function (state, action) { return (__assign(__assign({}, state), { products: __assign(__assign({}, state.products), { isLoading: false, error: { after: getStoreProductsActionType(action.type), error: action.errors }, success: null }) })); }, ɵ19$1 = function (state) { return (__assign(__assign({}, state), { products: __assign(__assign({}, state.products), { error: null, success: null }) })); }, ɵ20$1 = function (state, action) { return (__assign(__assign({}, state), { variations: __assign(__assign({}, state.variations), { isLoading: true, error: null, success: null }) })); }, ɵ21$1 = function (state, action) { return (__assign(__assign({}, state), { variations: __assign(__assign({}, state.variations), { isLoading: false, hasBeenFetched: true, data: action.payload, error: null, success: { after: getStoreVariationsActionType(action.type) } }) })); }, ɵ22$1 = function (state, action) { return (__assign(__assign({}, state), { variations: __assign(__assign({}, state.variations), { isLoading: false, hasBeenFetched: true, selectedVariation: action.payload, error: null, success: { after: getStoreVariationsActionType(action.type) } }) })); }, ɵ23$1 = function (state, action) { return (__assign(__assign({}, state), { variations: __assign(__assign({}, state.variations), { isLoading: false, error: { after: getStoreVariationsActionType(action.type), error: action.errors }, success: null }) })); }, ɵ24$1 = function (state) { return (__assign(__assign({}, state), { variations: __assign(__assign({}, state.variations), { error: null, success: null }) })); }, ɵ25$1 = function (state, action) { return (__assign(__assign({}, state), { images: __assign(__assign({}, state.images), { isLoading: true, error: null, success: null }) })); }, ɵ26$1 = function (state, action) { return (__assign(__assign({}, state), { images: __assign(__assign({}, state.images), { isLoading: false, hasBeenFetched: true, data: action.payload, error: null, success: { after: getStoreImagesActionType(action.type) } }) })); }, ɵ27$1 = function (state, action) { return (__assign(__assign({}, state), { images: __assign(__assign({}, state.images), { isLoading: false, selectedImage: action.payload, error: null, success: { after: getStoreImagesActionType(action.type) } }) })); }, ɵ28$1 = function (state, action) { return (__assign(__assign({}, state), { images: __assign(__assign({}, state.images), { isLoading: false, error: { after: getStoreImagesActionType(action.type), error: action.errors }, success: null }) })); }, ɵ29$1 = function (state) { return (__assign(__assign({}, state), { images: __assign(__assign({}, state.images), { error: null, success: null }) })); };
    var reducer = store.createReducer(initialState, 
    // ------------------ CATEGORIES: ---------------------
    store.on(LoadCategoriesBeginAction, LoadCategorieByIdBeginAction, CreateCategoryBeginAction, DeleteCategoryBeginAction, ɵ5$1), store.on(LoadCategoriesSuccessAction, CreateCategorySuccessAction, DeleteCategorySuccessAction, ɵ6$1), store.on(LoadCategorieByIdSuccessAction, ɵ7$1), store.on(LoadCategoriesFailAction, LoadCategorieByIdFailAction, CreateCategoryFailAction, DeleteCategoryFailAction, ɵ8$1), store.on(ClearCategoriesSuccessErrorDataAction, ɵ9$1), 
    // ------------------ ORDERS: ---------------------
    store.on(LoadOrdersBeginAction, LoadOrderByIdBeginAction, UpdateOrderBeginAction, ɵ10$1), store.on(LoadOrdersSuccessAction, UpdateOrderSuccessAction, ɵ11$1), store.on(LoadOrderByIdSuccessAction, ɵ12$1), store.on(LoadOrdersFailAction, LoadOrdersFailAction, UpdateOrderFailAction, ɵ13$1), store.on(ClearOrdersSuccessErrorDataAction, ɵ14$1), 
    // ------------------ PRODUCTS: ---------------------
    store.on(LoadProductsBeginAction, LoadProductByIdBeginAction, CreateProductBeginAction, UpdateProductBeginAction, DeleteProductBeginAction, ɵ15$1), store.on(LoadProductsSuccessAction, CreateProductSuccessAction, UpdateProductSuccessAction, DeleteProductSuccessAction, ɵ16$1), store.on(LoadProductByIdSuccessAction, ɵ17$1), store.on(LoadProductsFailAction, LoadProductByIdFailAction, CreateProductFailAction, UpdateProductFailAction, DeleteProductFailAction, ɵ18$1), store.on(ClearProductsSuccessErrorDataAction, ɵ19$1), 
    // ------------------ VARIATIONS: ---------------------
    store.on(LoadVariationsBeginAction, LoadVariationByIdBeginAction, CreteVariationBeginAction, DelteVariationBeginAction, ɵ20$1), store.on(LoadVariationsSuccessAction, CreteVariationSuccessAction, DelteVariationSuccessAction, ɵ21$1), store.on(LoadVariationByIdSuccessAction, ɵ22$1), store.on(LoadVariationsFailAction, LoadVariationByIdFailAction, CreteVariataionFailAction, DelteVariationFailAction, ɵ23$1), store.on(ClearVariationsSuccessErrorDataAction, ɵ24$1), 
    // ------------------ IMAGES: ---------------------
    store.on(LoadImagesBeginAction, LoadImageByIdBeginAction, CreateImageBeginAction, DeleteImageBeginAction, ɵ25$1), store.on(LoadImagesSuccessAction, CreateImageSuccessAction, DeleteImageSuccessAction, ɵ26$1), store.on(LoadImageByIdSuccessAction, ɵ27$1), store.on(LoadImagesFailAction, LoadImageByIdFailAction, CreateImageFailAction, DeleteImageFailAction, ɵ28$1), store.on(ClearImagesSuccessErrorDataAction, ɵ29$1));
    function getStoreCategoriesActionType(type) {
        var action;
        switch (type) {
            case exports.ɵa.LoadCategoriesSuccess:
            case exports.ɵa.LoadCategoriesFail:
                action = 'LOAD_CATEGORIES';
                break;
            case exports.ɵa.CreateCategorySuccess:
            case exports.ɵa.CreateCategoryFail:
                action = 'CREATE_CATEGORY';
                break;
            case exports.ɵa.DeleteCategorySuccess:
            case exports.ɵa.DeleteCategoryFail:
                action = 'DELETE_CATEGORY';
                break;
            default:
                action = 'UNKNOWN';
        }
        return action;
    }
    function getStoreOrdersActionType(type) {
        var action;
        switch (type) {
            case exports.StoreOrdersActionTypes.LoadOrdersSuccess:
            case exports.StoreOrdersActionTypes.LoadOrdersFail:
                action = 'LOAD_ORDERS';
                break;
            case exports.StoreOrdersActionTypes.UpdateOrderSuccess:
            case exports.StoreOrdersActionTypes.UpdateOrderFail:
                action = 'UPDATE_ORDER';
                break;
            default:
                action = 'UNKNOWN';
        }
        return action;
    }
    function getStoreProductsActionType(type) {
        var action;
        switch (type) {
            case exports.StoreProductsActionTypes.LoadProductsSuccess:
            case exports.StoreProductsActionTypes.LoadProductsFail:
                action = 'LOAD_PRODUCTS';
                break;
            case exports.StoreProductsActionTypes.CreateProductSuccess:
            case exports.StoreProductsActionTypes.CreateProductFail:
                action = 'CREATE_PRODUCT';
                break;
            case exports.StoreProductsActionTypes.UpdateProductSuccess:
            case exports.StoreProductsActionTypes.UpdateProductFail:
                action = 'UPDATE_PRODUCT';
                break;
            case exports.StoreProductsActionTypes.DeleteProductSuccess:
            case exports.StoreProductsActionTypes.DeleteProductFail:
                action = 'DELETE_PRODUCT';
                break;
            default:
                action = 'UNKNOWN';
        }
        return action;
    }
    function getStoreVariationsActionType(type) {
        var action;
        switch (type) {
            case exports.StoreVariationsActionTypes.LoadVariationsSuccess:
            case exports.StoreVariationsActionTypes.LoadVariationsFail:
                action = 'LOAD_VARIATIONS';
                break;
            case exports.StoreVariationsActionTypes.CreateVariationSuccess:
            case exports.StoreVariationsActionTypes.CreateVariationFail:
                action = 'CREATE_VARIATION';
                break;
            case exports.StoreVariationsActionTypes.DeleteVariationSuccess:
            case exports.StoreVariationsActionTypes.DeleteVariationFail:
                action = 'DELETE_VARIATION';
                break;
            default:
                action = 'UNKNOWN';
        }
        return action;
    }
    function getStoreImagesActionType(type) {
        var action;
        switch (type) {
            case exports.StoreImagesActionTypes.LoadImagesSuccess:
            case exports.StoreImagesActionTypes.LoadImagesFail:
            case exports.StoreImagesActionTypes.LoadImageByIdSuccess:
            case exports.StoreImagesActionTypes.LoadImageByIdFail:
                action = 'LOAD_IMAGES';
                break;
            case exports.StoreImagesActionTypes.CreateImageBegin:
            case exports.StoreImagesActionTypes.CreateImageFail:
                action = 'CREATE_IMAGE';
                break;
            case exports.StoreImagesActionTypes.DelteImageSuccess:
            case exports.StoreImagesActionTypes.DelteImageFail:
                action = 'DELETE_IMAGE';
                break;
            default:
                action = 'UNKNOWN';
        }
        return action;
    }
    function storeReducer(state, action) {
        return reducer(state, action);
    }

    var StoreCoreModule = /** @class */ (function () {
        function StoreCoreModule() {
        }
        StoreCoreModule_1 = StoreCoreModule;
        StoreCoreModule.forRoot = function (config) {
            return {
                ngModule: StoreCoreModule_1,
                providers: __spread([
                    { provide: STORE_SERVICE, useClass: StoreService },
                    { provide: STORE_REPOSITORY, useClass: StoreRepository }
                ], config.providers, [
                    StorePageStore
                ])
            };
        };
        var StoreCoreModule_1;
        StoreCoreModule = StoreCoreModule_1 = __decorate([
            core.NgModule({
                declarations: [],
                imports: [
                    http.HttpClientModule,
                    store.StoreModule.forFeature('store', storeReducer),
                    effects.EffectsModule.forFeature([StoreEffects]),
                ],
                exports: []
            })
        ], StoreCoreModule);
        return StoreCoreModule;
    }());

    exports.CategoryModel = CategoryModel;
    exports.ClearImagesSuccessErrorDataAction = ClearImagesSuccessErrorDataAction;
    exports.ClearOrdersSuccessErrorDataAction = ClearOrdersSuccessErrorDataAction;
    exports.ClearProductsSuccessErrorDataAction = ClearProductsSuccessErrorDataAction;
    exports.ClearVariationsSuccessErrorDataAction = ClearVariationsSuccessErrorDataAction;
    exports.CreateImageBeginAction = CreateImageBeginAction;
    exports.CreateImageFailAction = CreateImageFailAction;
    exports.CreateImageSuccessAction = CreateImageSuccessAction;
    exports.CreateProductBeginAction = CreateProductBeginAction;
    exports.CreateProductFailAction = CreateProductFailAction;
    exports.CreateProductSuccessAction = CreateProductSuccessAction;
    exports.CreteVariataionFailAction = CreteVariataionFailAction;
    exports.CreteVariationBeginAction = CreteVariationBeginAction;
    exports.CreteVariationSuccessAction = CreteVariationSuccessAction;
    exports.DeleteImageBeginAction = DeleteImageBeginAction;
    exports.DeleteImageFailAction = DeleteImageFailAction;
    exports.DeleteImageSuccessAction = DeleteImageSuccessAction;
    exports.DeleteProductBeginAction = DeleteProductBeginAction;
    exports.DeleteProductFailAction = DeleteProductFailAction;
    exports.DeleteProductSuccessAction = DeleteProductSuccessAction;
    exports.DelteVariationBeginAction = DelteVariationBeginAction;
    exports.DelteVariationFailAction = DelteVariationFailAction;
    exports.DelteVariationSuccessAction = DelteVariationSuccessAction;
    exports.ImageModel = ImageModel;
    exports.LoadImageByIdBeginAction = LoadImageByIdBeginAction;
    exports.LoadImageByIdFailAction = LoadImageByIdFailAction;
    exports.LoadImageByIdSuccessAction = LoadImageByIdSuccessAction;
    exports.LoadImagesBeginAction = LoadImagesBeginAction;
    exports.LoadImagesFailAction = LoadImagesFailAction;
    exports.LoadImagesSuccessAction = LoadImagesSuccessAction;
    exports.LoadOrderByIdBeginAction = LoadOrderByIdBeginAction;
    exports.LoadOrderByIdFailAction = LoadOrderByIdFailAction;
    exports.LoadOrderByIdSuccessAction = LoadOrderByIdSuccessAction;
    exports.LoadOrdersBeginAction = LoadOrdersBeginAction;
    exports.LoadOrdersFailAction = LoadOrdersFailAction;
    exports.LoadOrdersSuccessAction = LoadOrdersSuccessAction;
    exports.LoadProductByIdBeginAction = LoadProductByIdBeginAction;
    exports.LoadProductByIdFailAction = LoadProductByIdFailAction;
    exports.LoadProductByIdSuccessAction = LoadProductByIdSuccessAction;
    exports.LoadProductsBeginAction = LoadProductsBeginAction;
    exports.LoadProductsFailAction = LoadProductsFailAction;
    exports.LoadProductsSuccessAction = LoadProductsSuccessAction;
    exports.LoadVariationByIdBeginAction = LoadVariationByIdBeginAction;
    exports.LoadVariationByIdFailAction = LoadVariationByIdFailAction;
    exports.LoadVariationByIdSuccessAction = LoadVariationByIdSuccessAction;
    exports.LoadVariationsBeginAction = LoadVariationsBeginAction;
    exports.LoadVariationsFailAction = LoadVariationsFailAction;
    exports.LoadVariationsSuccessAction = LoadVariationsSuccessAction;
    exports.OrderModel = OrderModel;
    exports.ProductModel = ProductModel;
    exports.ProductVariationModel = ProductVariationModel;
    exports.STORE_REPOSITORY = STORE_REPOSITORY;
    exports.STORE_SERVICE = STORE_SERVICE;
    exports.StoreCoreModule = StoreCoreModule;
    exports.StoreEffects = StoreEffects;
    exports.StorePageStore = StorePageStore;
    exports.StoreRepository = StoreRepository;
    exports.StoreService = StoreService;
    exports.UpdateOrderBeginAction = UpdateOrderBeginAction;
    exports.UpdateOrderFailAction = UpdateOrderFailAction;
    exports.UpdateOrderSuccessAction = UpdateOrderSuccessAction;
    exports.UpdateProductBeginAction = UpdateProductBeginAction;
    exports.UpdateProductFailAction = UpdateProductFailAction;
    exports.UpdateProductSuccessAction = UpdateProductSuccessAction;
    exports.VariationModel = VariationModel;
    exports.getCategoriesData = getCategoriesData;
    exports.getCategoriesError = getCategoriesError;
    exports.getCategoriesHasBeenFetched = getCategoriesHasBeenFetched;
    exports.getCategoriesIsloading = getCategoriesIsloading;
    exports.getCategoriesSuccess = getCategoriesSuccess;
    exports.getOrdersData = getOrdersData;
    exports.getOrdersError = getOrdersError;
    exports.getOrdersHasBeenFetched = getOrdersHasBeenFetched;
    exports.getOrdersIsLoading = getOrdersIsLoading;
    exports.getOrdersSuccess = getOrdersSuccess;
    exports.getStoreCategoriesState = getStoreCategoriesState;
    exports.getStoreImagesData = getStoreImagesData;
    exports.getStoreImagesError = getStoreImagesError;
    exports.getStoreImagesHasBeenFetched = getStoreImagesHasBeenFetched;
    exports.getStoreImagesIsLoading = getStoreImagesIsLoading;
    exports.getStoreImagesState = getStoreImagesState;
    exports.getStoreImagesSuccess = getStoreImagesSuccess;
    exports.getStoreOrdersState = getStoreOrdersState;
    exports.getStorePageState = getStorePageState;
    exports.getStoreProductsData = getStoreProductsData;
    exports.getStoreProductsError = getStoreProductsError;
    exports.getStoreProductsHasBeenFetched = getStoreProductsHasBeenFetched;
    exports.getStoreProductsIsLoading = getStoreProductsIsLoading;
    exports.getStoreProductsState = getStoreProductsState;
    exports.getStoreProductsSuccess = getStoreProductsSuccess;
    exports.getStoreState = getStoreState;
    exports.getStoreVariationsData = getStoreVariationsData;
    exports.getStoreVariationsError = getStoreVariationsError;
    exports.getStoreVariationsHasBeenFetched = getStoreVariationsHasBeenFetched;
    exports.getStoreVariationsIsLoading = getStoreVariationsIsLoading;
    exports.getStoreVariationsState = getStoreVariationsState;
    exports.getStoreVariationsSuccess = getStoreVariationsSuccess;
    exports.initialState = initialState;
    exports.storeReducer = storeReducer;
    exports.ɵ0 = ɵ0;
    exports.ɵ1 = ɵ1;
    exports.ɵ10 = ɵ10;
    exports.ɵ11 = ɵ11;
    exports.ɵ12 = ɵ12;
    exports.ɵ13 = ɵ13;
    exports.ɵ14 = ɵ14;
    exports.ɵ15 = ɵ15;
    exports.ɵ16 = ɵ16;
    exports.ɵ17 = ɵ17;
    exports.ɵ18 = ɵ18;
    exports.ɵ19 = ɵ19;
    exports.ɵ2 = ɵ2;
    exports.ɵ20 = ɵ20;
    exports.ɵ21 = ɵ21;
    exports.ɵ22 = ɵ22;
    exports.ɵ23 = ɵ23;
    exports.ɵ24 = ɵ24;
    exports.ɵ25 = ɵ25;
    exports.ɵ26 = ɵ26;
    exports.ɵ27 = ɵ27;
    exports.ɵ28 = ɵ28;
    exports.ɵ29 = ɵ29;
    exports.ɵ3 = ɵ3;
    exports.ɵ30 = ɵ30;
    exports.ɵ4 = ɵ4;
    exports.ɵ5 = ɵ5;
    exports.ɵ6 = ɵ6;
    exports.ɵ7 = ɵ7;
    exports.ɵ8 = ɵ8;
    exports.ɵ9 = ɵ9;
    exports.ɵb = LoadCategoriesBeginAction;
    exports.ɵc = LoadCategoriesSuccessAction;
    exports.ɵd = LoadCategoriesFailAction;
    exports.ɵe = LoadCategorieByIdBeginAction;
    exports.ɵf = LoadCategorieByIdSuccessAction;
    exports.ɵg = LoadCategorieByIdFailAction;
    exports.ɵh = CreateCategoryBeginAction;
    exports.ɵi = CreateCategorySuccessAction;
    exports.ɵj = CreateCategoryFailAction;
    exports.ɵk = DeleteCategoryBeginAction;
    exports.ɵl = DeleteCategorySuccessAction;
    exports.ɵm = DeleteCategoryFailAction;
    exports.ɵn = ClearCategoriesSuccessErrorDataAction;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=boxx-store-core.umd.js.map
