import { InjectionToken, Inject, Injectable, ɵɵdefineInjectable, ɵɵinject, NgModule } from '@angular/core';
import { __decorate, __param } from 'tslib';
import { HttpParams, HttpClient, HttpClientModule } from '@angular/common/http';
import { AbstractAppConfigService, APP_CONFIG_SERVICE } from '@boxx/core';
import { map, catchError, switchMap, flatMap, withLatestFrom } from 'rxjs/operators';
import { createAction, props, createFeatureSelector, createSelector, Store, createReducer, on, StoreModule } from '@ngrx/store';
import { createEffect, ofType, Actions, EffectsModule } from '@ngrx/effects';
import { of } from 'rxjs';

class CategoryModel {
    constructor(data) {
        this.active = data.active;
        this.categoryImages = data.categoryImages;
        this.description = data.description;
        this.id = data.id;
        this.name = data.name;
        this.parentId = data.parentId;
        this.resourceUri = data.resourceUri;
        this.shopId = data.shopId;
        this.slug = data.slug;
    }
    static fromApi(data) {
        let categoryImages = [];
        if (Array.isArray(data.category_images)) {
            categoryImages = data.category_images;
        }
        else if (typeof data.category_images === 'object') {
            Object.keys(data.category_images).forEach(key => {
                categoryImages.push(data.category_images[key].url);
            });
        }
        return new CategoryModel({
            active: data.active,
            categoryImages,
            description: data.description,
            id: data.id,
            name: data.name,
            parentId: data.parent_id,
            resourceUri: data.resource_uri,
            shopId: data.shop_id,
            slug: data.slug
        });
    }
    static empty() {
        return new CategoryModel({
            active: null,
            categoryImages: [],
            description: null,
            id: null,
            name: null,
            parentId: null,
            resourceUri: null,
            shopId: null,
            slug: null,
        });
    }
}

class ImageModel {
    constructor(data) {
        this.absolutePath = data.absolutePath;
        this.added = data.added;
        this.alt = data.alt;
        this.baseUrl = data.baseUrl;
        this.checksum = data.checksum;
        this.filesize = data.filesize;
        this.height = data.height;
        this.id = data.id;
        this.image = data.image;
        this.isGlobal = data.isGlobal;
        this.lastIncrement = data.lastIncrement;
        this.mimetype = data.mimetype;
        this.modified = data.modified;
        this.resourceUri = data.resourceUri;
        this.siteId = data.siteId;
        this.sizes = data.sizes;
        this.type = data.type;
        this.userId = data.userId;
        this.versions = data.versions;
        this.whitelabelId = data.whitelabelId;
        this.width = data.width;
    }
    static fromApi(data) {
        const versions = data.versions.map(v => ({
            absolutePath: v.absolute_path,
            filesize: v.filesize,
            height: v.height,
            id: v.id,
            image: v.image,
            imageId: v.image_id,
            isGlobal: v.is_global,
            mimetype: v.mimetype,
            resourceUri: v.resource_uri,
            type: v.type,
            width: v.width,
        }));
        return new ImageModel({
            absolutePath: data.absolute_path,
            added: data.added,
            alt: data.alt,
            baseUrl: data.base_url,
            checksum: data.checksum,
            filesize: data.filesize,
            height: data.height,
            id: data.id,
            image: data.image,
            isGlobal: data.is_global,
            lastIncrement: data.last_increment,
            mimetype: data.mimetype,
            modified: data.modified,
            resourceUri: data.resource_uri,
            siteId: data.site_id,
            sizes: data.sizes,
            type: data.type,
            userId: data.user_id,
            versions,
            whitelabelId: data.whitelabel_id,
            width: data.width,
        });
    }
    static empty() {
        return new ImageModel({
            absolutePath: null,
            added: null,
            alt: null,
            baseUrl: null,
            checksum: null,
            filesize: null,
            height: null,
            id: null,
            image: null,
            isGlobal: null,
            lastIncrement: null,
            mimetype: null,
            modified: null,
            resourceUri: null,
            siteId: null,
            sizes: null,
            type: null,
            userId: null,
            versions: null,
            whitelabelId: null,
            width: null,
        });
    }
}

class OrderModel {
    constructor(data) {
        this.buyerEmail = data.buyerEmail;
        this.buyerName = data.buyerName;
        this.id = data.id;
        this.price = data.price;
        this.resourceUri = data.resourceUri;
        this.sessionId = data.sessionId;
        this.shipToAddress = data.shipToAddress;
        this.shipToCity = data.shipToCity;
        this.shipToCountry = data.shipToCountry;
        this.shipToPostalCode = data.shipToPostalCode;
        this.shipToState = data.shipToState;
        this.shippingCost = data.shippingCost;
        this.shippingId = data.shippingId;
        this.shippingName = data.shippingName;
        this.siteId = data.siteId;
        this.status = data.status;
        this.timestamp = data.timestamp;
        this.transactionId = data.transactionId;
        this.whitelabelId = data.whitelabelId;
    }
    static fromApi(data) {
        return new OrderModel({
            buyerEmail: data.buyer_email,
            buyerName: data.buyer_name,
            id: data.id,
            price: data.price,
            resourceUri: data.resource_uri,
            sessionId: data.session_id,
            shipToAddress: data.ship_to_address,
            shipToCity: data.ship_to_city,
            shipToCountry: data.ship_to_country,
            shipToPostalCode: data.ship_to_postal_code,
            shipToState: data.ship_to_state,
            shippingCost: data.shipping_cost,
            shippingId: data.shipping_id,
            shippingName: data.shipping_name,
            siteId: data.site_id,
            status: data.status,
            timestamp: data.timestamp,
            transactionId: data.transaction_id,
            whitelabelId: data.whitelabel_id
        });
    }
    static empty() {
        return new OrderModel({
            buyerEmail: null,
            buyerName: null,
            id: null,
            price: null,
            resourceUri: null,
            sessionId: null,
            shipToAddress: null,
            shipToCity: null,
            shipToCountry: null,
            shipToPostalCode: null,
            shipToState: null,
            shippingCost: null,
            shippingId: null,
            shippingName: null,
            siteId: null,
            status: null,
            timestamp: null,
            transactionId: null,
            whitelabelId: null
        });
    }
}

class ProductModel {
    constructor(data) {
        this.active = data.active;
        this.added = data.added;
        this.asin = data.asin;
        this.autoStock = data.autoStock;
        this.brand = data.brand;
        this.categories = data.categories;
        this.description = data.description;
        this.excerpt = data.excerpt;
        this.height = data.height;
        this.id = data.id;
        this.image = data.image;
        this.imageId = data.imageId;
        this.length = data.length;
        this.msrpPrice = data.msrpPrice;
        this.outgoingUrl = data.outgoingUrl;
        this.price = data.price;
        this.primaryImageUrl = data.primaryImageUrl;
        this.productImages = data.productImages;
        this.published = data.published;
        this.quantity = data.quantity;
        this.resourceUri = data.resourceUri;
        this.salePrice = data.salePrice;
        this.shopId = data.shopId;
        this.sku = data.sku;
        this.slug = data.slug;
        this.status = data.status;
        this.title = data.title;
        this.upc = data.upc;
        this.variations = data.variations;
        this.vendor = data.vendor;
        this.weight = data.weight;
        this.weightUnit = data.weightUnit;
        this.wholesalePrice = data.wholesalePrice;
        this.width = data.width;
    }
    static fromApi(data) {
        const categories = data.categories.map(c => CategoryModel.fromApi(c));
        let productImages;
        if (data.product_images) {
            if (!Array.isArray(data.product_images)) {
                productImages = data.product_images;
            }
            else {
                data.product_images.forEach((img, idx) => productImages[idx] = img);
            }
        }
        const variations = data.variations.map(v => ProductVariationModel.fromApi(v));
        return new ProductModel({
            active: data.active,
            added: data.added,
            asin: data.asin,
            autoStock: data.auto_stock,
            brand: data.brand,
            categories: data.categories.map(c => CategoryModel.fromApi(c)),
            description: data.description,
            excerpt: data.excerpt,
            height: data.height,
            id: data.id,
            image: data.image ? { id: data.image.id, url: data.image.base_url + '/' + data.image.absolute_path } : null,
            imageId: data.image_id,
            length: data.length,
            msrpPrice: data.msrp_price,
            outgoingUrl: data.outgoing_url,
            price: data.price,
            primaryImageUrl: data.primary_image_url,
            productImages,
            published: data.published,
            quantity: data.quantity,
            resourceUri: data.resource_uri,
            salePrice: data.sale_price,
            shopId: data.shop_id,
            sku: data.sku,
            slug: data.slug,
            status: data.status,
            title: data.title,
            upc: data.upc,
            variations,
            vendor: data.vendor,
            weight: data.weight,
            weightUnit: data.weight_unit,
            wholesalePrice: data.wholesale_price,
            width: data.width,
        });
    }
    static empty() {
        return new ProductModel({
            active: null,
            added: null,
            asin: null,
            autoStock: null,
            brand: null,
            categories: null,
            description: null,
            excerpt: null,
            height: null,
            id: null,
            image: null,
            imageId: null,
            length: null,
            msrpPrice: null,
            outgoingUrl: null,
            price: null,
            primaryImageUrl: null,
            productImages: null,
            published: null,
            quantity: null,
            resourceUri: null,
            salePrice: null,
            shopId: null,
            sku: null,
            slug: null,
            status: null,
            title: null,
            upc: null,
            variations: null,
            vendor: null,
            weight: null,
            weightUnit: null,
            wholesalePrice: null,
            width: null,
        });
    }
}
class ProductVariationModel {
    constructor(data) {
        this.id = data.id;
        this.name = data.name;
        this.options = data.options;
        this.required = data.required;
        this.resourceUri = data.resourceUri;
        this.shop = data.shop;
        this.shopId = data.shopId;
    }
    static fromApi(data) {
        const options = data.options.map(o => ({
            id: o.id,
            name: o.name,
            order: o.order,
            price: o.price,
            variationId: o.variation_id
        }));
        return new ProductVariationModel({
            id: data.id,
            name: data.name,
            options,
            required: data.required,
            resourceUri: data.resource_uri,
            shop: data.shop,
            shopId: data.shop_id
        });
    }
    static empty() {
        return new ProductVariationModel({
            id: null,
            name: null,
            options: null,
            required: null,
            resourceUri: null,
            shop: null,
            shopId: null
        });
    }
}

class VariationModel {
    constructor(data) {
        this.id = data.id;
        this.name = data.name;
        this.options = data.options;
        this.required = data.required;
        this.shop = data.shop;
        this.shopId = data.shopId;
    }
    static fromApi(data) {
        const options = data.options.map(o => ({
            id: o.id,
            name: o.name,
            order: o.order,
            price: o.price,
            variationId: o.variation_id
        }));
        return new VariationModel({
            id: data.id,
            name: data.name,
            options,
            required: data.required,
            resourceUri: data.resource_uri,
            shop: data.shop,
            shopId: data.shop_id,
        });
    }
    static empty() {
        return new VariationModel({
            id: null,
            name: null,
            options: null,
            required: null,
            resourceUri: null,
            shop: null,
            shopId: null,
        });
    }
}

const STORE_REPOSITORY = new InjectionToken('StoreRepository');

let StoreRepository = class StoreRepository {
    constructor(appSettings, httpClient) {
        this.appSettings = appSettings;
        this.httpClient = httpClient;
    }
    // --------------------- CATEGORIES: -------------------------
    loadCategories(pagination = '') {
        return this.httpClient.get(`${this.getBaseUrl()}/categories${pagination}`);
    }
    loadCategoryById(id) {
        return this.httpClient.get(`${this.getBaseUrl()}/categories/${id}`);
    }
    createCategory(payload) {
        const body = this.createHttParams(payload);
        return this.httpClient.post(`${this.getBaseUrl()}/create_category`, body);
    }
    deleteCategory(id) {
        return this.httpClient.delete(`${this.getBaseUrl()}/delete_category/${id}`);
    }
    // --------------------- ORDERS: -------------------------
    loadOrders(pagination = '') {
        return this.httpClient.get(`${this.getBaseUrl()}/orders${pagination}`);
    }
    loadOrderById(id) {
        return this.httpClient.get(`${this.getBaseUrl()}/orders/${id}`);
    }
    updateOrder(id, payload) {
        const body = this.createHttParams(payload);
        return this.httpClient.post(`${this.getBaseUrl()}/update_order/${id}`, body);
    }
    // --------------------- PRODUCTS: -------------------------
    loadProducts() {
        return this.httpClient.get(`${this.getBaseUrl()}/products`);
    }
    loadProductById(id) {
        return this.httpClient.get(`${this.getBaseUrl()}/products/${id}`);
    }
    createProduct(payload) {
        const body = this.createHttParams(payload);
        return this.httpClient.post(`${this.getBaseUrl()}/create_product`, body);
    }
    updateProduct(id, payload) {
        const body = this.createHttParams(payload);
        return this.httpClient.put(`${this.getBaseUrl()}/update_product/${id}`, body);
    }
    deleteProduct(id) {
        return this.httpClient.delete(`${this.getBaseUrl()}/delete_product/${id}`);
    }
    // ---------------- PRODUCT VARIATIONS: ---------------------
    loadProductsVariations(pagination = '') {
        return this.httpClient.get(`${this.getBaseUrl()}/product_variations${pagination}`);
    }
    loadProductVariationByVariationId(id) {
        return this.httpClient.get(`${this.getBaseUrl()}/product_variations/${id}`);
    }
    createProductVariation(payload) {
        const body = this.createHttParams(payload);
        return this.httpClient.post(`${this.getBaseUrl()}/create_product_variation`, body);
    }
    deleteProductVariation(id) {
        return this.httpClient.delete(`${this.getBaseUrl()}/delete_product_variation/${id}`);
    }
    // --------------------- IMAGES: -------------------------
    loadSiteImages(pagination = '') {
        return this.httpClient.get(`${this.getBaseUrl()}/site_images${pagination}`);
    }
    loadSiteImageById(id) {
        return this.httpClient.get(`${this.getBaseUrl()}/site_images/${id}`);
    }
    createSiteImage(payload) {
        const body = this.createHttParams(payload);
        return this.httpClient.post(`${this.getBaseUrl()}/create_site_image`, body);
    }
    deleteSiteImage(id) {
        return this.httpClient.delete(`${this.getBaseUrl()}/delete_site_image/${id}`);
    }
    createHttParams(payload) {
        let params = new HttpParams();
        for (const key in payload) {
            if (payload.hasOwnProperty(key)) {
                (typeof payload[key] === 'object') ?
                    params = params.append(key, JSON.stringify(payload[key]))
                    :
                        params = params.append(key, payload[key]);
            }
        }
        return params.toString();
    }
    getBaseUrl() {
        return `${this.appSettings.baseUrl()}/api/${this.appSettings.instance()}/v1/shop`;
    }
};
StoreRepository.ctorParameters = () => [
    { type: AbstractAppConfigService, decorators: [{ type: Inject, args: [APP_CONFIG_SERVICE,] }] },
    { type: HttpClient }
];
StoreRepository = __decorate([
    Injectable(),
    __param(0, Inject(APP_CONFIG_SERVICE))
], StoreRepository);

const STORE_SERVICE = new InjectionToken('StoreService');

let StoreService = class StoreService {
    constructor(repository) {
        this.repository = repository;
    }
    // --------------------- CATEGORIES: -------------------------
    loadCategories() {
        return this.repository.loadCategories().pipe(map(response => response.data.map(c => CategoryModel.fromApi(c))), catchError(error => { throw error; }));
    }
    loadCategoryById(id) {
        return this.repository.loadCategoryById(id).pipe(map(response => CategoryModel.fromApi(response.data)), catchError(error => { throw error; }));
    }
    createCategory(payload) {
        return this.repository.createCategory(payload).pipe(map(response => CategoryModel.fromApi(response.data)), catchError(error => { throw error; }));
    }
    deleteCategory(payload) {
        return this.repository.deleteCategory(payload).pipe(map(resp => resp.status === 'success'), catchError(error => { throw error; }));
    }
    // --------------------- ORDERS: -------------------------
    loadOrders() {
        return this.repository.loadOrders().pipe(map(response => response.data.map(o => OrderModel.fromApi(o))), catchError(error => { throw error; }));
    }
    loadOrderById(id) {
        return this.repository.loadOrderById(id).pipe(map(response => OrderModel.fromApi(response.data)), catchError(error => { throw error; }));
    }
    updateOrder(id, payload) {
        return this.repository.updateOrder(id, payload).pipe(map(response => OrderModel.fromApi(response.data)), catchError(error => { throw error; }));
    }
    // --------------------- PRODUCTS: -------------------------
    loadProducts() {
        return this.repository.loadProducts().pipe(map(response => response.data.map(p => ProductModel.fromApi(p))), catchError(error => { throw error; }));
    }
    loadProductById(id) {
        return this.repository.loadProductById(id).pipe(map(response => ProductModel.fromApi(response.data)), catchError(error => { throw error; }));
    }
    createProduct(payload) {
        return this.repository.createProduct(payload).pipe(map(response => ProductModel.fromApi(response.data)), catchError(error => { throw error; }));
    }
    updateProduct(id, payload) {
        return this.repository.updateProduct(id, payload).pipe(map(response => ProductModel.fromApi(response.data)), catchError(error => { throw error; }));
    }
    deleteProduct(id) {
        return this.repository.deleteProduct(id).pipe(map(resp => resp.status === 'success'), catchError(error => { throw error; }));
    }
    // ---------------- PRODUCT VARIATIONS: ---------------------
    loadProductsVariations() {
        return this.repository.loadProductsVariations().pipe(map(resp => resp.data.map(v => VariationModel.fromApi(v))), catchError(error => { throw error; }));
    }
    loadProductVariationById(id) {
        return this.repository.loadProductVariationByVariationId(id).pipe(map(resp => VariationModel.fromApi(resp.data)), catchError(error => { throw error; }));
    }
    createProductVariation(payload) {
        return this.repository.createProductVariation(payload).pipe(map(resp => VariationModel.fromApi(resp.data)), catchError(error => { throw error; }));
    }
    deleteProductVariation(id) {
        return this.repository.deleteProductVariation(id).pipe(map(resp => resp.status === 'success'), catchError(error => { throw error; }));
    }
    // --------------------- IMAGES: -------------------------
    loadSiteImages() {
        return this.repository.loadSiteImages().pipe(map(resp => resp.data.map(i => ImageModel.fromApi(i))), catchError(error => { throw error; }));
    }
    loadSiteImageById(id) {
        throw new Error('Method not implemented');
    }
    createSiteImage(payload) {
        throw new Error('Method not implemented');
    }
    deleteSiteImage(id) {
        throw new Error('Method not implemented');
    }
};
StoreService.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [STORE_REPOSITORY,] }] }
];
StoreService.ɵprov = ɵɵdefineInjectable({ factory: function StoreService_Factory() { return new StoreService(ɵɵinject(STORE_REPOSITORY)); }, token: StoreService, providedIn: "root" });
StoreService = __decorate([
    Injectable({
        providedIn: 'root'
    }),
    __param(0, Inject(STORE_REPOSITORY))
], StoreService);

var StoreImagesActionTypes;
(function (StoreImagesActionTypes) {
    StoreImagesActionTypes["LoadImagesBegin"] = "[STORE] Load Images Begin";
    StoreImagesActionTypes["LoadImagesSuccess"] = "[STORE] Load Images Success";
    StoreImagesActionTypes["LoadImagesFail"] = "[STORE] Load Images Fail";
    StoreImagesActionTypes["LoadImageByIdBegin"] = "[STORE] Load Image By Id Begin";
    StoreImagesActionTypes["LoadImageByIdSuccess"] = "[STORE] Load Image By Id Success";
    StoreImagesActionTypes["LoadImageByIdFail"] = "[STORE] Load Image By Id Fail";
    StoreImagesActionTypes["CreateImageBegin"] = "[STORE] Create Image Begin";
    StoreImagesActionTypes["CreateImageSuccess"] = "[STORE] Create Image Success";
    StoreImagesActionTypes["CreateImageFail"] = "[STORE] Create Image Fail";
    StoreImagesActionTypes["DelteImageBegin"] = "[STORE] Load Delete  Begin";
    StoreImagesActionTypes["DelteImageSuccess"] = "[STORE] Load Delete Success";
    StoreImagesActionTypes["DelteImageFail"] = "[STORE] Delete Image Fail";
    StoreImagesActionTypes["ClearImagesSuccessErrorData"] = "[STORE] Clear Images Success/Error Data";
})(StoreImagesActionTypes || (StoreImagesActionTypes = {}));
const LoadImagesBeginAction = createAction(StoreImagesActionTypes.LoadImagesBegin);
const LoadImagesSuccessAction = createAction(StoreImagesActionTypes.LoadImagesSuccess, props());
const LoadImagesFailAction = createAction(StoreImagesActionTypes.LoadImagesFail, props());
const LoadImageByIdBeginAction = createAction(StoreImagesActionTypes.LoadImageByIdBegin, props());
const LoadImageByIdSuccessAction = createAction(StoreImagesActionTypes.LoadImageByIdSuccess, props());
const LoadImageByIdFailAction = createAction(StoreImagesActionTypes.LoadImageByIdFail, props());
const CreateImageBeginAction = createAction(StoreImagesActionTypes.CreateImageBegin, props());
const CreateImageSuccessAction = createAction(StoreImagesActionTypes.CreateImageSuccess, props());
const CreateImageFailAction = createAction(StoreImagesActionTypes.CreateImageFail, props());
const DeleteImageBeginAction = createAction(StoreImagesActionTypes.DelteImageBegin, props());
const DeleteImageSuccessAction = createAction(StoreImagesActionTypes.DelteImageSuccess, props());
const DeleteImageFailAction = createAction(StoreImagesActionTypes.DelteImageFail, props());
const ClearImagesSuccessErrorDataAction = createAction(StoreImagesActionTypes.ClearImagesSuccessErrorData);

var StoreOrdersActionTypes;
(function (StoreOrdersActionTypes) {
    StoreOrdersActionTypes["LoadOrdersBegin"] = "[STORE] Load Orders Begin";
    StoreOrdersActionTypes["LoadOrdersSuccess"] = "[STORE] Load Orders Success";
    StoreOrdersActionTypes["LoadOrdersFail"] = "[STORE] Load Orders Fail";
    StoreOrdersActionTypes["LoadOrderByIdBegin"] = "[STORE] Load Order By Id Begin";
    StoreOrdersActionTypes["LoadOrderByIdSuccess"] = "[STORE] Load Order By Id Success";
    StoreOrdersActionTypes["LoadOrderByIdFail"] = "[STORE] Load Order By Id Fail";
    StoreOrdersActionTypes["UpdateOrderBegin"] = "[STORE] Update Order Begin";
    StoreOrdersActionTypes["UpdateOrderSuccess"] = "[STORE] Update Order Success";
    StoreOrdersActionTypes["UpdateOrderFail"] = "[STORE] Update Order Fail";
    StoreOrdersActionTypes["ClearOrdersSuccessErrorData"] = "[STORE] Clear Orders Success/Error Data";
})(StoreOrdersActionTypes || (StoreOrdersActionTypes = {}));
const LoadOrdersBeginAction = createAction(StoreOrdersActionTypes.LoadOrdersBegin);
const LoadOrdersSuccessAction = createAction(StoreOrdersActionTypes.LoadOrdersSuccess, props());
const LoadOrdersFailAction = createAction(StoreOrdersActionTypes.LoadOrdersFail, props());
const LoadOrderByIdBeginAction = createAction(StoreOrdersActionTypes.LoadOrderByIdBegin, props());
const LoadOrderByIdSuccessAction = createAction(StoreOrdersActionTypes.LoadOrderByIdSuccess, props());
const LoadOrderByIdFailAction = createAction(StoreOrdersActionTypes.LoadOrderByIdFail, props());
const UpdateOrderBeginAction = createAction(StoreOrdersActionTypes.UpdateOrderBegin, props());
const UpdateOrderSuccessAction = createAction(StoreOrdersActionTypes.UpdateOrderSuccess, props());
const UpdateOrderFailAction = createAction(StoreOrdersActionTypes.UpdateOrderFail, props());
const ClearOrdersSuccessErrorDataAction = createAction(StoreOrdersActionTypes.ClearOrdersSuccessErrorData);

var StoreProductsActionTypes;
(function (StoreProductsActionTypes) {
    StoreProductsActionTypes["LoadProductsBegin"] = "[STORE] Load Products Begin";
    StoreProductsActionTypes["LoadProductsSuccess"] = "[STORE] Load Products Success";
    StoreProductsActionTypes["LoadProductsFail"] = "[STORE] Load Products Fail";
    StoreProductsActionTypes["LoadProductByIdBegin"] = "[STORE] Load Product By Id Begin";
    StoreProductsActionTypes["LoadProductByIdSuccess"] = "[STORE] Load Product By Id Success";
    StoreProductsActionTypes["LoadProductByIdFail"] = "[STORE] Load Product By Id Fail";
    StoreProductsActionTypes["CreateProductBegin"] = "[STORE] Create Product Begin";
    StoreProductsActionTypes["CreateProductSuccess"] = "[STORE] Create Product Success";
    StoreProductsActionTypes["CreateProductFail"] = "[STORE] Create Product Fail";
    StoreProductsActionTypes["UpdateProductBegin"] = "[STORE] Update Product Begin";
    StoreProductsActionTypes["UpdateProductSuccess"] = "[STORE] Update Product Success";
    StoreProductsActionTypes["UpdateProductFail"] = "[STORE] Update Product Fail";
    StoreProductsActionTypes["DeleteProductBegin"] = "[STORE] Delete Product Begin";
    StoreProductsActionTypes["DeleteProductSuccess"] = "[STORE] Delete Product Success";
    StoreProductsActionTypes["DeleteProductFail"] = "[STORE] Delete Product Fail";
    StoreProductsActionTypes["ClearProductsSuccessErrorData"] = "[STORE] Clear Products Success/Error Data";
})(StoreProductsActionTypes || (StoreProductsActionTypes = {}));
const LoadProductsBeginAction = createAction(StoreProductsActionTypes.LoadProductsBegin);
const LoadProductsSuccessAction = createAction(StoreProductsActionTypes.LoadProductsSuccess, props());
const LoadProductsFailAction = createAction(StoreProductsActionTypes.LoadProductsFail, props());
const LoadProductByIdBeginAction = createAction(StoreProductsActionTypes.LoadProductByIdBegin, props());
const LoadProductByIdSuccessAction = createAction(StoreProductsActionTypes.LoadProductByIdSuccess, props());
const LoadProductByIdFailAction = createAction(StoreProductsActionTypes.LoadProductByIdFail, props());
const CreateProductBeginAction = createAction(StoreProductsActionTypes.CreateProductBegin, props());
const CreateProductSuccessAction = createAction(StoreProductsActionTypes.CreateProductSuccess, props());
const CreateProductFailAction = createAction(StoreProductsActionTypes.CreateProductFail, props());
const UpdateProductBeginAction = createAction(StoreProductsActionTypes.UpdateProductBegin, props());
const UpdateProductSuccessAction = createAction(StoreProductsActionTypes.UpdateProductSuccess, props());
const UpdateProductFailAction = createAction(StoreProductsActionTypes.UpdateProductFail, props());
const DeleteProductBeginAction = createAction(StoreProductsActionTypes.DeleteProductBegin, props());
const DeleteProductSuccessAction = createAction(StoreProductsActionTypes.DeleteProductSuccess, props());
const DeleteProductFailAction = createAction(StoreProductsActionTypes.DeleteProductFail, props());
const ClearProductsSuccessErrorDataAction = createAction(StoreProductsActionTypes.ClearProductsSuccessErrorData);

var StoreVariationsActionTypes;
(function (StoreVariationsActionTypes) {
    StoreVariationsActionTypes["LoadVariationsBegin"] = "[STORE] Load Variations Begin";
    StoreVariationsActionTypes["LoadVariationsSuccess"] = "[STORE] Load Variations Success";
    StoreVariationsActionTypes["LoadVariationsFail"] = "[STORE] Load Variations Fail";
    StoreVariationsActionTypes["LoadVariationByIdBegin"] = "[STORE] Load Variation By Id Begin";
    StoreVariationsActionTypes["LoadVariationByIdSuccess"] = "[STORE] Load Variation By Id Success";
    StoreVariationsActionTypes["LoadVariationByIdFail"] = "[STORE] Load Variation By Id Fail";
    StoreVariationsActionTypes["CreateVariationBegin"] = "[STORE] Create Variation Begin";
    StoreVariationsActionTypes["CreateVariationSuccess"] = "[STORE] Create Variation Success";
    StoreVariationsActionTypes["CreateVariationFail"] = "[STORE] Create Variation Fail";
    StoreVariationsActionTypes["DeleteVariationBegin"] = "[STORE] Delete Variation Begin";
    StoreVariationsActionTypes["DeleteVariationSuccess"] = "[STORE] Delete Variation Success";
    StoreVariationsActionTypes["DeleteVariationFail"] = "[STORE] Delete Variation Fail";
    StoreVariationsActionTypes["ClearVariationsSuccessErrorData"] = "[STORE] Clear Variations Success/Error Data";
})(StoreVariationsActionTypes || (StoreVariationsActionTypes = {}));
const LoadVariationsBeginAction = createAction(StoreVariationsActionTypes.LoadVariationsBegin);
const LoadVariationsSuccessAction = createAction(StoreVariationsActionTypes.LoadVariationsSuccess, props());
const LoadVariationsFailAction = createAction(StoreVariationsActionTypes.LoadVariationsFail, props());
const LoadVariationByIdBeginAction = createAction(StoreVariationsActionTypes.LoadVariationByIdBegin, props());
const LoadVariationByIdSuccessAction = createAction(StoreVariationsActionTypes.LoadVariationByIdSuccess, props());
const LoadVariationByIdFailAction = createAction(StoreVariationsActionTypes.LoadVariationByIdFail, props());
const CreteVariationBeginAction = createAction(StoreVariationsActionTypes.CreateVariationBegin, props());
const CreteVariationSuccessAction = createAction(StoreVariationsActionTypes.CreateVariationSuccess, props());
const CreteVariataionFailAction = createAction(StoreVariationsActionTypes.CreateVariationFail, props());
const DelteVariationBeginAction = createAction(StoreVariationsActionTypes.DeleteVariationBegin, props());
const DelteVariationSuccessAction = createAction(StoreVariationsActionTypes.DeleteVariationSuccess, props());
const DelteVariationFailAction = createAction(StoreVariationsActionTypes.DeleteVariationFail, props());
const ClearVariationsSuccessErrorDataAction = createAction(StoreVariationsActionTypes.ClearVariationsSuccessErrorData);

var StoreCategoriesActionTypes;
(function (StoreCategoriesActionTypes) {
    StoreCategoriesActionTypes["LoadCategoriesBegin"] = "[STORE] Load Categories Begin";
    StoreCategoriesActionTypes["LoadCategoriesSuccess"] = "[STORE] Load Categories Success";
    StoreCategoriesActionTypes["LoadCategoriesFail"] = "[STORE] Load Categories Fail";
    StoreCategoriesActionTypes["LoadCategoryByIdBegin"] = "[STORE] Load LoadCategory By Id Begin";
    StoreCategoriesActionTypes["LoadCategoryByIdSuccess"] = "[STORE] Load Categorie By Id Success";
    StoreCategoriesActionTypes["LoadCategoryByIdFail"] = "[STORE] Load Categorie By Id Fail";
    StoreCategoriesActionTypes["CreateCategoryBegin"] = "[STORE] Create Category Begin";
    StoreCategoriesActionTypes["CreateCategorySuccess"] = "[STORE] Create Category Success";
    StoreCategoriesActionTypes["CreateCategoryFail"] = "[STORE] Create Category Fail";
    StoreCategoriesActionTypes["DeleteCategoryBegin"] = "[STORE] Delete Category Begin";
    StoreCategoriesActionTypes["DeleteCategorySuccess"] = "[STORE] Delete Category Success";
    StoreCategoriesActionTypes["DeleteCategoryFail"] = "[STORE] Delete Category Fail";
    StoreCategoriesActionTypes["ClearCategoriesSuccessErrorData"] = "[STORE] Clear Categories Success/Error Data";
})(StoreCategoriesActionTypes || (StoreCategoriesActionTypes = {}));
const LoadCategoriesBeginAction = createAction(StoreCategoriesActionTypes.LoadCategoriesBegin);
const LoadCategoriesSuccessAction = createAction(StoreCategoriesActionTypes.LoadCategoriesSuccess, props());
const LoadCategoriesFailAction = createAction(StoreCategoriesActionTypes.LoadCategoriesFail, props());
const LoadCategorieByIdBeginAction = createAction(StoreCategoriesActionTypes.LoadCategoryByIdBegin, props());
const LoadCategorieByIdSuccessAction = createAction(StoreCategoriesActionTypes.LoadCategoryByIdSuccess, props());
const LoadCategorieByIdFailAction = createAction(StoreCategoriesActionTypes.LoadCategoryByIdFail, props());
const CreateCategoryBeginAction = createAction(StoreCategoriesActionTypes.CreateCategoryBegin, props());
const CreateCategorySuccessAction = createAction(StoreCategoriesActionTypes.CreateCategorySuccess, props());
const CreateCategoryFailAction = createAction(StoreCategoriesActionTypes.CreateCategoryFail, props());
const DeleteCategoryBeginAction = createAction(StoreCategoriesActionTypes.DeleteCategoryBegin, props());
const DeleteCategorySuccessAction = createAction(StoreCategoriesActionTypes.DeleteCategorySuccess, props());
const DeleteCategoryFailAction = createAction(StoreCategoriesActionTypes.DeleteCategoryFail, props());
const ClearCategoriesSuccessErrorDataAction = createAction(StoreCategoriesActionTypes.ClearCategoriesSuccessErrorData);

const getStoreState = createFeatureSelector('store');
const ɵ0 = state => state;
// ------- STORE GLOBAL STATE:
const getStorePageState = createSelector(getStoreState, ɵ0);
const ɵ1 = state => state.categories;
// --------------------- CATEGORIES: -------------------------
const getStoreCategoriesState = createSelector(getStorePageState, ɵ1);
const ɵ2 = categories => categories.isLoading;
const getCategoriesIsloading = createSelector(getStoreCategoriesState, ɵ2);
const ɵ3 = categories => categories.data;
const getCategoriesData = createSelector(getStoreCategoriesState, ɵ3);
const ɵ4 = categories => categories.hasBeenFetched;
const getCategoriesHasBeenFetched = createSelector(getStoreCategoriesState, ɵ4);
const ɵ5 = categories => categories.error;
const getCategoriesError = createSelector(getStoreCategoriesState, ɵ5);
const ɵ6 = categories => categories.success;
const getCategoriesSuccess = createSelector(getStoreCategoriesState, ɵ6);
const ɵ7 = state => state.orders;
// --------------------- ORDERS: -------------------------
const getStoreOrdersState = createSelector(getStorePageState, ɵ7);
const ɵ8 = orders => orders.isLoading;
const getOrdersIsLoading = createSelector(getStoreOrdersState, ɵ8);
const ɵ9 = orders => orders.data;
const getOrdersData = createSelector(getStoreOrdersState, ɵ9);
const ɵ10 = orders => orders.hasBeenFetched;
const getOrdersHasBeenFetched = createSelector(getStoreOrdersState, ɵ10);
const ɵ11 = orders => orders.error;
const getOrdersError = createSelector(getStoreOrdersState, ɵ11);
const ɵ12 = orders => orders.success;
const getOrdersSuccess = createSelector(getStoreOrdersState, ɵ12);
const ɵ13 = state => state.products;
// --------------------- PRODUCTS: -------------------------
const getStoreProductsState = createSelector(getStorePageState, ɵ13);
const ɵ14 = products => products.isLoading;
const getStoreProductsIsLoading = createSelector(getStoreProductsState, ɵ14);
const ɵ15 = products => products.data;
const getStoreProductsData = createSelector(getStoreProductsState, ɵ15);
const ɵ16 = products => products.hasBeenFetched;
const getStoreProductsHasBeenFetched = createSelector(getStoreProductsState, ɵ16);
const ɵ17 = products => products.error;
const getStoreProductsError = createSelector(getStoreProductsState, ɵ17);
const ɵ18 = products => products.success;
const getStoreProductsSuccess = createSelector(getStoreProductsState, ɵ18);
const ɵ19 = state => state.variations;
// --------------------- VARIATIONS: -------------------------
const getStoreVariationsState = createSelector(getStorePageState, ɵ19);
const ɵ20 = variations => variations.isLoading;
const getStoreVariationsIsLoading = createSelector(getStoreVariationsState, ɵ20);
const ɵ21 = variations => variations.data;
const getStoreVariationsData = createSelector(getStoreVariationsState, ɵ21);
const ɵ22 = variations => variations.hasBeenFetched;
const getStoreVariationsHasBeenFetched = createSelector(getStoreVariationsState, ɵ22);
const ɵ23 = variations => variations.error;
const getStoreVariationsError = createSelector(getStoreVariationsState, ɵ23);
const ɵ24 = variations => variations.success;
const getStoreVariationsSuccess = createSelector(getStoreVariationsState, ɵ24);
const ɵ25 = state => state.images;
// --------------------- IMAGES: -------------------------
const getStoreImagesState = createSelector(getStorePageState, ɵ25);
const ɵ26 = images => images.isLoading;
const getStoreImagesIsLoading = createSelector(getStoreImagesState, ɵ26);
const ɵ27 = images => images.data;
const getStoreImagesData = createSelector(getStoreImagesState, ɵ27);
const ɵ28 = images => images.hasBeenFetched;
const getStoreImagesHasBeenFetched = createSelector(getStoreImagesState, ɵ28);
const ɵ29 = images => images.error;
const getStoreImagesError = createSelector(getStoreImagesState, ɵ29);
const ɵ30 = images => images.success;
const getStoreImagesSuccess = createSelector(getStoreImagesState, ɵ30);

let StorePageStore = class StorePageStore {
    constructor(store) {
        this.store = store;
        // ----------- CATEGORIES METHODS:
        this.LoadCategories = () => this.store.dispatch(LoadCategoriesBeginAction());
        this.loadCategoryById = (id) => this.store.dispatch(LoadCategorieByIdBeginAction({ id }));
        this.CreateCategory = (payload) => this.store.dispatch(CreateCategoryBeginAction({ payload }));
        this.DeleteCategory = (id) => this.store.dispatch(DeleteCategoryBeginAction({ id }));
        // ----------- ORDERS METHODS:
        this.LoadOrders = () => this.store.dispatch(LoadOrdersBeginAction());
        this.loadOrderById = (id) => this.store.dispatch(LoadOrderByIdBeginAction({ id }));
        this.updateOrder = (id, payload) => this.store.dispatch(UpdateOrderBeginAction({ id, payload }));
        // ----------- PRODUCTS METHODS:
        this.LoadProducts = () => this.store.dispatch(LoadProductsBeginAction());
        this.CreateProduct = (payload) => this.store.dispatch(CreateProductBeginAction({ payload }));
        this.UpdateProduct = (id, payload) => this.store.dispatch(UpdateProductBeginAction({ id, payload }));
        this.DeleteProduct = (id) => this.store.dispatch(DeleteProductBeginAction({ id }));
        // ----------- VARIATIONS METHODS:
        this.LoadVariations = () => this.store.dispatch(LoadVariationsBeginAction());
        this.CreateVariation = (payload) => this.store.dispatch(CreteVariationBeginAction({ payload }));
        this.DeleteVariation = (payload) => this.store.dispatch(DelteVariationBeginAction({ payload }));
        // ----------- IMAGE METHODS:
        this.LoadImages = () => this.store.dispatch(LoadImagesBeginAction());
        this.LoadImageById = (id) => this.store.dispatch(LoadImageByIdBeginAction({ id }));
        this.CreateImage = (payload) => this.store.dispatch(CreateImageBeginAction({ payload }));
        this.DeleteImage = (id) => this.store.dispatch(DeleteImageBeginAction({ id }));
    }
    get IsLoadingCategories$() { return this.store.select(getCategoriesIsloading); }
    get CategoriesData$() { return this.store.select(getCategoriesData); }
    get CategoriesHasBeenFetched$() { return this.store.select(getCategoriesHasBeenFetched); }
    get CategoriesError$() { return this.store.select(getCategoriesError); }
    get CategoriesSuccess$() { return this.store.select(getCategoriesSuccess); }
    get IsLoadingOrders$() { return this.store.select(getOrdersIsLoading); }
    get OrdersData$() { return this.store.select(getOrdersData); }
    get OrdersHasBeenFetched$() { return this.store.select(getOrdersHasBeenFetched); }
    get OrdersError$() { return this.store.select(getOrdersError); }
    get OrdersSuccess$() { return this.store.select(getOrdersSuccess); }
    get IsLoadingProducts$() { return this.store.select(getStoreProductsIsLoading); }
    get ProductsData$() { return this.store.select(getStoreProductsData); }
    get ProductsHasBeenFetched$() { return this.store.select(getStoreProductsHasBeenFetched); }
    get ProductsError$() { return this.store.select(getStoreProductsError); }
    get ProductsSuccess$() { return this.store.select(getStoreProductsSuccess); }
    get IsLoadingVariations$() { return this.store.select(getStoreVariationsIsLoading); }
    get VariationsData$() { return this.store.select(getStoreVariationsData); }
    get VariationsHasBeenFetched$() { return this.store.select(getStoreVariationsHasBeenFetched); }
    get VariationsError$() { return this.store.select(getStoreVariationsError); }
    get VariationsSuccess$() { return this.store.select(getStoreVariationsSuccess); }
    get IsLoadingImages$() { return this.store.select(getStoreImagesIsLoading); }
    get ImagesData$() { return this.store.select(getStoreImagesData); }
    get ImagesHasBeenFetched$() { return this.store.select(getStoreImagesHasBeenFetched); }
    get ImagesError$() { return this.store.select(getStoreImagesError); }
    get ImagesSuccess$() { return this.store.select(getStoreImagesSuccess); }
};
StorePageStore.ctorParameters = () => [
    { type: Store }
];
StorePageStore = __decorate([
    Injectable()
], StorePageStore);

let StoreEffects = class StoreEffects {
    constructor(actions, storePageStore, service) {
        this.actions = actions;
        this.storePageStore = storePageStore;
        this.service = service;
        // --------------------------- CATEGORIES: ---------------------------------
        this.LoadCategories$ = createEffect(() => this.actions.pipe(ofType(LoadCategoriesBeginAction), switchMap(() => {
            return this.service.loadCategories().pipe(flatMap(payload => [
                LoadCategoriesSuccessAction({ payload }),
                ClearCategoriesSuccessErrorDataAction()
            ]), catchError(errors => {
                console.error('StoreEffects.LoadCategories$ ERROR', errors);
                return of([
                    LoadCategoriesFailAction({ errors }),
                    ClearCategoriesSuccessErrorDataAction()
                ]).pipe(switchMap(d => d));
            }));
        })));
        this.LoadCategoryById$ = createEffect(() => this.actions.pipe(ofType(LoadCategorieByIdBeginAction), switchMap((action) => {
            return this.service.loadCategoryById(action.id).pipe(flatMap(payload => [
                LoadCategorieByIdSuccessAction({ payload }),
                ClearCategoriesSuccessErrorDataAction()
            ]), catchError(errors => {
                console.error('StoreEffects.LoadCategoryById$ ERROR', errors);
                return of([
                    LoadCategorieByIdFailAction({ errors }),
                    ClearCategoriesSuccessErrorDataAction()
                ]).pipe(switchMap(d => d));
            }));
        })));
        this.CreateCategory$ = createEffect(() => this.actions.pipe(ofType(CreateCategoryBeginAction), withLatestFrom(this.storePageStore.CategoriesData$), switchMap(([action, categoriesData]) => {
            return this.service.createCategory(action.payload).pipe(flatMap(data => {
                const payload = [data, ...categoriesData];
                return [
                    CreateCategorySuccessAction({ payload }),
                    ClearCategoriesSuccessErrorDataAction()
                ];
            }), catchError(errors => {
                console.error('StoreEffects.CreateCategory$ ERROR', errors);
                return of([
                    CreateCategoryFailAction({ errors }),
                    ClearCategoriesSuccessErrorDataAction()
                ]).pipe(switchMap(d => d));
            }));
        })));
        this.DeleteCategory$ = createEffect(() => this.actions.pipe(ofType(DeleteCategoryBeginAction), withLatestFrom(this.storePageStore.CategoriesData$), switchMap(([action, categoriesData]) => {
            return this.service.deleteCategory(action.id).pipe(flatMap(() => {
                const payload = categoriesData.filter(c => c.id !== action.id);
                return [
                    DeleteCategorySuccessAction({ payload }),
                    ClearCategoriesSuccessErrorDataAction()
                ];
            }), catchError(errors => {
                console.error('StoreEffects.DeleteCategory$ ERROR', errors);
                return of([
                    DeleteCategoryFailAction({ errors }),
                    ClearCategoriesSuccessErrorDataAction()
                ]).pipe(switchMap(d => d));
            }));
        })));
        // --------------------------- ORDERS: ---------------------------------
        this.LoadOrders$ = createEffect(() => this.actions.pipe(ofType(LoadOrdersBeginAction), switchMap(() => {
            return this.service.loadOrders().pipe(flatMap(data => [
                LoadOrdersSuccessAction({ payload: data }),
                ClearOrdersSuccessErrorDataAction()
            ]), catchError(errors => {
                console.error('StoreEffects.LoadOrders$ ERROR', errors);
                return of([
                    LoadOrdersFailAction({ errors }),
                    ClearOrdersSuccessErrorDataAction()
                ]).pipe(switchMap(d => d));
            }));
        })));
        this.LoadOrderById$ = createEffect(() => this.actions.pipe(ofType(LoadOrderByIdBeginAction), switchMap((action) => {
            return this.service.loadOrderById(action.id).pipe(flatMap(payload => [
                LoadOrderByIdSuccessAction({ payload }),
                ClearOrdersSuccessErrorDataAction()
            ]), catchError(errors => {
                console.error('StoreEffects.LoadOrderById$ ERROR', errors);
                return of([
                    LoadOrderByIdFailAction({ errors }),
                    ClearOrdersSuccessErrorDataAction()
                ]).pipe(switchMap(d => d));
            }));
        })));
        this.UpdateOrderById$ = createEffect(() => this.actions.pipe(ofType(UpdateOrderBeginAction), withLatestFrom(this.storePageStore.OrdersData$), switchMap(([action, ordersData]) => {
            return this.service.updateOrder(action.id, action.payload).pipe(flatMap(data => {
                const payload = [data, ...ordersData.filter(o => o.id === action.id)];
                return [
                    UpdateOrderSuccessAction({ payload }),
                    ClearOrdersSuccessErrorDataAction()
                ];
            }), catchError(errors => {
                console.error('StoreEffects.UpdateOrderById$ ERROR', errors);
                return of([
                    UpdateOrderFailAction({ errors }),
                    ClearOrdersSuccessErrorDataAction()
                ]).pipe(switchMap(d => d));
            }));
        })));
        // --------------------------- PRODUCTS: ---------------------------------
        this.LoadProducts$ = createEffect(() => this.actions.pipe(ofType(LoadProductsBeginAction), switchMap(() => {
            return this.service.loadProducts().pipe(flatMap(payload => [
                LoadProductsSuccessAction({ payload }),
                ClearProductsSuccessErrorDataAction()
            ]), catchError(errors => {
                console.error('StoreEffects.LoadProducts$ ERROR', errors);
                return of([
                    LoadProductsFailAction({ errors }),
                    ClearProductsSuccessErrorDataAction()
                ]).pipe(switchMap(d => d));
            }));
        })));
        this.LoadProductById$ = createEffect(() => this.actions.pipe(ofType(LoadProductByIdBeginAction), switchMap((action) => {
            return this.service.loadProductById(action.id).pipe(flatMap(payload => [
                LoadProductByIdSuccessAction({ payload }),
                ClearProductsSuccessErrorDataAction()
            ]), catchError(errors => {
                console.error('StoreEffects.LoadProductById$ ERROR', errors);
                return of([
                    LoadProductByIdFailAction({ errors }),
                    ClearProductsSuccessErrorDataAction()
                ]).pipe(switchMap(d => d));
            }));
        })));
        this.CreateProduct$ = createEffect(() => this.actions.pipe(ofType(CreateProductBeginAction), withLatestFrom(this.storePageStore.ProductsData$), switchMap(([action, productsData]) => {
            return this.service.createProduct(action.payload).pipe(flatMap(data => {
                const payload = [data, ...productsData];
                return [
                    CreateProductSuccessAction({ payload }),
                    ClearProductsSuccessErrorDataAction()
                ];
            }), catchError(errors => {
                console.error('StoreEffects.CreateProducts$ ERROR', errors);
                return of([
                    CreateProductFailAction({ errors }),
                    ClearProductsSuccessErrorDataAction()
                ]).pipe(switchMap(d => d));
            }));
        })));
        this.UpdateProduct$ = createEffect(() => this.actions.pipe(ofType(UpdateProductBeginAction), withLatestFrom(this.storePageStore.ProductsData$), switchMap(([action, productsData]) => {
            return this.service.updateProduct(action.id, action.payload).pipe(flatMap(data => {
                const payload = [data, ...productsData.filter(p => p.id !== action.id)];
                return [
                    UpdateProductSuccessAction({ payload }),
                    ClearProductsSuccessErrorDataAction()
                ];
            }), catchError(errors => {
                console.error('StoreEffects.UpdateProducts$ ERROR', errors);
                return of([
                    UpdateProductFailAction({ errors }),
                    ClearProductsSuccessErrorDataAction()
                ]).pipe(switchMap(d => d));
            }));
        })));
        this.DeleteProduct$ = createEffect(() => this.actions.pipe(ofType(DeleteProductBeginAction), withLatestFrom(this.storePageStore.ProductsData$), switchMap(([action, productsData]) => {
            return this.service.deleteProduct(action.id).pipe(flatMap(() => {
                const payload = productsData.filter(p => p.id !== action.id);
                return [
                    DeleteProductSuccessAction({ payload }),
                    ClearProductsSuccessErrorDataAction()
                ];
            }), catchError(errors => {
                console.error('StoreEffects.DeleteProduct$ ERROR', errors);
                return of([
                    DeleteProductFailAction({ errors }),
                    ClearProductsSuccessErrorDataAction()
                ]).pipe(switchMap(d => d));
            }));
        })));
        // --------------------------- PRODUCTS: ---------------------------------
        this.LoadVariations$ = createEffect(() => this.actions.pipe(ofType(LoadVariationsBeginAction), switchMap(() => {
            return this.service.loadProductsVariations().pipe(flatMap(data => [
                LoadVariationsSuccessAction({ payload: data }),
                ClearVariationsSuccessErrorDataAction()
            ]), catchError(errors => {
                console.error('StoreEffects.LoadVariations$ ERROR', errors);
                return of([
                    LoadVariationsFailAction({ errors }),
                    ClearVariationsSuccessErrorDataAction()
                ]).pipe(switchMap(d => d));
            }));
        })));
        this.LoadVariationById$ = createEffect(() => this.actions.pipe(ofType(LoadVariationByIdBeginAction), switchMap((action) => {
            return this.service.loadProductVariationById(action.id).pipe(flatMap(payload => [
                LoadVariationByIdSuccessAction({ payload }),
                ClearVariationsSuccessErrorDataAction()
            ]), catchError(errors => {
                console.error('StoreEffects.LoadVariationById$ ERROR', errors);
                return of([
                    LoadVariationByIdFailAction({ errors }),
                    ClearVariationsSuccessErrorDataAction()
                ]).pipe(switchMap(d => d));
            }));
        })));
        this.CreateVariation$ = createEffect(() => this.actions.pipe(ofType(CreteVariationBeginAction), withLatestFrom(this.storePageStore.VariationsData$), switchMap(([action, variationsData]) => {
            return this.service.createProductVariation(action.payload).pipe(flatMap(data => {
                const payload = [data, ...variationsData];
                return [
                    CreteVariationSuccessAction({ payload }),
                    ClearVariationsSuccessErrorDataAction()
                ];
            }), catchError(errors => {
                console.error('StoreEffects.CreateVariation$ ERROR', errors);
                return of([
                    CreteVariataionFailAction({ errors }),
                    ClearVariationsSuccessErrorDataAction()
                ]).pipe(switchMap(d => d));
            }));
        })));
        this.DeleteVariation$ = createEffect(() => this.actions.pipe(ofType(DelteVariationBeginAction), withLatestFrom(this.storePageStore.VariationsData$), switchMap(([action, variationsData]) => {
            return this.service.deleteProductVariation(action.id).pipe(flatMap(() => {
                const payload = variationsData.filter(v => v.id !== action.id);
                return [
                    DelteVariationSuccessAction({ payload }),
                    ClearVariationsSuccessErrorDataAction()
                ];
            }), catchError(errors => {
                console.error('StoreEffects.DeleteVariation$ ERROR', errors);
                return of([
                    DelteVariationFailAction({ errors }),
                    ClearVariationsSuccessErrorDataAction()
                ]).pipe(switchMap(d => d));
            }));
        })));
        // --------------------------- IMAGES: ---------------------------------
        this.LoadImages$ = createEffect(() => this.actions.pipe(ofType(LoadImagesBeginAction), switchMap(() => {
            return this.service.loadSiteImages().pipe(flatMap(payload => [
                LoadImagesSuccessAction({ payload }),
                ClearImagesSuccessErrorDataAction()
            ]), catchError(errors => {
                console.error('StoreEffects.LoadImages$ ERROR', errors);
                return of([
                    LoadImagesFailAction({ errors }),
                    ClearImagesSuccessErrorDataAction()
                ]).pipe(switchMap(d => d));
            }));
        })));
        this.LoadImageById$ = createEffect(() => this.actions.pipe(ofType(LoadImageByIdBeginAction), switchMap((action) => {
            return this.service.loadSiteImageById(action.id).pipe(flatMap(payload => [
                LoadImageByIdSuccessAction({ payload }),
                ClearImagesSuccessErrorDataAction()
            ]), catchError(errors => {
                console.error('StoreEffects.LoadImageById$ ERROR', errors);
                return of([
                    LoadImageByIdFailAction({ errors }),
                    ClearImagesSuccessErrorDataAction()
                ]).pipe(switchMap(d => d));
            }));
        })));
        this.CreateImage$ = createEffect(() => this.actions.pipe(ofType(CreateImageBeginAction), withLatestFrom(this.storePageStore.ImagesData$), switchMap(([action, imagesData]) => {
            return this.service.createSiteImage(action.payload).pipe(flatMap(data => {
                const payload = [data, ...imagesData];
                return [
                    CreateImageSuccessAction({ payload }),
                    ClearImagesSuccessErrorDataAction()
                ];
            }), catchError(errors => {
                console.error('StoreEffects.CreateImage$ ERROR', errors);
                return of([
                    CreateImageFailAction({ errors }),
                    ClearImagesSuccessErrorDataAction()
                ]).pipe(switchMap(d => d));
            }));
        })));
        this.DeleteImage$ = createEffect(() => this.actions.pipe(ofType(DeleteImageBeginAction), withLatestFrom(this.storePageStore.ImagesData$), switchMap(([action, imagesData]) => {
            return this.service.deleteSiteImage(action.id).pipe(flatMap(() => {
                const payload = imagesData.filter(i => i.id !== action.id);
                return [
                    DeleteImageSuccessAction({ payload }),
                    ClearImagesSuccessErrorDataAction()
                ];
            }), catchError(errors => {
                console.error('StoreEffects.DeleteImage$ ERROR', errors);
                return of([
                    DeleteImageFailAction({ errors }),
                    ClearImagesSuccessErrorDataAction()
                ]).pipe(switchMap(d => d));
            }));
        })));
    }
};
StoreEffects.ctorParameters = () => [
    { type: Actions },
    { type: StorePageStore },
    { type: undefined, decorators: [{ type: Inject, args: [STORE_SERVICE,] }] }
];
StoreEffects = __decorate([
    Injectable(),
    __param(2, Inject(STORE_SERVICE))
], StoreEffects);

const ɵ0$1 = [], ɵ1$1 = [], ɵ2$1 = [], ɵ3$1 = [], ɵ4$1 = [];
const initialState = {
    categories: {
        isLoading: false,
        data: ɵ0$1,
        selectedCategory: null,
        hasBeenFetched: false,
        error: null,
        success: null
    },
    orders: {
        isLoading: false,
        data: ɵ1$1,
        selectedOrder: null,
        hasBeenFetched: false,
        error: null,
        success: null
    },
    products: {
        isLoading: false,
        data: ɵ2$1,
        selectedProduct: null,
        hasBeenFetched: false,
        error: null,
        success: null
    },
    variations: {
        isLoading: false,
        data: ɵ3$1,
        selectedVariation: null,
        hasBeenFetched: false,
        error: null,
        success: null
    },
    images: {
        isLoading: false,
        data: ɵ4$1,
        selectedImage: null,
        hasBeenFetched: false,
        error: null,
        success: null
    }
};
const ɵ5$1 = (state, action) => (Object.assign(Object.assign({}, state), { categories: Object.assign(Object.assign({}, state.categories), { isLoading: true, error: null, success: null }) })), ɵ6$1 = (state, action) => (Object.assign(Object.assign({}, state), { categories: Object.assign(Object.assign({}, state.categories), { isLoading: false, hasBeenFetched: true, data: action.payload, error: null, success: { after: getStoreCategoriesActionType(action.type) } }) })), ɵ7$1 = (state, action) => (Object.assign(Object.assign({}, state), { categories: Object.assign(Object.assign({}, state.categories), { isLoading: false, hasBeenFetched: true, selectedCategory: action.payload, error: null, success: { after: getStoreCategoriesActionType(action.type) } }) })), ɵ8$1 = (state, action) => (Object.assign(Object.assign({}, state), { categories: Object.assign(Object.assign({}, state.categories), { isLoading: false, error: { after: getStoreCategoriesActionType(action.type), error: action.errors } }) })), ɵ9$1 = (state) => (Object.assign(Object.assign({}, state), { categories: Object.assign(Object.assign({}, state.categories), { error: null, success: null }) })), ɵ10$1 = (state, action) => (Object.assign(Object.assign({}, state), { orders: Object.assign(Object.assign({}, state.orders), { isLoading: true, error: null, success: null }) })), ɵ11$1 = (state, action) => (Object.assign(Object.assign({}, state), { orders: Object.assign(Object.assign({}, state.orders), { isLoading: false, hasBeenFetched: true, data: action.payload, error: null, success: { after: getStoreOrdersActionType(action.type) } }) })), ɵ12$1 = (state, action) => (Object.assign(Object.assign({}, state), { orders: Object.assign(Object.assign({}, state.orders), { isLoading: false, hasBeenFetched: true, selectedOrder: action.payload, error: null, success: { after: getStoreOrdersActionType(action.type) } }) })), ɵ13$1 = (state, action) => (Object.assign(Object.assign({}, state), { orders: Object.assign(Object.assign({}, state.orders), { isLoading: false, error: { after: getStoreOrdersActionType(action.type), error: action.errors }, success: null }) })), ɵ14$1 = (state) => (Object.assign(Object.assign({}, state), { orders: Object.assign(Object.assign({}, state.orders), { error: null, success: null }) })), ɵ15$1 = (state, action) => (Object.assign(Object.assign({}, state), { products: Object.assign(Object.assign({}, state.products), { isLoading: true, error: null, success: null }) })), ɵ16$1 = (state, action) => (Object.assign(Object.assign({}, state), { products: Object.assign(Object.assign({}, state.products), { isLoading: false, hasBeenFetched: true, data: action.payload, error: null, success: { after: getStoreProductsActionType(action.type) } }) })), ɵ17$1 = (state, action) => (Object.assign(Object.assign({}, state), { products: Object.assign(Object.assign({}, state.products), { isLoading: false, selectedProduct: action.payload, error: null, success: { after: getStoreProductsActionType(action.type) } }) })), ɵ18$1 = (state, action) => (Object.assign(Object.assign({}, state), { products: Object.assign(Object.assign({}, state.products), { isLoading: false, error: { after: getStoreProductsActionType(action.type), error: action.errors }, success: null }) })), ɵ19$1 = (state) => (Object.assign(Object.assign({}, state), { products: Object.assign(Object.assign({}, state.products), { error: null, success: null }) })), ɵ20$1 = (state, action) => (Object.assign(Object.assign({}, state), { variations: Object.assign(Object.assign({}, state.variations), { isLoading: true, error: null, success: null }) })), ɵ21$1 = (state, action) => (Object.assign(Object.assign({}, state), { variations: Object.assign(Object.assign({}, state.variations), { isLoading: false, hasBeenFetched: true, data: action.payload, error: null, success: { after: getStoreVariationsActionType(action.type) } }) })), ɵ22$1 = (state, action) => (Object.assign(Object.assign({}, state), { variations: Object.assign(Object.assign({}, state.variations), { isLoading: false, hasBeenFetched: true, selectedVariation: action.payload, error: null, success: { after: getStoreVariationsActionType(action.type) } }) })), ɵ23$1 = (state, action) => (Object.assign(Object.assign({}, state), { variations: Object.assign(Object.assign({}, state.variations), { isLoading: false, error: { after: getStoreVariationsActionType(action.type), error: action.errors }, success: null }) })), ɵ24$1 = (state) => (Object.assign(Object.assign({}, state), { variations: Object.assign(Object.assign({}, state.variations), { error: null, success: null }) })), ɵ25$1 = (state, action) => (Object.assign(Object.assign({}, state), { images: Object.assign(Object.assign({}, state.images), { isLoading: true, error: null, success: null }) })), ɵ26$1 = (state, action) => (Object.assign(Object.assign({}, state), { images: Object.assign(Object.assign({}, state.images), { isLoading: false, hasBeenFetched: true, data: action.payload, error: null, success: { after: getStoreImagesActionType(action.type) } }) })), ɵ27$1 = (state, action) => (Object.assign(Object.assign({}, state), { images: Object.assign(Object.assign({}, state.images), { isLoading: false, selectedImage: action.payload, error: null, success: { after: getStoreImagesActionType(action.type) } }) })), ɵ28$1 = (state, action) => (Object.assign(Object.assign({}, state), { images: Object.assign(Object.assign({}, state.images), { isLoading: false, error: { after: getStoreImagesActionType(action.type), error: action.errors }, success: null }) })), ɵ29$1 = (state) => (Object.assign(Object.assign({}, state), { images: Object.assign(Object.assign({}, state.images), { error: null, success: null }) }));
const reducer = createReducer(initialState, 
// ------------------ CATEGORIES: ---------------------
on(LoadCategoriesBeginAction, LoadCategorieByIdBeginAction, CreateCategoryBeginAction, DeleteCategoryBeginAction, ɵ5$1), on(LoadCategoriesSuccessAction, CreateCategorySuccessAction, DeleteCategorySuccessAction, ɵ6$1), on(LoadCategorieByIdSuccessAction, ɵ7$1), on(LoadCategoriesFailAction, LoadCategorieByIdFailAction, CreateCategoryFailAction, DeleteCategoryFailAction, ɵ8$1), on(ClearCategoriesSuccessErrorDataAction, ɵ9$1), 
// ------------------ ORDERS: ---------------------
on(LoadOrdersBeginAction, LoadOrderByIdBeginAction, UpdateOrderBeginAction, ɵ10$1), on(LoadOrdersSuccessAction, UpdateOrderSuccessAction, ɵ11$1), on(LoadOrderByIdSuccessAction, ɵ12$1), on(LoadOrdersFailAction, LoadOrdersFailAction, UpdateOrderFailAction, ɵ13$1), on(ClearOrdersSuccessErrorDataAction, ɵ14$1), 
// ------------------ PRODUCTS: ---------------------
on(LoadProductsBeginAction, LoadProductByIdBeginAction, CreateProductBeginAction, UpdateProductBeginAction, DeleteProductBeginAction, ɵ15$1), on(LoadProductsSuccessAction, CreateProductSuccessAction, UpdateProductSuccessAction, DeleteProductSuccessAction, ɵ16$1), on(LoadProductByIdSuccessAction, ɵ17$1), on(LoadProductsFailAction, LoadProductByIdFailAction, CreateProductFailAction, UpdateProductFailAction, DeleteProductFailAction, ɵ18$1), on(ClearProductsSuccessErrorDataAction, ɵ19$1), 
// ------------------ VARIATIONS: ---------------------
on(LoadVariationsBeginAction, LoadVariationByIdBeginAction, CreteVariationBeginAction, DelteVariationBeginAction, ɵ20$1), on(LoadVariationsSuccessAction, CreteVariationSuccessAction, DelteVariationSuccessAction, ɵ21$1), on(LoadVariationByIdSuccessAction, ɵ22$1), on(LoadVariationsFailAction, LoadVariationByIdFailAction, CreteVariataionFailAction, DelteVariationFailAction, ɵ23$1), on(ClearVariationsSuccessErrorDataAction, ɵ24$1), 
// ------------------ IMAGES: ---------------------
on(LoadImagesBeginAction, LoadImageByIdBeginAction, CreateImageBeginAction, DeleteImageBeginAction, ɵ25$1), on(LoadImagesSuccessAction, CreateImageSuccessAction, DeleteImageSuccessAction, ɵ26$1), on(LoadImageByIdSuccessAction, ɵ27$1), on(LoadImagesFailAction, LoadImageByIdFailAction, CreateImageFailAction, DeleteImageFailAction, ɵ28$1), on(ClearImagesSuccessErrorDataAction, ɵ29$1));
function getStoreCategoriesActionType(type) {
    let action;
    switch (type) {
        case StoreCategoriesActionTypes.LoadCategoriesSuccess:
        case StoreCategoriesActionTypes.LoadCategoriesFail:
            action = 'LOAD_CATEGORIES';
            break;
        case StoreCategoriesActionTypes.CreateCategorySuccess:
        case StoreCategoriesActionTypes.CreateCategoryFail:
            action = 'CREATE_CATEGORY';
            break;
        case StoreCategoriesActionTypes.DeleteCategorySuccess:
        case StoreCategoriesActionTypes.DeleteCategoryFail:
            action = 'DELETE_CATEGORY';
            break;
        default:
            action = 'UNKNOWN';
    }
    return action;
}
function getStoreOrdersActionType(type) {
    let action;
    switch (type) {
        case StoreOrdersActionTypes.LoadOrdersSuccess:
        case StoreOrdersActionTypes.LoadOrdersFail:
            action = 'LOAD_ORDERS';
            break;
        case StoreOrdersActionTypes.UpdateOrderSuccess:
        case StoreOrdersActionTypes.UpdateOrderFail:
            action = 'UPDATE_ORDER';
            break;
        default:
            action = 'UNKNOWN';
    }
    return action;
}
function getStoreProductsActionType(type) {
    let action;
    switch (type) {
        case StoreProductsActionTypes.LoadProductsSuccess:
        case StoreProductsActionTypes.LoadProductsFail:
            action = 'LOAD_PRODUCTS';
            break;
        case StoreProductsActionTypes.CreateProductSuccess:
        case StoreProductsActionTypes.CreateProductFail:
            action = 'CREATE_PRODUCT';
            break;
        case StoreProductsActionTypes.UpdateProductSuccess:
        case StoreProductsActionTypes.UpdateProductFail:
            action = 'UPDATE_PRODUCT';
            break;
        case StoreProductsActionTypes.DeleteProductSuccess:
        case StoreProductsActionTypes.DeleteProductFail:
            action = 'DELETE_PRODUCT';
            break;
        default:
            action = 'UNKNOWN';
    }
    return action;
}
function getStoreVariationsActionType(type) {
    let action;
    switch (type) {
        case StoreVariationsActionTypes.LoadVariationsSuccess:
        case StoreVariationsActionTypes.LoadVariationsFail:
            action = 'LOAD_VARIATIONS';
            break;
        case StoreVariationsActionTypes.CreateVariationSuccess:
        case StoreVariationsActionTypes.CreateVariationFail:
            action = 'CREATE_VARIATION';
            break;
        case StoreVariationsActionTypes.DeleteVariationSuccess:
        case StoreVariationsActionTypes.DeleteVariationFail:
            action = 'DELETE_VARIATION';
            break;
        default:
            action = 'UNKNOWN';
    }
    return action;
}
function getStoreImagesActionType(type) {
    let action;
    switch (type) {
        case StoreImagesActionTypes.LoadImagesSuccess:
        case StoreImagesActionTypes.LoadImagesFail:
        case StoreImagesActionTypes.LoadImageByIdSuccess:
        case StoreImagesActionTypes.LoadImageByIdFail:
            action = 'LOAD_IMAGES';
            break;
        case StoreImagesActionTypes.CreateImageBegin:
        case StoreImagesActionTypes.CreateImageFail:
            action = 'CREATE_IMAGE';
            break;
        case StoreImagesActionTypes.DelteImageSuccess:
        case StoreImagesActionTypes.DelteImageFail:
            action = 'DELETE_IMAGE';
            break;
        default:
            action = 'UNKNOWN';
    }
    return action;
}
function storeReducer(state, action) {
    return reducer(state, action);
}

var StoreCoreModule_1;
let StoreCoreModule = StoreCoreModule_1 = class StoreCoreModule {
    static forRoot(config) {
        return {
            ngModule: StoreCoreModule_1,
            providers: [
                { provide: STORE_SERVICE, useClass: StoreService },
                { provide: STORE_REPOSITORY, useClass: StoreRepository },
                ...config.providers,
                StorePageStore
            ]
        };
    }
};
StoreCoreModule = StoreCoreModule_1 = __decorate([
    NgModule({
        declarations: [],
        imports: [
            HttpClientModule,
            StoreModule.forFeature('store', storeReducer),
            EffectsModule.forFeature([StoreEffects]),
        ],
        exports: []
    })
], StoreCoreModule);

/*
 * Public API Surface of store-core
 */

/**
 * Generated bundle index. Do not edit.
 */

export { CategoryModel, ClearImagesSuccessErrorDataAction, ClearOrdersSuccessErrorDataAction, ClearProductsSuccessErrorDataAction, ClearVariationsSuccessErrorDataAction, CreateImageBeginAction, CreateImageFailAction, CreateImageSuccessAction, CreateProductBeginAction, CreateProductFailAction, CreateProductSuccessAction, CreteVariataionFailAction, CreteVariationBeginAction, CreteVariationSuccessAction, DeleteImageBeginAction, DeleteImageFailAction, DeleteImageSuccessAction, DeleteProductBeginAction, DeleteProductFailAction, DeleteProductSuccessAction, DelteVariationBeginAction, DelteVariationFailAction, DelteVariationSuccessAction, ImageModel, LoadImageByIdBeginAction, LoadImageByIdFailAction, LoadImageByIdSuccessAction, LoadImagesBeginAction, LoadImagesFailAction, LoadImagesSuccessAction, LoadOrderByIdBeginAction, LoadOrderByIdFailAction, LoadOrderByIdSuccessAction, LoadOrdersBeginAction, LoadOrdersFailAction, LoadOrdersSuccessAction, LoadProductByIdBeginAction, LoadProductByIdFailAction, LoadProductByIdSuccessAction, LoadProductsBeginAction, LoadProductsFailAction, LoadProductsSuccessAction, LoadVariationByIdBeginAction, LoadVariationByIdFailAction, LoadVariationByIdSuccessAction, LoadVariationsBeginAction, LoadVariationsFailAction, LoadVariationsSuccessAction, OrderModel, ProductModel, ProductVariationModel, STORE_REPOSITORY, STORE_SERVICE, StoreCoreModule, StoreEffects, StoreImagesActionTypes, StoreOrdersActionTypes, StorePageStore, StoreProductsActionTypes, StoreRepository, StoreService, StoreVariationsActionTypes, UpdateOrderBeginAction, UpdateOrderFailAction, UpdateOrderSuccessAction, UpdateProductBeginAction, UpdateProductFailAction, UpdateProductSuccessAction, VariationModel, getCategoriesData, getCategoriesError, getCategoriesHasBeenFetched, getCategoriesIsloading, getCategoriesSuccess, getOrdersData, getOrdersError, getOrdersHasBeenFetched, getOrdersIsLoading, getOrdersSuccess, getStoreCategoriesState, getStoreImagesData, getStoreImagesError, getStoreImagesHasBeenFetched, getStoreImagesIsLoading, getStoreImagesState, getStoreImagesSuccess, getStoreOrdersState, getStorePageState, getStoreProductsData, getStoreProductsError, getStoreProductsHasBeenFetched, getStoreProductsIsLoading, getStoreProductsState, getStoreProductsSuccess, getStoreState, getStoreVariationsData, getStoreVariationsError, getStoreVariationsHasBeenFetched, getStoreVariationsIsLoading, getStoreVariationsState, getStoreVariationsSuccess, initialState, storeReducer, ɵ0, ɵ1, ɵ10, ɵ11, ɵ12, ɵ13, ɵ14, ɵ15, ɵ16, ɵ17, ɵ18, ɵ19, ɵ2, ɵ20, ɵ21, ɵ22, ɵ23, ɵ24, ɵ25, ɵ26, ɵ27, ɵ28, ɵ29, ɵ3, ɵ30, ɵ4, ɵ5, ɵ6, ɵ7, ɵ8, ɵ9, StoreCategoriesActionTypes as ɵa, LoadCategoriesBeginAction as ɵb, LoadCategoriesSuccessAction as ɵc, LoadCategoriesFailAction as ɵd, LoadCategorieByIdBeginAction as ɵe, LoadCategorieByIdSuccessAction as ɵf, LoadCategorieByIdFailAction as ɵg, CreateCategoryBeginAction as ɵh, CreateCategorySuccessAction as ɵi, CreateCategoryFailAction as ɵj, DeleteCategoryBeginAction as ɵk, DeleteCategorySuccessAction as ɵl, DeleteCategoryFailAction as ɵm, ClearCategoriesSuccessErrorDataAction as ɵn };
//# sourceMappingURL=boxx-store-core.js.map
