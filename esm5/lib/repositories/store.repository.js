import { __decorate, __param } from "tslib";
import { HttpClient, HttpParams } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { AbstractAppConfigService, APP_CONFIG_SERVICE, IHttpBasicResponse } from '@boxx/core';
var StoreRepository = /** @class */ (function () {
    function StoreRepository(appSettings, httpClient) {
        this.appSettings = appSettings;
        this.httpClient = httpClient;
    }
    // --------------------- CATEGORIES: -------------------------
    StoreRepository.prototype.loadCategories = function (pagination) {
        if (pagination === void 0) { pagination = ''; }
        return this.httpClient.get(this.getBaseUrl() + "/categories" + pagination);
    };
    StoreRepository.prototype.loadCategoryById = function (id) {
        return this.httpClient.get(this.getBaseUrl() + "/categories/" + id);
    };
    StoreRepository.prototype.createCategory = function (payload) {
        var body = this.createHttParams(payload);
        return this.httpClient.post(this.getBaseUrl() + "/create_category", body);
    };
    StoreRepository.prototype.deleteCategory = function (id) {
        return this.httpClient.delete(this.getBaseUrl() + "/delete_category/" + id);
    };
    // --------------------- ORDERS: -------------------------
    StoreRepository.prototype.loadOrders = function (pagination) {
        if (pagination === void 0) { pagination = ''; }
        return this.httpClient.get(this.getBaseUrl() + "/orders" + pagination);
    };
    StoreRepository.prototype.loadOrderById = function (id) {
        return this.httpClient.get(this.getBaseUrl() + "/orders/" + id);
    };
    StoreRepository.prototype.updateOrder = function (id, payload) {
        var body = this.createHttParams(payload);
        return this.httpClient.post(this.getBaseUrl() + "/update_order/" + id, body);
    };
    // --------------------- PRODUCTS: -------------------------
    StoreRepository.prototype.loadProducts = function () {
        return this.httpClient.get(this.getBaseUrl() + "/products");
    };
    StoreRepository.prototype.loadProductById = function (id) {
        return this.httpClient.get(this.getBaseUrl() + "/products/" + id);
    };
    StoreRepository.prototype.createProduct = function (payload) {
        var body = this.createHttParams(payload);
        return this.httpClient.post(this.getBaseUrl() + "/create_product", body);
    };
    StoreRepository.prototype.updateProduct = function (id, payload) {
        var body = this.createHttParams(payload);
        return this.httpClient.put(this.getBaseUrl() + "/update_product/" + id, body);
    };
    StoreRepository.prototype.deleteProduct = function (id) {
        return this.httpClient.delete(this.getBaseUrl() + "/delete_product/" + id);
    };
    // ---------------- PRODUCT VARIATIONS: ---------------------
    StoreRepository.prototype.loadProductsVariations = function (pagination) {
        if (pagination === void 0) { pagination = ''; }
        return this.httpClient.get(this.getBaseUrl() + "/product_variations" + pagination);
    };
    StoreRepository.prototype.loadProductVariationByVariationId = function (id) {
        return this.httpClient.get(this.getBaseUrl() + "/product_variations/" + id);
    };
    StoreRepository.prototype.createProductVariation = function (payload) {
        var body = this.createHttParams(payload);
        return this.httpClient.post(this.getBaseUrl() + "/create_product_variation", body);
    };
    StoreRepository.prototype.deleteProductVariation = function (id) {
        return this.httpClient.delete(this.getBaseUrl() + "/delete_product_variation/" + id);
    };
    // --------------------- IMAGES: -------------------------
    StoreRepository.prototype.loadSiteImages = function (pagination) {
        if (pagination === void 0) { pagination = ''; }
        return this.httpClient.get(this.getBaseUrl() + "/site_images" + pagination);
    };
    StoreRepository.prototype.loadSiteImageById = function (id) {
        return this.httpClient.get(this.getBaseUrl() + "/site_images/" + id);
    };
    StoreRepository.prototype.createSiteImage = function (payload) {
        var body = this.createHttParams(payload);
        return this.httpClient.post(this.getBaseUrl() + "/create_site_image", body);
    };
    StoreRepository.prototype.deleteSiteImage = function (id) {
        return this.httpClient.delete(this.getBaseUrl() + "/delete_site_image/" + id);
    };
    StoreRepository.prototype.createHttParams = function (payload) {
        var params = new HttpParams();
        for (var key in payload) {
            if (payload.hasOwnProperty(key)) {
                (typeof payload[key] === 'object') ?
                    params = params.append(key, JSON.stringify(payload[key]))
                    :
                        params = params.append(key, payload[key]);
            }
        }
        return params.toString();
    };
    StoreRepository.prototype.getBaseUrl = function () {
        return this.appSettings.baseUrl() + "/api/" + this.appSettings.instance() + "/v1/shop";
    };
    StoreRepository.ctorParameters = function () { return [
        { type: AbstractAppConfigService, decorators: [{ type: Inject, args: [APP_CONFIG_SERVICE,] }] },
        { type: HttpClient }
    ]; };
    StoreRepository = __decorate([
        Injectable(),
        __param(0, Inject(APP_CONFIG_SERVICE))
    ], StoreRepository);
    return StoreRepository;
}());
export { StoreRepository };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RvcmUucmVwb3NpdG9yeS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bib3h4L3N0b3JlLWNvcmUvIiwic291cmNlcyI6WyJsaWIvcmVwb3NpdG9yaWVzL3N0b3JlLnJlcG9zaXRvcnkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDOUQsT0FBTyxFQUFFLE1BQU0sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDbkQsT0FBTyxFQUFFLHdCQUF3QixFQUFFLGtCQUFrQixFQUFFLGtCQUFrQixFQUFFLE1BQU0sWUFBWSxDQUFDO0FBTTlGO0lBQ0kseUJBQ3dDLFdBQXFDLEVBQ2pFLFVBQXNCO1FBRE0sZ0JBQVcsR0FBWCxXQUFXLENBQTBCO1FBQ2pFLGVBQVUsR0FBVixVQUFVLENBQVk7SUFDOUIsQ0FBQztJQUVMLDhEQUE4RDtJQUM5RCx3Q0FBYyxHQUFkLFVBQWUsVUFBdUI7UUFBdkIsMkJBQUEsRUFBQSxlQUF1QjtRQUNsQyxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUE2QyxJQUFJLENBQUMsVUFBVSxFQUFFLG1CQUFjLFVBQVksQ0FBQyxDQUFDO0lBQ3hILENBQUM7SUFDRCwwQ0FBZ0IsR0FBaEIsVUFBaUIsRUFBVTtRQUN2QixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUEyQyxJQUFJLENBQUMsVUFBVSxFQUFFLG9CQUFlLEVBQUksQ0FBQyxDQUFDO0lBQy9HLENBQUM7SUFDRCx3Q0FBYyxHQUFkLFVBQWUsT0FBK0I7UUFDMUMsSUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUMzQyxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUEyQyxJQUFJLENBQUMsVUFBVSxFQUFFLHFCQUFrQixFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ3JILENBQUM7SUFDRCx3Q0FBYyxHQUFkLFVBQWUsRUFBVTtRQUNyQixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUE4QixJQUFJLENBQUMsVUFBVSxFQUFFLHlCQUFvQixFQUFJLENBQUMsQ0FBQztJQUMxRyxDQUFDO0lBRUQsMERBQTBEO0lBQzFELG9DQUFVLEdBQVYsVUFBVyxVQUF1QjtRQUF2QiwyQkFBQSxFQUFBLGVBQXVCO1FBQzlCLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQTBDLElBQUksQ0FBQyxVQUFVLEVBQUUsZUFBVSxVQUFZLENBQUMsQ0FBQztJQUNqSCxDQUFDO0lBQ0QsdUNBQWEsR0FBYixVQUFjLEVBQVU7UUFDcEIsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBd0MsSUFBSSxDQUFDLFVBQVUsRUFBRSxnQkFBVyxFQUFJLENBQUMsQ0FBQztJQUN4RyxDQUFDO0lBQ0QscUNBQVcsR0FBWCxVQUFZLEVBQVUsRUFBRSxPQUFZO1FBQ2hDLElBQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDM0MsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBd0MsSUFBSSxDQUFDLFVBQVUsRUFBRSxzQkFBaUIsRUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ3JILENBQUM7SUFFRCw0REFBNEQ7SUFDNUQsc0NBQVksR0FBWjtRQUNJLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQTRDLElBQUksQ0FBQyxVQUFVLEVBQUUsY0FBVyxDQUFDLENBQUM7SUFDeEcsQ0FBQztJQUNELHlDQUFlLEdBQWYsVUFBZ0IsRUFBVTtRQUN0QixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUEwQyxJQUFJLENBQUMsVUFBVSxFQUFFLGtCQUFhLEVBQUksQ0FBQyxDQUFDO0lBQzVHLENBQUM7SUFDRCx1Q0FBYSxHQUFiLFVBQWMsT0FBOEI7UUFDeEMsSUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUMzQyxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUEwQyxJQUFJLENBQUMsVUFBVSxFQUFFLG9CQUFpQixFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ25ILENBQUM7SUFDRCx1Q0FBYSxHQUFiLFVBQWMsRUFBVSxFQUFFLE9BQThCO1FBQ3BELElBQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDM0MsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBMEMsSUFBSSxDQUFDLFVBQVUsRUFBRSx3QkFBbUIsRUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ3hILENBQUM7SUFDRCx1Q0FBYSxHQUFiLFVBQWMsRUFBVTtRQUNwQixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUE4QixJQUFJLENBQUMsVUFBVSxFQUFFLHdCQUFtQixFQUFJLENBQUMsQ0FBQztJQUN6RyxDQUFDO0lBRUQsNkRBQTZEO0lBQzdELGdEQUFzQixHQUF0QixVQUF1QixVQUF1QjtRQUF2QiwyQkFBQSxFQUFBLGVBQXVCO1FBQzFDLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQTZCLElBQUksQ0FBQyxVQUFVLEVBQUUsMkJBQXNCLFVBQVksQ0FBQyxDQUFDO0lBQ2hILENBQUM7SUFDRCwyREFBaUMsR0FBakMsVUFBa0MsRUFBVTtRQUN4QyxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUE2QixJQUFJLENBQUMsVUFBVSxFQUFFLDRCQUF1QixFQUFJLENBQUMsQ0FBQztJQUN6RyxDQUFDO0lBQ0QsZ0RBQXNCLEdBQXRCLFVBQXVCLE9BQWdDO1FBQ25ELElBQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDM0MsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBNkIsSUFBSSxDQUFDLFVBQVUsRUFBRSw4QkFBMkIsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUNoSCxDQUFDO0lBQ0QsZ0RBQXNCLEdBQXRCLFVBQXVCLEVBQVU7UUFDN0IsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBNkIsSUFBSSxDQUFDLFVBQVUsRUFBRSxrQ0FBNkIsRUFBSSxDQUFDLENBQUM7SUFDbEgsQ0FBQztJQUVELDBEQUEwRDtJQUMxRCx3Q0FBYyxHQUFkLFVBQWUsVUFBdUI7UUFBdkIsMkJBQUEsRUFBQSxlQUF1QjtRQUNsQyxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUErQixJQUFJLENBQUMsVUFBVSxFQUFFLG9CQUFlLFVBQVksQ0FBQyxDQUFDO0lBQzNHLENBQUM7SUFDRCwyQ0FBaUIsR0FBakIsVUFBa0IsRUFBVTtRQUN4QixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUE2QixJQUFJLENBQUMsVUFBVSxFQUFFLHFCQUFnQixFQUFJLENBQUMsQ0FBQztJQUNsRyxDQUFDO0lBQ0QseUNBQWUsR0FBZixVQUFnQixPQUFlO1FBQzNCLElBQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDM0MsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBd0MsSUFBSSxDQUFDLFVBQVUsRUFBRSx1QkFBb0IsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUNwSCxDQUFDO0lBQ0QseUNBQWUsR0FBZixVQUFnQixFQUFVO1FBQ3RCLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQTZCLElBQUksQ0FBQyxVQUFVLEVBQUUsMkJBQXNCLEVBQUksQ0FBQyxDQUFDO0lBQzNHLENBQUM7SUFFRCx5Q0FBZSxHQUFmLFVBQWdCLE9BQVk7UUFDeEIsSUFBSSxNQUFNLEdBQUcsSUFBSSxVQUFVLEVBQUUsQ0FBQztRQUM5QixLQUFLLElBQU0sR0FBRyxJQUFJLE9BQU8sRUFBRTtZQUN2QixJQUFJLE9BQU8sQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLEVBQUU7Z0JBQzdCLENBQUMsT0FBTyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssUUFBUSxDQUFDLENBQUMsQ0FBQztvQkFDaEMsTUFBTSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7b0JBQ3pELENBQUM7d0JBQ0QsTUFBTSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO2FBQ2pEO1NBQ0o7UUFFRCxPQUFPLE1BQU0sQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUM3QixDQUFDO0lBRU8sb0NBQVUsR0FBbEI7UUFDSSxPQUFVLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLGFBQVEsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLEVBQUUsYUFBVSxDQUFDO0lBQ3RGLENBQUM7O2dCQWhHb0Qsd0JBQXdCLHVCQUF4RSxNQUFNLFNBQUMsa0JBQWtCO2dCQUNOLFVBQVU7O0lBSHpCLGVBQWU7UUFEM0IsVUFBVSxFQUFFO1FBR0osV0FBQSxNQUFNLENBQUMsa0JBQWtCLENBQUMsQ0FBQTtPQUZ0QixlQUFlLENBbUczQjtJQUFELHNCQUFDO0NBQUEsQUFuR0QsSUFtR0M7U0FuR1ksZUFBZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEh0dHBDbGllbnQsIEh0dHBQYXJhbXMgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBJbmplY3QsIEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEFic3RyYWN0QXBwQ29uZmlnU2VydmljZSwgQVBQX0NPTkZJR19TRVJWSUNFLCBJSHR0cEJhc2ljUmVzcG9uc2UgfSBmcm9tICdAYm94eC9jb3JlJztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IElDYXRlZ29yeUFwaU1vZGVsLCBJQ3JlYXRlVmFyaWF0aW9uUGF5bG9hZCwgSUltYWdlQXBpTW9kZWwsIElPcmRlckFwaU1vZGVsLCBJUHJvZHVjdEFwaU1vZGVsLCBJVXBzZXJ0Q2F0ZWdvcnlQYXlsb2FkLCBJVXBzZXJ0UHJvZHVjdFBheWxvYWQsIElWYXJpYXRpb25BcGlNb2RlbCB9IGZyb20gJy4vSVN0b3JlLmFwaSc7XG5pbXBvcnQgeyBJU3RvcmVSZXBvc2l0b3J5IH0gZnJvbSAnLi9JU3RvcmUucmVwb3NpdG9yeSc7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBTdG9yZVJlcG9zaXRvcnkgaW1wbGVtZW50cyBJU3RvcmVSZXBvc2l0b3J5IHtcbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgQEluamVjdChBUFBfQ09ORklHX1NFUlZJQ0UpIHByaXZhdGUgYXBwU2V0dGluZ3M6IEFic3RyYWN0QXBwQ29uZmlnU2VydmljZSxcbiAgICAgICAgcHJpdmF0ZSBodHRwQ2xpZW50OiBIdHRwQ2xpZW50LFxuICAgICkgeyB9XG5cbiAgICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0gQ0FURUdPUklFUzogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICAgIGxvYWRDYXRlZ29yaWVzKHBhZ2luYXRpb246IHN0cmluZyA9ICcnKTogT2JzZXJ2YWJsZTxJSHR0cEJhc2ljUmVzcG9uc2U8SUNhdGVnb3J5QXBpTW9kZWxbXT4+IHtcbiAgICAgICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5nZXQ8SUh0dHBCYXNpY1Jlc3BvbnNlPElDYXRlZ29yeUFwaU1vZGVsW10+PihgJHt0aGlzLmdldEJhc2VVcmwoKX0vY2F0ZWdvcmllcyR7cGFnaW5hdGlvbn1gKTtcbiAgICB9XG4gICAgbG9hZENhdGVnb3J5QnlJZChpZDogbnVtYmVyKTogT2JzZXJ2YWJsZTxJSHR0cEJhc2ljUmVzcG9uc2U8SUNhdGVnb3J5QXBpTW9kZWw+PiB7XG4gICAgICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQuZ2V0PElIdHRwQmFzaWNSZXNwb25zZTxJQ2F0ZWdvcnlBcGlNb2RlbD4+KGAke3RoaXMuZ2V0QmFzZVVybCgpfS9jYXRlZ29yaWVzLyR7aWR9YCk7XG4gICAgfVxuICAgIGNyZWF0ZUNhdGVnb3J5KHBheWxvYWQ6IElVcHNlcnRDYXRlZ29yeVBheWxvYWQpOiBPYnNlcnZhYmxlPElIdHRwQmFzaWNSZXNwb25zZTxJQ2F0ZWdvcnlBcGlNb2RlbD4+IHtcbiAgICAgICAgY29uc3QgYm9keSA9IHRoaXMuY3JlYXRlSHR0UGFyYW1zKHBheWxvYWQpO1xuICAgICAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LnBvc3Q8SUh0dHBCYXNpY1Jlc3BvbnNlPElDYXRlZ29yeUFwaU1vZGVsPj4oYCR7dGhpcy5nZXRCYXNlVXJsKCl9L2NyZWF0ZV9jYXRlZ29yeWAsIGJvZHkpO1xuICAgIH1cbiAgICBkZWxldGVDYXRlZ29yeShpZDogbnVtYmVyKTogT2JzZXJ2YWJsZTxJSHR0cEJhc2ljUmVzcG9uc2U8bnVsbD4+IHtcbiAgICAgICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5kZWxldGU8SUh0dHBCYXNpY1Jlc3BvbnNlPG51bGw+PihgJHt0aGlzLmdldEJhc2VVcmwoKX0vZGVsZXRlX2NhdGVnb3J5LyR7aWR9YCk7XG4gICAgfVxuXG4gICAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tIE9SREVSUzogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICAgIGxvYWRPcmRlcnMocGFnaW5hdGlvbjogc3RyaW5nID0gJycpOiBPYnNlcnZhYmxlPElIdHRwQmFzaWNSZXNwb25zZTxJT3JkZXJBcGlNb2RlbFtdPj4ge1xuICAgICAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LmdldDxJSHR0cEJhc2ljUmVzcG9uc2U8SU9yZGVyQXBpTW9kZWxbXT4+KGAke3RoaXMuZ2V0QmFzZVVybCgpfS9vcmRlcnMke3BhZ2luYXRpb259YCk7XG4gICAgfVxuICAgIGxvYWRPcmRlckJ5SWQoaWQ6IG51bWJlcik6IE9ic2VydmFibGU8SUh0dHBCYXNpY1Jlc3BvbnNlPElPcmRlckFwaU1vZGVsPj4ge1xuICAgICAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LmdldDxJSHR0cEJhc2ljUmVzcG9uc2U8SU9yZGVyQXBpTW9kZWw+PihgJHt0aGlzLmdldEJhc2VVcmwoKX0vb3JkZXJzLyR7aWR9YCk7XG4gICAgfVxuICAgIHVwZGF0ZU9yZGVyKGlkOiBudW1iZXIsIHBheWxvYWQ6IGFueSk6IE9ic2VydmFibGU8SUh0dHBCYXNpY1Jlc3BvbnNlPElPcmRlckFwaU1vZGVsPj4ge1xuICAgICAgICBjb25zdCBib2R5ID0gdGhpcy5jcmVhdGVIdHRQYXJhbXMocGF5bG9hZCk7XG4gICAgICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQucG9zdDxJSHR0cEJhc2ljUmVzcG9uc2U8SU9yZGVyQXBpTW9kZWw+PihgJHt0aGlzLmdldEJhc2VVcmwoKX0vdXBkYXRlX29yZGVyLyR7aWR9YCwgYm9keSk7XG4gICAgfVxuXG4gICAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tIFBST0RVQ1RTOiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gICAgbG9hZFByb2R1Y3RzKCk6IE9ic2VydmFibGU8SUh0dHBCYXNpY1Jlc3BvbnNlPElQcm9kdWN0QXBpTW9kZWxbXT4+IHtcbiAgICAgICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5nZXQ8SUh0dHBCYXNpY1Jlc3BvbnNlPElQcm9kdWN0QXBpTW9kZWxbXT4+KGAke3RoaXMuZ2V0QmFzZVVybCgpfS9wcm9kdWN0c2ApO1xuICAgIH1cbiAgICBsb2FkUHJvZHVjdEJ5SWQoaWQ6IG51bWJlcik6IE9ic2VydmFibGU8SUh0dHBCYXNpY1Jlc3BvbnNlPElQcm9kdWN0QXBpTW9kZWw+PiB7XG4gICAgICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQuZ2V0PElIdHRwQmFzaWNSZXNwb25zZTxJUHJvZHVjdEFwaU1vZGVsPj4oYCR7dGhpcy5nZXRCYXNlVXJsKCl9L3Byb2R1Y3RzLyR7aWR9YCk7XG4gICAgfVxuICAgIGNyZWF0ZVByb2R1Y3QocGF5bG9hZDogSVVwc2VydFByb2R1Y3RQYXlsb2FkKTogT2JzZXJ2YWJsZTxJSHR0cEJhc2ljUmVzcG9uc2U8SVByb2R1Y3RBcGlNb2RlbD4+IHtcbiAgICAgICAgY29uc3QgYm9keSA9IHRoaXMuY3JlYXRlSHR0UGFyYW1zKHBheWxvYWQpO1xuICAgICAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LnBvc3Q8SUh0dHBCYXNpY1Jlc3BvbnNlPElQcm9kdWN0QXBpTW9kZWw+PihgJHt0aGlzLmdldEJhc2VVcmwoKX0vY3JlYXRlX3Byb2R1Y3RgLCBib2R5KTtcbiAgICB9XG4gICAgdXBkYXRlUHJvZHVjdChpZDogbnVtYmVyLCBwYXlsb2FkOiBJVXBzZXJ0UHJvZHVjdFBheWxvYWQpOiBPYnNlcnZhYmxlPElIdHRwQmFzaWNSZXNwb25zZTxJUHJvZHVjdEFwaU1vZGVsPj4ge1xuICAgICAgICBjb25zdCBib2R5ID0gdGhpcy5jcmVhdGVIdHRQYXJhbXMocGF5bG9hZCk7XG4gICAgICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQucHV0PElIdHRwQmFzaWNSZXNwb25zZTxJUHJvZHVjdEFwaU1vZGVsPj4oYCR7dGhpcy5nZXRCYXNlVXJsKCl9L3VwZGF0ZV9wcm9kdWN0LyR7aWR9YCwgYm9keSk7XG4gICAgfVxuICAgIGRlbGV0ZVByb2R1Y3QoaWQ6IG51bWJlcik6IE9ic2VydmFibGU8SUh0dHBCYXNpY1Jlc3BvbnNlPG51bGw+PiB7XG4gICAgICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQuZGVsZXRlPElIdHRwQmFzaWNSZXNwb25zZTxudWxsPj4oYCR7dGhpcy5nZXRCYXNlVXJsKCl9L2RlbGV0ZV9wcm9kdWN0LyR7aWR9YCk7XG4gICAgfVxuXG4gICAgLy8gLS0tLS0tLS0tLS0tLS0tLSBQUk9EVUNUIFZBUklBVElPTlM6IC0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICAgIGxvYWRQcm9kdWN0c1ZhcmlhdGlvbnMocGFnaW5hdGlvbjogc3RyaW5nID0gJycpOiBPYnNlcnZhYmxlPElIdHRwQmFzaWNSZXNwb25zZTxJVmFyaWF0aW9uQXBpTW9kZWxbXT4+IHtcbiAgICAgICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5nZXQ8SUh0dHBCYXNpY1Jlc3BvbnNlPGFueT4+KGAke3RoaXMuZ2V0QmFzZVVybCgpfS9wcm9kdWN0X3ZhcmlhdGlvbnMke3BhZ2luYXRpb259YCk7XG4gICAgfVxuICAgIGxvYWRQcm9kdWN0VmFyaWF0aW9uQnlWYXJpYXRpb25JZChpZDogbnVtYmVyKTogT2JzZXJ2YWJsZTxJSHR0cEJhc2ljUmVzcG9uc2U8SVZhcmlhdGlvbkFwaU1vZGVsPj4ge1xuICAgICAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LmdldDxJSHR0cEJhc2ljUmVzcG9uc2U8YW55Pj4oYCR7dGhpcy5nZXRCYXNlVXJsKCl9L3Byb2R1Y3RfdmFyaWF0aW9ucy8ke2lkfWApO1xuICAgIH1cbiAgICBjcmVhdGVQcm9kdWN0VmFyaWF0aW9uKHBheWxvYWQ6IElDcmVhdGVWYXJpYXRpb25QYXlsb2FkKTogT2JzZXJ2YWJsZTxJSHR0cEJhc2ljUmVzcG9uc2U8SVZhcmlhdGlvbkFwaU1vZGVsPj4ge1xuICAgICAgICBjb25zdCBib2R5ID0gdGhpcy5jcmVhdGVIdHRQYXJhbXMocGF5bG9hZCk7XG4gICAgICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQucG9zdDxJSHR0cEJhc2ljUmVzcG9uc2U8YW55Pj4oYCR7dGhpcy5nZXRCYXNlVXJsKCl9L2NyZWF0ZV9wcm9kdWN0X3ZhcmlhdGlvbmAsIGJvZHkpO1xuICAgIH1cbiAgICBkZWxldGVQcm9kdWN0VmFyaWF0aW9uKGlkOiBudW1iZXIpOiBPYnNlcnZhYmxlPElIdHRwQmFzaWNSZXNwb25zZTxudWxsPj4ge1xuICAgICAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LmRlbGV0ZTxJSHR0cEJhc2ljUmVzcG9uc2U8YW55Pj4oYCR7dGhpcy5nZXRCYXNlVXJsKCl9L2RlbGV0ZV9wcm9kdWN0X3ZhcmlhdGlvbi8ke2lkfWApO1xuICAgIH1cblxuICAgIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLSBJTUFHRVM6IC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAgICBsb2FkU2l0ZUltYWdlcyhwYWdpbmF0aW9uOiBzdHJpbmcgPSAnJyk6IE9ic2VydmFibGU8SUh0dHBCYXNpY1Jlc3BvbnNlPElJbWFnZUFwaU1vZGVsW10+PiB7XG4gICAgICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQuZ2V0PElIdHRwQmFzaWNSZXNwb25zZTxhbnlbXT4+KGAke3RoaXMuZ2V0QmFzZVVybCgpfS9zaXRlX2ltYWdlcyR7cGFnaW5hdGlvbn1gKTtcbiAgICB9XG4gICAgbG9hZFNpdGVJbWFnZUJ5SWQoaWQ6IG51bWJlcik6IE9ic2VydmFibGU8SUh0dHBCYXNpY1Jlc3BvbnNlPElJbWFnZUFwaU1vZGVsPj4ge1xuICAgICAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LmdldDxJSHR0cEJhc2ljUmVzcG9uc2U8YW55Pj4oYCR7dGhpcy5nZXRCYXNlVXJsKCl9L3NpdGVfaW1hZ2VzLyR7aWR9YCk7XG4gICAgfVxuICAgIGNyZWF0ZVNpdGVJbWFnZShwYXlsb2FkOiBudW1iZXIpOiBPYnNlcnZhYmxlPElIdHRwQmFzaWNSZXNwb25zZTxJSW1hZ2VBcGlNb2RlbD4+IHtcbiAgICAgICAgY29uc3QgYm9keSA9IHRoaXMuY3JlYXRlSHR0UGFyYW1zKHBheWxvYWQpO1xuICAgICAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LnBvc3Q8SUh0dHBCYXNpY1Jlc3BvbnNlPElJbWFnZUFwaU1vZGVsPj4oYCR7dGhpcy5nZXRCYXNlVXJsKCl9L2NyZWF0ZV9zaXRlX2ltYWdlYCwgYm9keSk7XG4gICAgfVxuICAgIGRlbGV0ZVNpdGVJbWFnZShpZDogbnVtYmVyKTogT2JzZXJ2YWJsZTxJSHR0cEJhc2ljUmVzcG9uc2U8bnVsbD4+IHtcbiAgICAgICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5kZWxldGU8SUh0dHBCYXNpY1Jlc3BvbnNlPGFueT4+KGAke3RoaXMuZ2V0QmFzZVVybCgpfS9kZWxldGVfc2l0ZV9pbWFnZS8ke2lkfWApO1xuICAgIH1cblxuICAgIGNyZWF0ZUh0dFBhcmFtcyhwYXlsb2FkOiBhbnkpIHtcbiAgICAgICAgbGV0IHBhcmFtcyA9IG5ldyBIdHRwUGFyYW1zKCk7XG4gICAgICAgIGZvciAoY29uc3Qga2V5IGluIHBheWxvYWQpIHtcbiAgICAgICAgICAgIGlmIChwYXlsb2FkLmhhc093blByb3BlcnR5KGtleSkpIHtcbiAgICAgICAgICAgICAgICAodHlwZW9mIHBheWxvYWRba2V5XSA9PT0gJ29iamVjdCcpID9cbiAgICAgICAgICAgICAgICAgICAgcGFyYW1zID0gcGFyYW1zLmFwcGVuZChrZXksIEpTT04uc3RyaW5naWZ5KHBheWxvYWRba2V5XSkpXG4gICAgICAgICAgICAgICAgICAgIDpcbiAgICAgICAgICAgICAgICAgICAgcGFyYW1zID0gcGFyYW1zLmFwcGVuZChrZXksIHBheWxvYWRba2V5XSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gcGFyYW1zLnRvU3RyaW5nKCk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBnZXRCYXNlVXJsKCkge1xuICAgICAgICByZXR1cm4gYCR7dGhpcy5hcHBTZXR0aW5ncy5iYXNlVXJsKCl9L2FwaS8ke3RoaXMuYXBwU2V0dGluZ3MuaW5zdGFuY2UoKX0vdjEvc2hvcGA7XG4gICAgfVxufVxuIl19