import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromCategoriesActions from './store-categories.actions';
import * as fromOrdersActions from './store-orders.actions';
import * as fromProductsActions from './store-products.actions';
import * as fromImagesActions from './store-images.action';
import * as fromVariationsActions from './store-variations.actions';
import * as fromSelectors from './store.selectors';
var StorePageStore = /** @class */ (function () {
    function StorePageStore(store) {
        var _this = this;
        this.store = store;
        // ----------- CATEGORIES METHODS:
        this.LoadCategories = function () { return _this.store.dispatch(fromCategoriesActions.LoadCategoriesBeginAction()); };
        this.loadCategoryById = function (id) { return _this.store.dispatch(fromCategoriesActions.LoadCategorieByIdBeginAction({ id: id })); };
        this.CreateCategory = function (payload) { return _this.store.dispatch(fromCategoriesActions.CreateCategoryBeginAction({ payload: payload })); };
        this.DeleteCategory = function (id) { return _this.store.dispatch(fromCategoriesActions.DeleteCategoryBeginAction({ id: id })); };
        // ----------- ORDERS METHODS:
        this.LoadOrders = function () { return _this.store.dispatch(fromOrdersActions.LoadOrdersBeginAction()); };
        this.loadOrderById = function (id) { return _this.store.dispatch(fromOrdersActions.LoadOrderByIdBeginAction({ id: id })); };
        this.updateOrder = function (id, payload) { return _this.store.dispatch(fromOrdersActions.UpdateOrderBeginAction({ id: id, payload: payload })); };
        // ----------- PRODUCTS METHODS:
        this.LoadProducts = function () { return _this.store.dispatch(fromProductsActions.LoadProductsBeginAction()); };
        this.CreateProduct = function (payload) { return _this.store.dispatch(fromProductsActions.CreateProductBeginAction({ payload: payload })); };
        this.UpdateProduct = function (id, payload) { return _this.store.dispatch(fromProductsActions.UpdateProductBeginAction({ id: id, payload: payload })); };
        this.DeleteProduct = function (id) { return _this.store.dispatch(fromProductsActions.DeleteProductBeginAction({ id: id })); };
        // ----------- VARIATIONS METHODS:
        this.LoadVariations = function () { return _this.store.dispatch(fromVariationsActions.LoadVariationsBeginAction()); };
        this.CreateVariation = function (payload) { return _this.store.dispatch(fromVariationsActions.CreteVariationBeginAction({ payload: payload })); };
        this.DeleteVariation = function (payload) { return _this.store.dispatch(fromVariationsActions.DelteVariationBeginAction({ payload: payload })); };
        // ----------- IMAGE METHODS:
        this.LoadImages = function () { return _this.store.dispatch(fromImagesActions.LoadImagesBeginAction()); };
        this.LoadImageById = function (id) { return _this.store.dispatch(fromImagesActions.LoadImageByIdBeginAction({ id: id })); };
        this.CreateImage = function (payload) { return _this.store.dispatch(fromImagesActions.CreateImageBeginAction({ payload: payload })); };
        this.DeleteImage = function (id) { return _this.store.dispatch(fromImagesActions.DeleteImageBeginAction({ id: id })); };
    }
    Object.defineProperty(StorePageStore.prototype, "IsLoadingCategories$", {
        get: function () { return this.store.select(fromSelectors.getCategoriesIsloading); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StorePageStore.prototype, "CategoriesData$", {
        get: function () { return this.store.select(fromSelectors.getCategoriesData); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StorePageStore.prototype, "CategoriesHasBeenFetched$", {
        get: function () { return this.store.select(fromSelectors.getCategoriesHasBeenFetched); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StorePageStore.prototype, "CategoriesError$", {
        get: function () { return this.store.select(fromSelectors.getCategoriesError); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StorePageStore.prototype, "CategoriesSuccess$", {
        get: function () { return this.store.select(fromSelectors.getCategoriesSuccess); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StorePageStore.prototype, "IsLoadingOrders$", {
        get: function () { return this.store.select(fromSelectors.getOrdersIsLoading); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StorePageStore.prototype, "OrdersData$", {
        get: function () { return this.store.select(fromSelectors.getOrdersData); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StorePageStore.prototype, "OrdersHasBeenFetched$", {
        get: function () { return this.store.select(fromSelectors.getOrdersHasBeenFetched); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StorePageStore.prototype, "OrdersError$", {
        get: function () { return this.store.select(fromSelectors.getOrdersError); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StorePageStore.prototype, "OrdersSuccess$", {
        get: function () { return this.store.select(fromSelectors.getOrdersSuccess); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StorePageStore.prototype, "IsLoadingProducts$", {
        get: function () { return this.store.select(fromSelectors.getStoreProductsIsLoading); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StorePageStore.prototype, "ProductsData$", {
        get: function () { return this.store.select(fromSelectors.getStoreProductsData); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StorePageStore.prototype, "ProductsHasBeenFetched$", {
        get: function () { return this.store.select(fromSelectors.getStoreProductsHasBeenFetched); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StorePageStore.prototype, "ProductsError$", {
        get: function () { return this.store.select(fromSelectors.getStoreProductsError); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StorePageStore.prototype, "ProductsSuccess$", {
        get: function () { return this.store.select(fromSelectors.getStoreProductsSuccess); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StorePageStore.prototype, "IsLoadingVariations$", {
        get: function () { return this.store.select(fromSelectors.getStoreVariationsIsLoading); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StorePageStore.prototype, "VariationsData$", {
        get: function () { return this.store.select(fromSelectors.getStoreVariationsData); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StorePageStore.prototype, "VariationsHasBeenFetched$", {
        get: function () { return this.store.select(fromSelectors.getStoreVariationsHasBeenFetched); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StorePageStore.prototype, "VariationsError$", {
        get: function () { return this.store.select(fromSelectors.getStoreVariationsError); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StorePageStore.prototype, "VariationsSuccess$", {
        get: function () { return this.store.select(fromSelectors.getStoreVariationsSuccess); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StorePageStore.prototype, "IsLoadingImages$", {
        get: function () { return this.store.select(fromSelectors.getStoreImagesIsLoading); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StorePageStore.prototype, "ImagesData$", {
        get: function () { return this.store.select(fromSelectors.getStoreImagesData); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StorePageStore.prototype, "ImagesHasBeenFetched$", {
        get: function () { return this.store.select(fromSelectors.getStoreImagesHasBeenFetched); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StorePageStore.prototype, "ImagesError$", {
        get: function () { return this.store.select(fromSelectors.getStoreImagesError); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StorePageStore.prototype, "ImagesSuccess$", {
        get: function () { return this.store.select(fromSelectors.getStoreImagesSuccess); },
        enumerable: true,
        configurable: true
    });
    StorePageStore.ctorParameters = function () { return [
        { type: Store }
    ]; };
    StorePageStore = __decorate([
        Injectable()
    ], StorePageStore);
    return StorePageStore;
}());
export { StorePageStore };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RvcmUuc3RvcmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYm94eC9zdG9yZS1jb3JlLyIsInNvdXJjZXMiOlsibGliL3N0YXRlL3N0b3JlLnN0b3JlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSxhQUFhLENBQUM7QUFFcEMsT0FBTyxLQUFLLHFCQUFxQixNQUFNLDRCQUE0QixDQUFDO0FBQ3BFLE9BQU8sS0FBSyxpQkFBaUIsTUFBTSx3QkFBd0IsQ0FBQztBQUM1RCxPQUFPLEtBQUssbUJBQW1CLE1BQU0sMEJBQTBCLENBQUM7QUFDaEUsT0FBTyxLQUFLLGlCQUFpQixNQUFNLHVCQUF1QixDQUFDO0FBQzNELE9BQU8sS0FBSyxxQkFBcUIsTUFBTSw0QkFBNEIsQ0FBQztBQUVwRSxPQUFPLEtBQUssYUFBYSxNQUFNLG1CQUFtQixDQUFDO0FBR25EO0lBQ0ksd0JBQW1CLEtBQXFDO1FBQXhELGlCQUE2RDtRQUExQyxVQUFLLEdBQUwsS0FBSyxDQUFnQztRQUV4RCxrQ0FBa0M7UUFDbEMsbUJBQWMsR0FBRyxjQUFNLE9BQUEsS0FBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMscUJBQXFCLENBQUMseUJBQXlCLEVBQUUsQ0FBQyxFQUF0RSxDQUFzRSxDQUFDO1FBQzlGLHFCQUFnQixHQUFHLFVBQUMsRUFBVSxJQUFLLE9BQUEsS0FBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMscUJBQXFCLENBQUMsNEJBQTRCLENBQUMsRUFBRSxFQUFFLElBQUEsRUFBRSxDQUFDLENBQUMsRUFBL0UsQ0FBK0UsQ0FBQztRQUNuSCxtQkFBYyxHQUFHLFVBQUMsT0FBK0IsSUFBSyxPQUFBLEtBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLHFCQUFxQixDQUFDLHlCQUF5QixDQUFDLEVBQUUsT0FBTyxTQUFBLEVBQUUsQ0FBQyxDQUFDLEVBQWpGLENBQWlGLENBQUM7UUFDeEksbUJBQWMsR0FBRyxVQUFDLEVBQVUsSUFBSyxPQUFBLEtBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLHFCQUFxQixDQUFDLHlCQUF5QixDQUFDLEVBQUUsRUFBRSxJQUFBLEVBQUUsQ0FBQyxDQUFDLEVBQTVFLENBQTRFLENBQUM7UUFROUcsOEJBQThCO1FBQzlCLGVBQVUsR0FBRyxjQUFNLE9BQUEsS0FBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsaUJBQWlCLENBQUMscUJBQXFCLEVBQUUsQ0FBQyxFQUE5RCxDQUE4RCxDQUFDO1FBQ2xGLGtCQUFhLEdBQUcsVUFBQyxFQUFVLElBQUssT0FBQSxLQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxpQkFBaUIsQ0FBQyx3QkFBd0IsQ0FBQyxFQUFFLEVBQUUsSUFBQSxFQUFFLENBQUMsQ0FBQyxFQUF2RSxDQUF1RSxDQUFDO1FBQ3hHLGdCQUFXLEdBQUcsVUFBQyxFQUFVLEVBQUUsT0FBWSxJQUFLLE9BQUEsS0FBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsaUJBQWlCLENBQUMsc0JBQXNCLENBQUMsRUFBRSxFQUFFLElBQUEsRUFBRSxPQUFPLFNBQUEsRUFBRSxDQUFDLENBQUMsRUFBOUUsQ0FBOEUsQ0FBQztRQVEzSCxnQ0FBZ0M7UUFDaEMsaUJBQVksR0FBRyxjQUFNLE9BQUEsS0FBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsbUJBQW1CLENBQUMsdUJBQXVCLEVBQUUsQ0FBQyxFQUFsRSxDQUFrRSxDQUFDO1FBQ3hGLGtCQUFhLEdBQUcsVUFBQyxPQUE4QixJQUFLLE9BQUEsS0FBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsbUJBQW1CLENBQUMsd0JBQXdCLENBQUMsRUFBRSxPQUFPLFNBQUEsRUFBRSxDQUFDLENBQUMsRUFBOUUsQ0FBOEUsQ0FBQztRQUNuSSxrQkFBYSxHQUFHLFVBQ1osRUFBVSxFQUFFLE9BQThCLElBQ3pDLE9BQUEsS0FBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsbUJBQW1CLENBQUMsd0JBQXdCLENBQUMsRUFBRSxFQUFFLElBQUEsRUFBRSxPQUFPLFNBQUEsRUFBRSxDQUFDLENBQUMsRUFBbEYsQ0FBa0YsQ0FBQTtRQUN2RixrQkFBYSxHQUFHLFVBQUMsRUFBVSxJQUFLLE9BQUEsS0FBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsbUJBQW1CLENBQUMsd0JBQXdCLENBQUMsRUFBRSxFQUFFLElBQUEsRUFBRSxDQUFDLENBQUMsRUFBekUsQ0FBeUUsQ0FBQztRQVExRyxrQ0FBa0M7UUFDbEMsbUJBQWMsR0FBRyxjQUFNLE9BQUEsS0FBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMscUJBQXFCLENBQUMseUJBQXlCLEVBQUUsQ0FBQyxFQUF0RSxDQUFzRSxDQUFDO1FBQzlGLG9CQUFlLEdBQUcsVUFBQyxPQUFnQyxJQUFLLE9BQUEsS0FBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQ3ZFLHFCQUFxQixDQUFDLHlCQUF5QixDQUFDLEVBQUUsT0FBTyxTQUFBLEVBQUUsQ0FBQyxDQUMvRCxFQUZ1RCxDQUV2RCxDQUFBO1FBQ0Qsb0JBQWUsR0FBRyxVQUFDLE9BQWUsSUFBSyxPQUFBLEtBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLHFCQUFxQixDQUFDLHlCQUF5QixDQUFDLEVBQUUsT0FBTyxTQUFBLEVBQUUsQ0FBQyxDQUFDLEVBQWpGLENBQWlGLENBQUM7UUFRekgsNkJBQTZCO1FBQzdCLGVBQVUsR0FBRyxjQUFNLE9BQUEsS0FBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsaUJBQWlCLENBQUMscUJBQXFCLEVBQUUsQ0FBQyxFQUE5RCxDQUE4RCxDQUFDO1FBQ2xGLGtCQUFhLEdBQUcsVUFBQyxFQUFVLElBQUssT0FBQSxLQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxpQkFBaUIsQ0FBQyx3QkFBd0IsQ0FBQyxFQUFFLEVBQUUsSUFBQSxFQUFFLENBQUMsQ0FBQyxFQUF2RSxDQUF1RSxDQUFDO1FBQ3hHLGdCQUFXLEdBQUcsVUFBQyxPQUFZLElBQUssT0FBQSxLQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxpQkFBaUIsQ0FBQyxzQkFBc0IsQ0FBQyxFQUFFLE9BQU8sU0FBQSxFQUFFLENBQUMsQ0FBQyxFQUExRSxDQUEwRSxDQUFDO1FBQzNHLGdCQUFXLEdBQUcsVUFBQyxFQUFVLElBQUssT0FBQSxLQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxpQkFBaUIsQ0FBQyxzQkFBc0IsQ0FBQyxFQUFFLEVBQUUsSUFBQSxFQUFFLENBQUMsQ0FBQyxFQUFyRSxDQUFxRSxDQUFDO0lBeER4QyxDQUFDO0lBUTdELHNCQUFJLGdEQUFvQjthQUF4QixjQUE2QixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDLENBQUMsQ0FBQzs7O09BQUE7SUFDOUYsc0JBQUksMkNBQWU7YUFBbkIsY0FBd0IsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLENBQUM7OztPQUFBO0lBQ3BGLHNCQUFJLHFEQUF5QjthQUE3QixjQUFrQyxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQywyQkFBMkIsQ0FBQyxDQUFDLENBQUMsQ0FBQzs7O09BQUE7SUFDeEcsc0JBQUksNENBQWdCO2FBQXBCLGNBQXlCLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxDQUFDOzs7T0FBQTtJQUN0RixzQkFBSSw4Q0FBa0I7YUFBdEIsY0FBMkIsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDLENBQUM7OztPQUFBO0lBTzFGLHNCQUFJLDRDQUFnQjthQUFwQixjQUF5QixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLENBQUMsQ0FBQzs7O09BQUE7SUFDdEYsc0JBQUksdUNBQVc7YUFBZixjQUFvQixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUM7OztPQUFBO0lBQzVFLHNCQUFJLGlEQUFxQjthQUF6QixjQUE4QixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDLENBQUMsQ0FBQzs7O09BQUE7SUFDaEcsc0JBQUksd0NBQVk7YUFBaEIsY0FBcUIsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDOzs7T0FBQTtJQUM5RSxzQkFBSSwwQ0FBYzthQUFsQixjQUF1QixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQzs7O09BQUE7SUFVbEYsc0JBQUksOENBQWtCO2FBQXRCLGNBQTJCLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLHlCQUF5QixDQUFDLENBQUMsQ0FBQyxDQUFDOzs7T0FBQTtJQUMvRixzQkFBSSx5Q0FBYTthQUFqQixjQUFzQixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsQ0FBQzs7O09BQUE7SUFDckYsc0JBQUksbURBQXVCO2FBQTNCLGNBQWdDLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLDhCQUE4QixDQUFDLENBQUMsQ0FBQyxDQUFDOzs7T0FBQTtJQUN6RyxzQkFBSSwwQ0FBYzthQUFsQixjQUF1QixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsQ0FBQzs7O09BQUE7SUFDdkYsc0JBQUksNENBQWdCO2FBQXBCLGNBQXlCLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLHVCQUF1QixDQUFDLENBQUMsQ0FBQyxDQUFDOzs7T0FBQTtJQVMzRixzQkFBSSxnREFBb0I7YUFBeEIsY0FBNkIsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsMkJBQTJCLENBQUMsQ0FBQyxDQUFDLENBQUM7OztPQUFBO0lBQ25HLHNCQUFJLDJDQUFlO2FBQW5CLGNBQXdCLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLHNCQUFzQixDQUFDLENBQUMsQ0FBQyxDQUFDOzs7T0FBQTtJQUN6RixzQkFBSSxxREFBeUI7YUFBN0IsY0FBa0MsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsZ0NBQWdDLENBQUMsQ0FBQyxDQUFDLENBQUM7OztPQUFBO0lBQzdHLHNCQUFJLDRDQUFnQjthQUFwQixjQUF5QixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDLENBQUMsQ0FBQzs7O09BQUE7SUFDM0Ysc0JBQUksOENBQWtCO2FBQXRCLGNBQTJCLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLHlCQUF5QixDQUFDLENBQUMsQ0FBQyxDQUFDOzs7T0FBQTtJQVEvRixzQkFBSSw0Q0FBZ0I7YUFBcEIsY0FBeUIsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsdUJBQXVCLENBQUMsQ0FBQyxDQUFDLENBQUM7OztPQUFBO0lBQzNGLHNCQUFJLHVDQUFXO2FBQWYsY0FBb0IsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDLENBQUM7OztPQUFBO0lBQ2pGLHNCQUFJLGlEQUFxQjthQUF6QixjQUE4QixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyw0QkFBNEIsQ0FBQyxDQUFDLENBQUMsQ0FBQzs7O09BQUE7SUFDckcsc0JBQUksd0NBQVk7YUFBaEIsY0FBcUIsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDLENBQUM7OztPQUFBO0lBQ25GLHNCQUFJLDBDQUFjO2FBQWxCLGNBQXVCLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQyxDQUFDOzs7T0FBQTs7Z0JBOUQ3RCxLQUFLOztJQUR0QixjQUFjO1FBRDFCLFVBQVUsRUFBRTtPQUNBLGNBQWMsQ0FnRTFCO0lBQUQscUJBQUM7Q0FBQSxBQWhFRCxJQWdFQztTQWhFWSxjQUFjIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgU3RvcmUgfSBmcm9tICdAbmdyeC9zdG9yZSc7XG5pbXBvcnQgeyBJQ3JlYXRlVmFyaWF0aW9uUGF5bG9hZCwgSVVwc2VydENhdGVnb3J5UGF5bG9hZCwgSVVwc2VydFByb2R1Y3RQYXlsb2FkIH0gZnJvbSAnLi4vcmVwb3NpdG9yaWVzL0lTdG9yZS5hcGknO1xuaW1wb3J0ICogYXMgZnJvbUNhdGVnb3JpZXNBY3Rpb25zIGZyb20gJy4vc3RvcmUtY2F0ZWdvcmllcy5hY3Rpb25zJztcbmltcG9ydCAqIGFzIGZyb21PcmRlcnNBY3Rpb25zIGZyb20gJy4vc3RvcmUtb3JkZXJzLmFjdGlvbnMnO1xuaW1wb3J0ICogYXMgZnJvbVByb2R1Y3RzQWN0aW9ucyBmcm9tICcuL3N0b3JlLXByb2R1Y3RzLmFjdGlvbnMnO1xuaW1wb3J0ICogYXMgZnJvbUltYWdlc0FjdGlvbnMgZnJvbSAnLi9zdG9yZS1pbWFnZXMuYWN0aW9uJztcbmltcG9ydCAqIGFzIGZyb21WYXJpYXRpb25zQWN0aW9ucyBmcm9tICcuL3N0b3JlLXZhcmlhdGlvbnMuYWN0aW9ucyc7XG5pbXBvcnQgKiBhcyBmcm9tUmVkdWNlciBmcm9tICcuL3N0b3JlLnJlZHVjZXInO1xuaW1wb3J0ICogYXMgZnJvbVNlbGVjdG9ycyBmcm9tICcuL3N0b3JlLnNlbGVjdG9ycyc7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBTdG9yZVBhZ2VTdG9yZSB7XG4gICAgY29uc3RydWN0b3IocHVibGljIHN0b3JlOiBTdG9yZTxmcm9tUmVkdWNlci5JU3RvcmVTdGF0ZT4pIHsgfVxuXG4gICAgLy8gLS0tLS0tLS0tLS0gQ0FURUdPUklFUyBNRVRIT0RTOlxuICAgIExvYWRDYXRlZ29yaWVzID0gKCkgPT4gdGhpcy5zdG9yZS5kaXNwYXRjaChmcm9tQ2F0ZWdvcmllc0FjdGlvbnMuTG9hZENhdGVnb3JpZXNCZWdpbkFjdGlvbigpKTtcbiAgICBsb2FkQ2F0ZWdvcnlCeUlkID0gKGlkOiBudW1iZXIpID0+IHRoaXMuc3RvcmUuZGlzcGF0Y2goZnJvbUNhdGVnb3JpZXNBY3Rpb25zLkxvYWRDYXRlZ29yaWVCeUlkQmVnaW5BY3Rpb24oeyBpZCB9KSk7XG4gICAgQ3JlYXRlQ2F0ZWdvcnkgPSAocGF5bG9hZDogSVVwc2VydENhdGVnb3J5UGF5bG9hZCkgPT4gdGhpcy5zdG9yZS5kaXNwYXRjaChmcm9tQ2F0ZWdvcmllc0FjdGlvbnMuQ3JlYXRlQ2F0ZWdvcnlCZWdpbkFjdGlvbih7IHBheWxvYWQgfSkpO1xuICAgIERlbGV0ZUNhdGVnb3J5ID0gKGlkOiBudW1iZXIpID0+IHRoaXMuc3RvcmUuZGlzcGF0Y2goZnJvbUNhdGVnb3JpZXNBY3Rpb25zLkRlbGV0ZUNhdGVnb3J5QmVnaW5BY3Rpb24oeyBpZCB9KSk7XG5cbiAgICBnZXQgSXNMb2FkaW5nQ2F0ZWdvcmllcyQoKSB7IHJldHVybiB0aGlzLnN0b3JlLnNlbGVjdChmcm9tU2VsZWN0b3JzLmdldENhdGVnb3JpZXNJc2xvYWRpbmcpOyB9XG4gICAgZ2V0IENhdGVnb3JpZXNEYXRhJCgpIHsgcmV0dXJuIHRoaXMuc3RvcmUuc2VsZWN0KGZyb21TZWxlY3RvcnMuZ2V0Q2F0ZWdvcmllc0RhdGEpOyB9XG4gICAgZ2V0IENhdGVnb3JpZXNIYXNCZWVuRmV0Y2hlZCQoKSB7IHJldHVybiB0aGlzLnN0b3JlLnNlbGVjdChmcm9tU2VsZWN0b3JzLmdldENhdGVnb3JpZXNIYXNCZWVuRmV0Y2hlZCk7IH1cbiAgICBnZXQgQ2F0ZWdvcmllc0Vycm9yJCgpIHsgcmV0dXJuIHRoaXMuc3RvcmUuc2VsZWN0KGZyb21TZWxlY3RvcnMuZ2V0Q2F0ZWdvcmllc0Vycm9yKTsgfVxuICAgIGdldCBDYXRlZ29yaWVzU3VjY2VzcyQoKSB7IHJldHVybiB0aGlzLnN0b3JlLnNlbGVjdChmcm9tU2VsZWN0b3JzLmdldENhdGVnb3JpZXNTdWNjZXNzKTsgfVxuXG4gICAgLy8gLS0tLS0tLS0tLS0gT1JERVJTIE1FVEhPRFM6XG4gICAgTG9hZE9yZGVycyA9ICgpID0+IHRoaXMuc3RvcmUuZGlzcGF0Y2goZnJvbU9yZGVyc0FjdGlvbnMuTG9hZE9yZGVyc0JlZ2luQWN0aW9uKCkpO1xuICAgIGxvYWRPcmRlckJ5SWQgPSAoaWQ6IG51bWJlcikgPT4gdGhpcy5zdG9yZS5kaXNwYXRjaChmcm9tT3JkZXJzQWN0aW9ucy5Mb2FkT3JkZXJCeUlkQmVnaW5BY3Rpb24oeyBpZCB9KSk7XG4gICAgdXBkYXRlT3JkZXIgPSAoaWQ6IG51bWJlciwgcGF5bG9hZDogYW55KSA9PiB0aGlzLnN0b3JlLmRpc3BhdGNoKGZyb21PcmRlcnNBY3Rpb25zLlVwZGF0ZU9yZGVyQmVnaW5BY3Rpb24oeyBpZCwgcGF5bG9hZCB9KSk7XG5cbiAgICBnZXQgSXNMb2FkaW5nT3JkZXJzJCgpIHsgcmV0dXJuIHRoaXMuc3RvcmUuc2VsZWN0KGZyb21TZWxlY3RvcnMuZ2V0T3JkZXJzSXNMb2FkaW5nKTsgfVxuICAgIGdldCBPcmRlcnNEYXRhJCgpIHsgcmV0dXJuIHRoaXMuc3RvcmUuc2VsZWN0KGZyb21TZWxlY3RvcnMuZ2V0T3JkZXJzRGF0YSk7IH1cbiAgICBnZXQgT3JkZXJzSGFzQmVlbkZldGNoZWQkKCkgeyByZXR1cm4gdGhpcy5zdG9yZS5zZWxlY3QoZnJvbVNlbGVjdG9ycy5nZXRPcmRlcnNIYXNCZWVuRmV0Y2hlZCk7IH1cbiAgICBnZXQgT3JkZXJzRXJyb3IkKCkgeyByZXR1cm4gdGhpcy5zdG9yZS5zZWxlY3QoZnJvbVNlbGVjdG9ycy5nZXRPcmRlcnNFcnJvcik7IH1cbiAgICBnZXQgT3JkZXJzU3VjY2VzcyQoKSB7IHJldHVybiB0aGlzLnN0b3JlLnNlbGVjdChmcm9tU2VsZWN0b3JzLmdldE9yZGVyc1N1Y2Nlc3MpOyB9XG5cbiAgICAvLyAtLS0tLS0tLS0tLSBQUk9EVUNUUyBNRVRIT0RTOlxuICAgIExvYWRQcm9kdWN0cyA9ICgpID0+IHRoaXMuc3RvcmUuZGlzcGF0Y2goZnJvbVByb2R1Y3RzQWN0aW9ucy5Mb2FkUHJvZHVjdHNCZWdpbkFjdGlvbigpKTtcbiAgICBDcmVhdGVQcm9kdWN0ID0gKHBheWxvYWQ6IElVcHNlcnRQcm9kdWN0UGF5bG9hZCkgPT4gdGhpcy5zdG9yZS5kaXNwYXRjaChmcm9tUHJvZHVjdHNBY3Rpb25zLkNyZWF0ZVByb2R1Y3RCZWdpbkFjdGlvbih7IHBheWxvYWQgfSkpO1xuICAgIFVwZGF0ZVByb2R1Y3QgPSAoXG4gICAgICAgIGlkOiBudW1iZXIsIHBheWxvYWQ6IElVcHNlcnRQcm9kdWN0UGF5bG9hZFxuICAgICkgPT4gdGhpcy5zdG9yZS5kaXNwYXRjaChmcm9tUHJvZHVjdHNBY3Rpb25zLlVwZGF0ZVByb2R1Y3RCZWdpbkFjdGlvbih7IGlkLCBwYXlsb2FkIH0pKVxuICAgIERlbGV0ZVByb2R1Y3QgPSAoaWQ6IG51bWJlcikgPT4gdGhpcy5zdG9yZS5kaXNwYXRjaChmcm9tUHJvZHVjdHNBY3Rpb25zLkRlbGV0ZVByb2R1Y3RCZWdpbkFjdGlvbih7IGlkIH0pKTtcblxuICAgIGdldCBJc0xvYWRpbmdQcm9kdWN0cyQoKSB7IHJldHVybiB0aGlzLnN0b3JlLnNlbGVjdChmcm9tU2VsZWN0b3JzLmdldFN0b3JlUHJvZHVjdHNJc0xvYWRpbmcpOyB9XG4gICAgZ2V0IFByb2R1Y3RzRGF0YSQoKSB7IHJldHVybiB0aGlzLnN0b3JlLnNlbGVjdChmcm9tU2VsZWN0b3JzLmdldFN0b3JlUHJvZHVjdHNEYXRhKTsgfVxuICAgIGdldCBQcm9kdWN0c0hhc0JlZW5GZXRjaGVkJCgpIHsgcmV0dXJuIHRoaXMuc3RvcmUuc2VsZWN0KGZyb21TZWxlY3RvcnMuZ2V0U3RvcmVQcm9kdWN0c0hhc0JlZW5GZXRjaGVkKTsgfVxuICAgIGdldCBQcm9kdWN0c0Vycm9yJCgpIHsgcmV0dXJuIHRoaXMuc3RvcmUuc2VsZWN0KGZyb21TZWxlY3RvcnMuZ2V0U3RvcmVQcm9kdWN0c0Vycm9yKTsgfVxuICAgIGdldCBQcm9kdWN0c1N1Y2Nlc3MkKCkgeyByZXR1cm4gdGhpcy5zdG9yZS5zZWxlY3QoZnJvbVNlbGVjdG9ycy5nZXRTdG9yZVByb2R1Y3RzU3VjY2Vzcyk7IH1cblxuICAgIC8vIC0tLS0tLS0tLS0tIFZBUklBVElPTlMgTUVUSE9EUzpcbiAgICBMb2FkVmFyaWF0aW9ucyA9ICgpID0+IHRoaXMuc3RvcmUuZGlzcGF0Y2goZnJvbVZhcmlhdGlvbnNBY3Rpb25zLkxvYWRWYXJpYXRpb25zQmVnaW5BY3Rpb24oKSk7XG4gICAgQ3JlYXRlVmFyaWF0aW9uID0gKHBheWxvYWQ6IElDcmVhdGVWYXJpYXRpb25QYXlsb2FkKSA9PiB0aGlzLnN0b3JlLmRpc3BhdGNoKFxuICAgICAgICBmcm9tVmFyaWF0aW9uc0FjdGlvbnMuQ3JldGVWYXJpYXRpb25CZWdpbkFjdGlvbih7IHBheWxvYWQgfSlcbiAgICApXG4gICAgRGVsZXRlVmFyaWF0aW9uID0gKHBheWxvYWQ6IG51bWJlcikgPT4gdGhpcy5zdG9yZS5kaXNwYXRjaChmcm9tVmFyaWF0aW9uc0FjdGlvbnMuRGVsdGVWYXJpYXRpb25CZWdpbkFjdGlvbih7IHBheWxvYWQgfSkpO1xuXG4gICAgZ2V0IElzTG9hZGluZ1ZhcmlhdGlvbnMkKCkgeyByZXR1cm4gdGhpcy5zdG9yZS5zZWxlY3QoZnJvbVNlbGVjdG9ycy5nZXRTdG9yZVZhcmlhdGlvbnNJc0xvYWRpbmcpOyB9XG4gICAgZ2V0IFZhcmlhdGlvbnNEYXRhJCgpIHsgcmV0dXJuIHRoaXMuc3RvcmUuc2VsZWN0KGZyb21TZWxlY3RvcnMuZ2V0U3RvcmVWYXJpYXRpb25zRGF0YSk7IH1cbiAgICBnZXQgVmFyaWF0aW9uc0hhc0JlZW5GZXRjaGVkJCgpIHsgcmV0dXJuIHRoaXMuc3RvcmUuc2VsZWN0KGZyb21TZWxlY3RvcnMuZ2V0U3RvcmVWYXJpYXRpb25zSGFzQmVlbkZldGNoZWQpOyB9XG4gICAgZ2V0IFZhcmlhdGlvbnNFcnJvciQoKSB7IHJldHVybiB0aGlzLnN0b3JlLnNlbGVjdChmcm9tU2VsZWN0b3JzLmdldFN0b3JlVmFyaWF0aW9uc0Vycm9yKTsgfVxuICAgIGdldCBWYXJpYXRpb25zU3VjY2VzcyQoKSB7IHJldHVybiB0aGlzLnN0b3JlLnNlbGVjdChmcm9tU2VsZWN0b3JzLmdldFN0b3JlVmFyaWF0aW9uc1N1Y2Nlc3MpOyB9XG5cbiAgICAvLyAtLS0tLS0tLS0tLSBJTUFHRSBNRVRIT0RTOlxuICAgIExvYWRJbWFnZXMgPSAoKSA9PiB0aGlzLnN0b3JlLmRpc3BhdGNoKGZyb21JbWFnZXNBY3Rpb25zLkxvYWRJbWFnZXNCZWdpbkFjdGlvbigpKTtcbiAgICBMb2FkSW1hZ2VCeUlkID0gKGlkOiBudW1iZXIpID0+IHRoaXMuc3RvcmUuZGlzcGF0Y2goZnJvbUltYWdlc0FjdGlvbnMuTG9hZEltYWdlQnlJZEJlZ2luQWN0aW9uKHsgaWQgfSkpO1xuICAgIENyZWF0ZUltYWdlID0gKHBheWxvYWQ6IGFueSkgPT4gdGhpcy5zdG9yZS5kaXNwYXRjaChmcm9tSW1hZ2VzQWN0aW9ucy5DcmVhdGVJbWFnZUJlZ2luQWN0aW9uKHsgcGF5bG9hZCB9KSk7XG4gICAgRGVsZXRlSW1hZ2UgPSAoaWQ6IG51bWJlcikgPT4gdGhpcy5zdG9yZS5kaXNwYXRjaChmcm9tSW1hZ2VzQWN0aW9ucy5EZWxldGVJbWFnZUJlZ2luQWN0aW9uKHsgaWQgfSkpO1xuXG4gICAgZ2V0IElzTG9hZGluZ0ltYWdlcyQoKSB7IHJldHVybiB0aGlzLnN0b3JlLnNlbGVjdChmcm9tU2VsZWN0b3JzLmdldFN0b3JlSW1hZ2VzSXNMb2FkaW5nKTsgfVxuICAgIGdldCBJbWFnZXNEYXRhJCgpIHsgcmV0dXJuIHRoaXMuc3RvcmUuc2VsZWN0KGZyb21TZWxlY3RvcnMuZ2V0U3RvcmVJbWFnZXNEYXRhKTsgfVxuICAgIGdldCBJbWFnZXNIYXNCZWVuRmV0Y2hlZCQoKSB7IHJldHVybiB0aGlzLnN0b3JlLnNlbGVjdChmcm9tU2VsZWN0b3JzLmdldFN0b3JlSW1hZ2VzSGFzQmVlbkZldGNoZWQpOyB9XG4gICAgZ2V0IEltYWdlc0Vycm9yJCgpIHsgcmV0dXJuIHRoaXMuc3RvcmUuc2VsZWN0KGZyb21TZWxlY3RvcnMuZ2V0U3RvcmVJbWFnZXNFcnJvcik7IH1cbiAgICBnZXQgSW1hZ2VzU3VjY2VzcyQoKSB7IHJldHVybiB0aGlzLnN0b3JlLnNlbGVjdChmcm9tU2VsZWN0b3JzLmdldFN0b3JlSW1hZ2VzU3VjY2Vzcyk7IH1cbn1cbiJdfQ==