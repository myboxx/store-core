var ImageModel = /** @class */ (function () {
    function ImageModel(data) {
        this.absolutePath = data.absolutePath;
        this.added = data.added;
        this.alt = data.alt;
        this.baseUrl = data.baseUrl;
        this.checksum = data.checksum;
        this.filesize = data.filesize;
        this.height = data.height;
        this.id = data.id;
        this.image = data.image;
        this.isGlobal = data.isGlobal;
        this.lastIncrement = data.lastIncrement;
        this.mimetype = data.mimetype;
        this.modified = data.modified;
        this.resourceUri = data.resourceUri;
        this.siteId = data.siteId;
        this.sizes = data.sizes;
        this.type = data.type;
        this.userId = data.userId;
        this.versions = data.versions;
        this.whitelabelId = data.whitelabelId;
        this.width = data.width;
    }
    ImageModel.fromApi = function (data) {
        var versions = data.versions.map(function (v) { return ({
            absolutePath: v.absolute_path,
            filesize: v.filesize,
            height: v.height,
            id: v.id,
            image: v.image,
            imageId: v.image_id,
            isGlobal: v.is_global,
            mimetype: v.mimetype,
            resourceUri: v.resource_uri,
            type: v.type,
            width: v.width,
        }); });
        return new ImageModel({
            absolutePath: data.absolute_path,
            added: data.added,
            alt: data.alt,
            baseUrl: data.base_url,
            checksum: data.checksum,
            filesize: data.filesize,
            height: data.height,
            id: data.id,
            image: data.image,
            isGlobal: data.is_global,
            lastIncrement: data.last_increment,
            mimetype: data.mimetype,
            modified: data.modified,
            resourceUri: data.resource_uri,
            siteId: data.site_id,
            sizes: data.sizes,
            type: data.type,
            userId: data.user_id,
            versions: versions,
            whitelabelId: data.whitelabel_id,
            width: data.width,
        });
    };
    ImageModel.empty = function () {
        return new ImageModel({
            absolutePath: null,
            added: null,
            alt: null,
            baseUrl: null,
            checksum: null,
            filesize: null,
            height: null,
            id: null,
            image: null,
            isGlobal: null,
            lastIncrement: null,
            mimetype: null,
            modified: null,
            resourceUri: null,
            siteId: null,
            sizes: null,
            type: null,
            userId: null,
            versions: null,
            whitelabelId: null,
            width: null,
        });
    };
    return ImageModel;
}());
export { ImageModel };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiSW1hZ2UubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYm94eC9zdG9yZS1jb3JlLyIsInNvdXJjZXMiOlsibGliL21vZGVscy9JbWFnZS5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQTtJQUNJLG9CQUFZLElBQXFCO1FBQzdCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQztRQUN0QyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDeEIsSUFBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDO1FBQ3BCLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQztRQUM1QixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7UUFDOUIsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO1FBQzlCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUMxQixJQUFJLENBQUMsRUFBRSxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUM7UUFDbEIsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO1FBQ3hCLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUM5QixJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUM7UUFDeEMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO1FBQzlCLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUM5QixJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUM7UUFDcEMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQzFCLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztRQUN4QixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7UUFDdEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQzFCLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUM5QixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUM7UUFDdEMsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO0lBQzVCLENBQUM7SUF3Qk0sa0JBQU8sR0FBZCxVQUFlLElBQW9CO1FBQy9CLElBQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQztZQUNyQyxZQUFZLEVBQUUsQ0FBQyxDQUFDLGFBQWE7WUFDN0IsUUFBUSxFQUFFLENBQUMsQ0FBQyxRQUFRO1lBQ3BCLE1BQU0sRUFBRSxDQUFDLENBQUMsTUFBTTtZQUNoQixFQUFFLEVBQUUsQ0FBQyxDQUFDLEVBQUU7WUFDUixLQUFLLEVBQUUsQ0FBQyxDQUFDLEtBQUs7WUFDZCxPQUFPLEVBQUUsQ0FBQyxDQUFDLFFBQVE7WUFDbkIsUUFBUSxFQUFFLENBQUMsQ0FBQyxTQUFTO1lBQ3JCLFFBQVEsRUFBRSxDQUFDLENBQUMsUUFBUTtZQUNwQixXQUFXLEVBQUUsQ0FBQyxDQUFDLFlBQVk7WUFDM0IsSUFBSSxFQUFFLENBQUMsQ0FBQyxJQUFJO1lBQ1osS0FBSyxFQUFFLENBQUMsQ0FBQyxLQUFLO1NBQ2pCLENBQUMsRUFac0MsQ0FZdEMsQ0FBQyxDQUFDO1FBRUosT0FBTyxJQUFJLFVBQVUsQ0FBQztZQUNsQixZQUFZLEVBQUUsSUFBSSxDQUFDLGFBQWE7WUFDaEMsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO1lBQ2pCLEdBQUcsRUFBRSxJQUFJLENBQUMsR0FBRztZQUNiLE9BQU8sRUFBRSxJQUFJLENBQUMsUUFBUTtZQUN0QixRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVE7WUFDdkIsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRO1lBQ3ZCLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTTtZQUNuQixFQUFFLEVBQUUsSUFBSSxDQUFDLEVBQUU7WUFDWCxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7WUFDakIsUUFBUSxFQUFFLElBQUksQ0FBQyxTQUFTO1lBQ3hCLGFBQWEsRUFBRSxJQUFJLENBQUMsY0FBYztZQUNsQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVE7WUFDdkIsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRO1lBQ3ZCLFdBQVcsRUFBRSxJQUFJLENBQUMsWUFBWTtZQUM5QixNQUFNLEVBQUUsSUFBSSxDQUFDLE9BQU87WUFDcEIsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO1lBQ2pCLElBQUksRUFBRSxJQUFJLENBQUMsSUFBSTtZQUNmLE1BQU0sRUFBRSxJQUFJLENBQUMsT0FBTztZQUNwQixRQUFRLFVBQUE7WUFDUixZQUFZLEVBQUUsSUFBSSxDQUFDLGFBQWE7WUFDaEMsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO1NBQ3BCLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFTSxnQkFBSyxHQUFaO1FBQ0ksT0FBTyxJQUFJLFVBQVUsQ0FBQztZQUNsQixZQUFZLEVBQUUsSUFBSTtZQUNsQixLQUFLLEVBQUUsSUFBSTtZQUNYLEdBQUcsRUFBRSxJQUFJO1lBQ1QsT0FBTyxFQUFFLElBQUk7WUFDYixRQUFRLEVBQUUsSUFBSTtZQUNkLFFBQVEsRUFBRSxJQUFJO1lBQ2QsTUFBTSxFQUFFLElBQUk7WUFDWixFQUFFLEVBQUUsSUFBSTtZQUNSLEtBQUssRUFBRSxJQUFJO1lBQ1gsUUFBUSxFQUFFLElBQUk7WUFDZCxhQUFhLEVBQUUsSUFBSTtZQUNuQixRQUFRLEVBQUUsSUFBSTtZQUNkLFFBQVEsRUFBRSxJQUFJO1lBQ2QsV0FBVyxFQUFFLElBQUk7WUFDakIsTUFBTSxFQUFFLElBQUk7WUFDWixLQUFLLEVBQUUsSUFBSTtZQUNYLElBQUksRUFBRSxJQUFJO1lBQ1YsTUFBTSxFQUFFLElBQUk7WUFDWixRQUFRLEVBQUUsSUFBSTtZQUNkLFlBQVksRUFBRSxJQUFJO1lBQ2xCLEtBQUssRUFBRSxJQUFJO1NBQ2QsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUNMLGlCQUFDO0FBQUQsQ0FBQyxBQWhIRCxJQWdIQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IElJbWFnZUFwaU1vZGVsIH0gZnJvbSAnLi4vcmVwb3NpdG9yaWVzL0lTdG9yZS5hcGknO1xuXG5leHBvcnQgY2xhc3MgSW1hZ2VNb2RlbCBpbXBsZW1lbnRzIEltYWdlTW9kZWxQb3JwcyB7XG4gICAgY29uc3RydWN0b3IoZGF0YTogSW1hZ2VNb2RlbFBvcnBzKSB7XG4gICAgICAgIHRoaXMuYWJzb2x1dGVQYXRoID0gZGF0YS5hYnNvbHV0ZVBhdGg7XG4gICAgICAgIHRoaXMuYWRkZWQgPSBkYXRhLmFkZGVkO1xuICAgICAgICB0aGlzLmFsdCA9IGRhdGEuYWx0O1xuICAgICAgICB0aGlzLmJhc2VVcmwgPSBkYXRhLmJhc2VVcmw7XG4gICAgICAgIHRoaXMuY2hlY2tzdW0gPSBkYXRhLmNoZWNrc3VtO1xuICAgICAgICB0aGlzLmZpbGVzaXplID0gZGF0YS5maWxlc2l6ZTtcbiAgICAgICAgdGhpcy5oZWlnaHQgPSBkYXRhLmhlaWdodDtcbiAgICAgICAgdGhpcy5pZCA9IGRhdGEuaWQ7XG4gICAgICAgIHRoaXMuaW1hZ2UgPSBkYXRhLmltYWdlO1xuICAgICAgICB0aGlzLmlzR2xvYmFsID0gZGF0YS5pc0dsb2JhbDtcbiAgICAgICAgdGhpcy5sYXN0SW5jcmVtZW50ID0gZGF0YS5sYXN0SW5jcmVtZW50O1xuICAgICAgICB0aGlzLm1pbWV0eXBlID0gZGF0YS5taW1ldHlwZTtcbiAgICAgICAgdGhpcy5tb2RpZmllZCA9IGRhdGEubW9kaWZpZWQ7XG4gICAgICAgIHRoaXMucmVzb3VyY2VVcmkgPSBkYXRhLnJlc291cmNlVXJpO1xuICAgICAgICB0aGlzLnNpdGVJZCA9IGRhdGEuc2l0ZUlkO1xuICAgICAgICB0aGlzLnNpemVzID0gZGF0YS5zaXplcztcbiAgICAgICAgdGhpcy50eXBlID0gZGF0YS50eXBlO1xuICAgICAgICB0aGlzLnVzZXJJZCA9IGRhdGEudXNlcklkO1xuICAgICAgICB0aGlzLnZlcnNpb25zID0gZGF0YS52ZXJzaW9ucztcbiAgICAgICAgdGhpcy53aGl0ZWxhYmVsSWQgPSBkYXRhLndoaXRlbGFiZWxJZDtcbiAgICAgICAgdGhpcy53aWR0aCA9IGRhdGEud2lkdGg7XG4gICAgfVxuXG4gICAgYWJzb2x1dGVQYXRoOiBzdHJpbmc7XG4gICAgYWRkZWQ6IHN0cmluZztcbiAgICBhbHQ/OiBhbnk7XG4gICAgYmFzZVVybD86IHN0cmluZztcbiAgICBjaGVja3N1bTogc3RyaW5nO1xuICAgIGZpbGVzaXplOiBudW1iZXI7XG4gICAgaGVpZ2h0OiBudW1iZXI7XG4gICAgaWQ6IG51bWJlcjtcbiAgICBpbWFnZTogc3RyaW5nO1xuICAgIGlzR2xvYmFsOiBib29sZWFuO1xuICAgIGxhc3RJbmNyZW1lbnQ6IG51bWJlcjtcbiAgICBtaW1ldHlwZTogc3RyaW5nO1xuICAgIG1vZGlmaWVkOiBzdHJpbmc7XG4gICAgcmVzb3VyY2VVcmk6IHN0cmluZztcbiAgICBzaXRlSWQ6IG51bWJlcjtcbiAgICBzaXplczogSUltYWdlTW9kZWxTaXplcztcbiAgICB0eXBlOiBzdHJpbmc7XG4gICAgdXNlcklkOiBudW1iZXI7XG4gICAgdmVyc2lvbnM6IElJbWFnZU1vZGVsVmVyc2lvbnNbXTtcbiAgICB3aGl0ZWxhYmVsSWQ6IG51bWJlcjtcbiAgICB3aWR0aDogbnVtYmVyO1xuXG4gICAgc3RhdGljIGZyb21BcGkoZGF0YTogSUltYWdlQXBpTW9kZWwpIHtcbiAgICAgICAgY29uc3QgdmVyc2lvbnMgPSBkYXRhLnZlcnNpb25zLm1hcCh2ID0+ICh7XG4gICAgICAgICAgICBhYnNvbHV0ZVBhdGg6IHYuYWJzb2x1dGVfcGF0aCxcbiAgICAgICAgICAgIGZpbGVzaXplOiB2LmZpbGVzaXplLFxuICAgICAgICAgICAgaGVpZ2h0OiB2LmhlaWdodCxcbiAgICAgICAgICAgIGlkOiB2LmlkLFxuICAgICAgICAgICAgaW1hZ2U6IHYuaW1hZ2UsXG4gICAgICAgICAgICBpbWFnZUlkOiB2LmltYWdlX2lkLFxuICAgICAgICAgICAgaXNHbG9iYWw6IHYuaXNfZ2xvYmFsLFxuICAgICAgICAgICAgbWltZXR5cGU6IHYubWltZXR5cGUsXG4gICAgICAgICAgICByZXNvdXJjZVVyaTogdi5yZXNvdXJjZV91cmksXG4gICAgICAgICAgICB0eXBlOiB2LnR5cGUsXG4gICAgICAgICAgICB3aWR0aDogdi53aWR0aCxcbiAgICAgICAgfSkpO1xuXG4gICAgICAgIHJldHVybiBuZXcgSW1hZ2VNb2RlbCh7XG4gICAgICAgICAgICBhYnNvbHV0ZVBhdGg6IGRhdGEuYWJzb2x1dGVfcGF0aCxcbiAgICAgICAgICAgIGFkZGVkOiBkYXRhLmFkZGVkLFxuICAgICAgICAgICAgYWx0OiBkYXRhLmFsdCxcbiAgICAgICAgICAgIGJhc2VVcmw6IGRhdGEuYmFzZV91cmwsXG4gICAgICAgICAgICBjaGVja3N1bTogZGF0YS5jaGVja3N1bSxcbiAgICAgICAgICAgIGZpbGVzaXplOiBkYXRhLmZpbGVzaXplLFxuICAgICAgICAgICAgaGVpZ2h0OiBkYXRhLmhlaWdodCxcbiAgICAgICAgICAgIGlkOiBkYXRhLmlkLFxuICAgICAgICAgICAgaW1hZ2U6IGRhdGEuaW1hZ2UsXG4gICAgICAgICAgICBpc0dsb2JhbDogZGF0YS5pc19nbG9iYWwsXG4gICAgICAgICAgICBsYXN0SW5jcmVtZW50OiBkYXRhLmxhc3RfaW5jcmVtZW50LFxuICAgICAgICAgICAgbWltZXR5cGU6IGRhdGEubWltZXR5cGUsXG4gICAgICAgICAgICBtb2RpZmllZDogZGF0YS5tb2RpZmllZCxcbiAgICAgICAgICAgIHJlc291cmNlVXJpOiBkYXRhLnJlc291cmNlX3VyaSxcbiAgICAgICAgICAgIHNpdGVJZDogZGF0YS5zaXRlX2lkLFxuICAgICAgICAgICAgc2l6ZXM6IGRhdGEuc2l6ZXMsXG4gICAgICAgICAgICB0eXBlOiBkYXRhLnR5cGUsXG4gICAgICAgICAgICB1c2VySWQ6IGRhdGEudXNlcl9pZCxcbiAgICAgICAgICAgIHZlcnNpb25zLFxuICAgICAgICAgICAgd2hpdGVsYWJlbElkOiBkYXRhLndoaXRlbGFiZWxfaWQsXG4gICAgICAgICAgICB3aWR0aDogZGF0YS53aWR0aCxcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgc3RhdGljIGVtcHR5KCkge1xuICAgICAgICByZXR1cm4gbmV3IEltYWdlTW9kZWwoe1xuICAgICAgICAgICAgYWJzb2x1dGVQYXRoOiBudWxsLFxuICAgICAgICAgICAgYWRkZWQ6IG51bGwsXG4gICAgICAgICAgICBhbHQ6IG51bGwsXG4gICAgICAgICAgICBiYXNlVXJsOiBudWxsLFxuICAgICAgICAgICAgY2hlY2tzdW06IG51bGwsXG4gICAgICAgICAgICBmaWxlc2l6ZTogbnVsbCxcbiAgICAgICAgICAgIGhlaWdodDogbnVsbCxcbiAgICAgICAgICAgIGlkOiBudWxsLFxuICAgICAgICAgICAgaW1hZ2U6IG51bGwsXG4gICAgICAgICAgICBpc0dsb2JhbDogbnVsbCxcbiAgICAgICAgICAgIGxhc3RJbmNyZW1lbnQ6IG51bGwsXG4gICAgICAgICAgICBtaW1ldHlwZTogbnVsbCxcbiAgICAgICAgICAgIG1vZGlmaWVkOiBudWxsLFxuICAgICAgICAgICAgcmVzb3VyY2VVcmk6IG51bGwsXG4gICAgICAgICAgICBzaXRlSWQ6IG51bGwsXG4gICAgICAgICAgICBzaXplczogbnVsbCxcbiAgICAgICAgICAgIHR5cGU6IG51bGwsXG4gICAgICAgICAgICB1c2VySWQ6IG51bGwsXG4gICAgICAgICAgICB2ZXJzaW9uczogbnVsbCxcbiAgICAgICAgICAgIHdoaXRlbGFiZWxJZDogbnVsbCxcbiAgICAgICAgICAgIHdpZHRoOiBudWxsLFxuICAgICAgICB9KTtcbiAgICB9XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgSW1hZ2VNb2RlbFBvcnBzIHtcbiAgICBhYnNvbHV0ZVBhdGg6IHN0cmluZztcbiAgICBhZGRlZDogc3RyaW5nO1xuICAgIGFsdD86IGFueTtcbiAgICBiYXNlVXJsPzogc3RyaW5nO1xuICAgIGNoZWNrc3VtOiBzdHJpbmc7XG4gICAgZmlsZXNpemU6IG51bWJlcjtcbiAgICBoZWlnaHQ6IG51bWJlcjtcbiAgICBpZDogbnVtYmVyO1xuICAgIGltYWdlOiBzdHJpbmc7XG4gICAgaXNHbG9iYWw6IGJvb2xlYW47XG4gICAgbGFzdEluY3JlbWVudDogbnVtYmVyO1xuICAgIG1pbWV0eXBlOiBzdHJpbmc7XG4gICAgbW9kaWZpZWQ6IHN0cmluZztcbiAgICByZXNvdXJjZVVyaTogc3RyaW5nO1xuICAgIHNpdGVJZDogbnVtYmVyO1xuICAgIHNpemVzOiBJSW1hZ2VNb2RlbFNpemVzO1xuICAgIHR5cGU6IHN0cmluZztcbiAgICB1c2VySWQ6IG51bWJlcjtcbiAgICB2ZXJzaW9uczogQXJyYXk8SUltYWdlTW9kZWxWZXJzaW9ucz47XG4gICAgd2hpdGVsYWJlbElkOiBudW1iZXI7XG4gICAgd2lkdGg6IG51bWJlcjtcbn1cblxuZXhwb3J0IGludGVyZmFjZSBJSW1hZ2VNb2RlbFNpemVzIHtcbiAgICBiaWc6IHN0cmluZztcbiAgICBsYXJnZTogc3RyaW5nO1xuICAgIG1lZGl1bTogc3RyaW5nO1xuICAgIG9yaWdpbmFsOiBzdHJpbmc7XG4gICAgc21hbGw6IHN0cmluZztcbiAgICB0aHVtYm5haWw6IHN0cmluZztcbn1cblxuZXhwb3J0IGludGVyZmFjZSBJSW1hZ2VNb2RlbFZlcnNpb25zIHtcbiAgICBhYnNvbHV0ZVBhdGg6IHN0cmluZztcbiAgICBmaWxlc2l6ZTogbnVtYmVyO1xuICAgIGhlaWdodDogbnVtYmVyO1xuICAgIGlkOiBudW1iZXI7XG4gICAgaW1hZ2U6IHN0cmluZztcbiAgICBpbWFnZUlkOiBudW1iZXI7XG4gICAgaXNHbG9iYWw6IGJvb2xlYW47XG4gICAgbWltZXR5cGU6IHN0cmluZztcbiAgICByZXNvdXJjZVVyaTogc3RyaW5nO1xuICAgIHR5cGU6IHN0cmluZztcbiAgICB3aWR0aDogbnVtYmVyO1xufVxuIl19