var VariationModel = /** @class */ (function () {
    function VariationModel(data) {
        this.id = data.id;
        this.name = data.name;
        this.options = data.options;
        this.required = data.required;
        this.shop = data.shop;
        this.shopId = data.shopId;
    }
    VariationModel.fromApi = function (data) {
        var options = data.options.map(function (o) { return ({
            id: o.id,
            name: o.name,
            order: o.order,
            price: o.price,
            variationId: o.variation_id
        }); });
        return new VariationModel({
            id: data.id,
            name: data.name,
            options: options,
            required: data.required,
            resourceUri: data.resource_uri,
            shop: data.shop,
            shopId: data.shop_id,
        });
    };
    VariationModel.empty = function () {
        return new VariationModel({
            id: null,
            name: null,
            options: null,
            required: null,
            resourceUri: null,
            shop: null,
            shopId: null,
        });
    };
    return VariationModel;
}());
export { VariationModel };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiVmFyaWF0aW9uLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGJveHgvc3RvcmUtY29yZS8iLCJzb3VyY2VzIjpbImxpYi9tb2RlbHMvVmFyaWF0aW9uLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBO0lBQ0ksd0JBQVksSUFBeUI7UUFDakMsSUFBSSxDQUFDLEVBQUUsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDO1FBQ2xCLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztRQUN0QixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUM7UUFDNUIsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO1FBQzlCLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztRQUN0QixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7SUFDOUIsQ0FBQztJQVVNLHNCQUFPLEdBQWQsVUFBZSxJQUF3QjtRQUNuQyxJQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUM7WUFDbkMsRUFBRSxFQUFFLENBQUMsQ0FBQyxFQUFFO1lBQ1IsSUFBSSxFQUFFLENBQUMsQ0FBQyxJQUFJO1lBQ1osS0FBSyxFQUFFLENBQUMsQ0FBQyxLQUFLO1lBQ2QsS0FBSyxFQUFFLENBQUMsQ0FBQyxLQUFLO1lBQ2QsV0FBVyxFQUFFLENBQUMsQ0FBQyxZQUFZO1NBQzlCLENBQUMsRUFOb0MsQ0FNcEMsQ0FBQyxDQUFDO1FBRUosT0FBTyxJQUFJLGNBQWMsQ0FBQztZQUN0QixFQUFFLEVBQUUsSUFBSSxDQUFDLEVBQUU7WUFDWCxJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUk7WUFDZixPQUFPLFNBQUE7WUFDUCxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVE7WUFDdkIsV0FBVyxFQUFFLElBQUksQ0FBQyxZQUFZO1lBQzlCLElBQUksRUFBRSxJQUFJLENBQUMsSUFBSTtZQUNmLE1BQU0sRUFBRSxJQUFJLENBQUMsT0FBTztTQUN2QixDQUFDLENBQUM7SUFDUCxDQUFDO0lBRU0sb0JBQUssR0FBWjtRQUNJLE9BQU8sSUFBSSxjQUFjLENBQUM7WUFDdEIsRUFBRSxFQUFFLElBQUk7WUFDUixJQUFJLEVBQUUsSUFBSTtZQUNWLE9BQU8sRUFBRSxJQUFJO1lBQ2IsUUFBUSxFQUFFLElBQUk7WUFDZCxXQUFXLEVBQUUsSUFBSTtZQUNqQixJQUFJLEVBQUUsSUFBSTtZQUNWLE1BQU0sRUFBRSxJQUFJO1NBQ2YsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUNKLHFCQUFDO0FBQUQsQ0FBQyxBQWpERixJQWlERSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IElWYXJpYXRpb25BcGlNb2RlbCB9IGZyb20gJy4uL3JlcG9zaXRvcmllcy9JU3RvcmUuYXBpJztcblxuZXhwb3J0IGNsYXNzIFZhcmlhdGlvbk1vZGVsIGltcGxlbWVudHMgVmFyaWF0aW9uTW9kZWxQcm9wcyB7XG4gICAgY29uc3RydWN0b3IoZGF0YTogVmFyaWF0aW9uTW9kZWxQcm9wcyl7XG4gICAgICAgIHRoaXMuaWQgPSBkYXRhLmlkO1xuICAgICAgICB0aGlzLm5hbWUgPSBkYXRhLm5hbWU7XG4gICAgICAgIHRoaXMub3B0aW9ucyA9IGRhdGEub3B0aW9ucztcbiAgICAgICAgdGhpcy5yZXF1aXJlZCA9IGRhdGEucmVxdWlyZWQ7XG4gICAgICAgIHRoaXMuc2hvcCA9IGRhdGEuc2hvcDtcbiAgICAgICAgdGhpcy5zaG9wSWQgPSBkYXRhLnNob3BJZDtcbiAgICB9XG5cbiAgICBpZDogbnVtYmVyO1xuICAgIG5hbWU6IHN0cmluZztcbiAgICBvcHRpb25zOiB7IGlkOiBudW1iZXI7IG5hbWU6IHN0cmluZzsgb3JkZXI6IG51bWJlcjsgcHJpY2U6IHN0cmluZzsgdmFyaWF0aW9uSWQ6IG51bWJlcjsgfVtdO1xuICAgIHJlcXVpcmVkOiBib29sZWFuO1xuICAgIHJlc291cmNlVXJpOiBzdHJpbmc7XG4gICAgc2hvcD86IG51bWJlcjtcbiAgICBzaG9wSWQ6IG51bWJlcjtcblxuICAgIHN0YXRpYyBmcm9tQXBpKGRhdGE6IElWYXJpYXRpb25BcGlNb2RlbCkge1xuICAgICAgICBjb25zdCBvcHRpb25zID0gZGF0YS5vcHRpb25zLm1hcChvID0+ICh7XG4gICAgICAgICAgICBpZDogby5pZCxcbiAgICAgICAgICAgIG5hbWU6IG8ubmFtZSxcbiAgICAgICAgICAgIG9yZGVyOiBvLm9yZGVyLFxuICAgICAgICAgICAgcHJpY2U6IG8ucHJpY2UsXG4gICAgICAgICAgICB2YXJpYXRpb25JZDogby52YXJpYXRpb25faWRcbiAgICAgICAgfSkpO1xuXG4gICAgICAgIHJldHVybiBuZXcgVmFyaWF0aW9uTW9kZWwoe1xuICAgICAgICAgICAgaWQ6IGRhdGEuaWQsXG4gICAgICAgICAgICBuYW1lOiBkYXRhLm5hbWUsXG4gICAgICAgICAgICBvcHRpb25zLFxuICAgICAgICAgICAgcmVxdWlyZWQ6IGRhdGEucmVxdWlyZWQsXG4gICAgICAgICAgICByZXNvdXJjZVVyaTogZGF0YS5yZXNvdXJjZV91cmksXG4gICAgICAgICAgICBzaG9wOiBkYXRhLnNob3AsXG4gICAgICAgICAgICBzaG9wSWQ6IGRhdGEuc2hvcF9pZCxcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgc3RhdGljIGVtcHR5KCkge1xuICAgICAgICByZXR1cm4gbmV3IFZhcmlhdGlvbk1vZGVsKHtcbiAgICAgICAgICAgIGlkOiBudWxsLFxuICAgICAgICAgICAgbmFtZTogbnVsbCxcbiAgICAgICAgICAgIG9wdGlvbnM6IG51bGwsXG4gICAgICAgICAgICByZXF1aXJlZDogbnVsbCxcbiAgICAgICAgICAgIHJlc291cmNlVXJpOiBudWxsLFxuICAgICAgICAgICAgc2hvcDogbnVsbCxcbiAgICAgICAgICAgIHNob3BJZDogbnVsbCxcbiAgICAgICAgfSk7XG4gICAgfVxuIH1cblxuZXhwb3J0IGludGVyZmFjZSBWYXJpYXRpb25Nb2RlbFByb3BzIHtcbiAgICBpZDogbnVtYmVyO1xuICAgIG5hbWU6IHN0cmluZztcbiAgICBvcHRpb25zOiBBcnJheTx7XG4gICAgICAgIGlkOiBudW1iZXI7XG4gICAgICAgIG5hbWU6IHN0cmluZztcbiAgICAgICAgb3JkZXI6IG51bWJlcjtcbiAgICAgICAgcHJpY2U6IHN0cmluZztcbiAgICAgICAgdmFyaWF0aW9uSWQ6IG51bWJlcjtcbiAgICB9PjtcbiAgICByZXF1aXJlZDogYm9vbGVhbjtcbiAgICByZXNvdXJjZVVyaTogc3RyaW5nO1xuICAgIHNob3A/OiBudW1iZXI7XG4gICAgc2hvcElkOiBudW1iZXI7XG59XG4iXX0=