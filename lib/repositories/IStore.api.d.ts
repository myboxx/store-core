export interface ICategoryApiModel {
    active: boolean;
    category_images?: {
        [key: string]: {
            id: number;
            url: string;
        };
    } | [];
    description: string;
    id: number;
    name: string;
    parent_id?: any;
    resource_uri: string;
    shop_id: number;
    slug: string;
}
export interface IUpsertCategoryPayload {
    name: string;
    description: string;
    category_images: number;
    active?: boolean;
}
export interface IOrderApiModel {
    buyer_email: string;
    buyer_name: string;
    id: number;
    price: number;
    resource_uri: string;
    session_id: string;
    ship_to_address: string;
    ship_to_city: string;
    ship_to_country: string;
    ship_to_postal_code: string;
    ship_to_state?: any;
    shipping_cost?: any;
    shipping_id: string;
    shipping_name: string;
    site_id: number;
    status: string;
    timestamp: string;
    transaction_id?: any;
    whitelabel_id: number;
}
export interface IProductApiModel {
    active: boolean;
    added: string;
    asin?: any;
    auto_stock: boolean;
    brand?: any;
    categories: ICategoryApiModel[];
    description: string;
    excerpt: string;
    height: string;
    id: number;
    image: IImageApiModel;
    image_id?: number;
    length: string;
    msrp_price?: any;
    outgoing_url: string;
    price: string;
    primary_image_url: string;
    product_images: any[] | {
        [key: string]: {
            id: number;
            url: string;
        };
    };
    published: boolean;
    quantity: number;
    resource_uri: string;
    sale_price?: any;
    shop_id: number;
    sku: string;
    slug: string;
    status?: any;
    title: string;
    upc?: any;
    variations: IVariationApiModel[];
    vendor?: any;
    weight: string;
    weight_unit: string;
    wholesale_price: string;
    width: string;
}
export interface IUpsertProductPayload {
    sku: string;
    title: string;
    description: string;
    excerpt: string;
    price: number;
    wholesale_price: number;
    quantity: number;
    auto_stock: boolean;
    weight: number;
    categories: string;
    published: boolean;
    outgoing_url: string;
    variations?: string;
    product_images?: string;
}
export interface IVariationApiModel {
    id: number;
    name: string;
    options: IVariationOptionApiModel[];
    required: boolean;
    resource_uri: string;
    shop?: number;
    shop_id: number;
}
export interface IVariationOptionApiModel {
    id: number;
    name: string;
    order: number;
    price: string;
    variation_id: number;
}
export interface ICreateVariationPayload {
    name: string;
    required: boolean;
    options: Array<any[]>;
}
export interface IImageApiModel {
    absolute_path: string;
    added: string;
    alt?: any;
    base_url: string;
    checksum: string;
    filesize: number;
    height: number;
    id: number;
    image: string;
    is_global: boolean;
    last_increment: number;
    mimetype: string;
    modified: string;
    resource_uri: string;
    site_id: number;
    sizes: IImageSizesApiModel;
    type: string;
    user_id: number;
    versions: IImageVersionApiModel[];
    whitelabel_id: number;
    width: number;
}
export interface IImageVersionApiModel {
    absolute_path: string;
    filesize: number;
    height: number;
    id: number;
    image: string;
    image_id: number;
    is_global: boolean;
    mimetype: string;
    resource_uri: string;
    type: string;
    width: number;
}
export interface IImageSizesApiModel {
    big: string;
    large: string;
    medium: string;
    original: string;
    small: string;
    thumbnail: string;
}
