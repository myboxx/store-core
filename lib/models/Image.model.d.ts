import { IImageApiModel } from '../repositories/IStore.api';
export declare class ImageModel implements ImageModelPorps {
    constructor(data: ImageModelPorps);
    absolutePath: string;
    added: string;
    alt?: any;
    baseUrl?: string;
    checksum: string;
    filesize: number;
    height: number;
    id: number;
    image: string;
    isGlobal: boolean;
    lastIncrement: number;
    mimetype: string;
    modified: string;
    resourceUri: string;
    siteId: number;
    sizes: IImageModelSizes;
    type: string;
    userId: number;
    versions: IImageModelVersions[];
    whitelabelId: number;
    width: number;
    static fromApi(data: IImageApiModel): ImageModel;
    static empty(): ImageModel;
}
export interface ImageModelPorps {
    absolutePath: string;
    added: string;
    alt?: any;
    baseUrl?: string;
    checksum: string;
    filesize: number;
    height: number;
    id: number;
    image: string;
    isGlobal: boolean;
    lastIncrement: number;
    mimetype: string;
    modified: string;
    resourceUri: string;
    siteId: number;
    sizes: IImageModelSizes;
    type: string;
    userId: number;
    versions: Array<IImageModelVersions>;
    whitelabelId: number;
    width: number;
}
export interface IImageModelSizes {
    big: string;
    large: string;
    medium: string;
    original: string;
    small: string;
    thumbnail: string;
}
export interface IImageModelVersions {
    absolutePath: string;
    filesize: number;
    height: number;
    id: number;
    image: string;
    imageId: number;
    isGlobal: boolean;
    mimetype: string;
    resourceUri: string;
    type: string;
    width: number;
}
