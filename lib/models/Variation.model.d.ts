import { IVariationApiModel } from '../repositories/IStore.api';
export declare class VariationModel implements VariationModelProps {
    constructor(data: VariationModelProps);
    id: number;
    name: string;
    options: {
        id: number;
        name: string;
        order: number;
        price: string;
        variationId: number;
    }[];
    required: boolean;
    resourceUri: string;
    shop?: number;
    shopId: number;
    static fromApi(data: IVariationApiModel): VariationModel;
    static empty(): VariationModel;
}
export interface VariationModelProps {
    id: number;
    name: string;
    options: Array<{
        id: number;
        name: string;
        order: number;
        price: string;
        variationId: number;
    }>;
    required: boolean;
    resourceUri: string;
    shop?: number;
    shopId: number;
}
