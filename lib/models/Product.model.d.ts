import { IProductApiModel, IVariationApiModel } from '../repositories/IStore.api';
import { CategoryModel } from './Category.model';
export declare class ProductModel implements ProductModelProps {
    constructor(data: ProductModelProps);
    active: boolean;
    added?: string;
    asin?: any;
    autoStock: boolean;
    brand?: any;
    categories: CategoryModel[];
    description: string;
    excerpt: string;
    height: string;
    id: number;
    image: {
        id: number;
        url: string;
    };
    imageId?: number;
    length: string;
    msrpPrice?: any;
    outgoingUrl: string;
    price: string;
    primaryImageUrl: string;
    productImages: {
        [key: string]: {
            id: number;
            url: string;
        };
    };
    published: boolean;
    quantity: number;
    resourceUri: string;
    salePrice: string;
    shopId: number;
    sku: string;
    slug: string;
    status: string;
    title: string;
    upc: any;
    variations: ProductVariationModel[];
    vendor: any;
    weight: string;
    weightUnit?: string;
    wholesalePrice: string;
    width: string;
    static fromApi(data: IProductApiModel): ProductModel;
    static empty(): ProductModel;
}
export interface ProductModelProps {
    active: boolean;
    added?: string;
    asin?: any;
    autoStock: boolean;
    brand?: any;
    categories: CategoryModel[];
    description: string;
    excerpt: string;
    height: string;
    id: number;
    image: {
        id: number;
        url: string;
    };
    imageId?: number;
    length: string;
    msrpPrice?: any;
    outgoingUrl: string;
    price: string;
    primaryImageUrl: string;
    productImages: {
        [key: string]: {
            id: number;
            url: string;
        };
    };
    published: boolean;
    quantity: number;
    resourceUri: string;
    salePrice: string;
    shopId: number;
    sku: string;
    slug: string;
    status: string;
    title: string;
    upc: any;
    variations: ProductVariationModel[];
    vendor: any;
    weight: string;
    weightUnit?: string;
    wholesalePrice: string;
    width: string;
}
export declare class ProductVariationModel implements ProductVariationModelProps {
    constructor(data: ProductVariationModelProps);
    id: number;
    name: string;
    options: Array<{
        id: number;
        name: string;
        order: number;
        price: string;
        variationId: number;
    }>;
    required: boolean;
    resourceUri: string;
    shop?: number;
    shopId: number;
    static fromApi(data: IVariationApiModel): ProductVariationModel;
    static empty(): ProductVariationModel;
}
export interface ProductVariationModelProps {
    id: number;
    name: string;
    options: Array<{
        id: number;
        name: string;
        order: number;
        price: string;
        variationId: number;
    }>;
    required: boolean;
    resourceUri: string;
    shop?: number;
    shopId: number;
}
