import { ICategoryApiModel } from '../repositories/IStore.api';
export declare class CategoryModel implements CategoryModelProps {
    constructor(data: CategoryModelProps);
    active: boolean;
    categoryImages: number[];
    description: string;
    id: number;
    name: string;
    parentId?: any;
    resourceUri: string;
    shopId: number;
    slug: string;
    static fromApi(data: ICategoryApiModel): CategoryModel;
    static empty(): CategoryModel;
}
export interface CategoryModelProps {
    active: boolean;
    categoryImages: number[];
    description: string;
    id: number;
    name: string;
    parentId?: any;
    resourceUri: string;
    shopId: number;
    slug: string;
}
