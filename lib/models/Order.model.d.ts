import { IOrderApiModel } from '../repositories/IStore.api';
export declare class OrderModel implements OrderModelProps {
    constructor(data: OrderModelProps);
    buyerEmail: string;
    buyerName: string;
    id: number;
    price: number;
    resourceUri: string;
    sessionId: string;
    shipToAddress: string;
    shipToCity: string;
    shipToCountry: string;
    shipToPostalCode: string;
    shipToState?: any;
    shippingCost?: any;
    shippingId: string;
    shippingName: string;
    siteId: number;
    status: 'new' | string;
    timestamp: string;
    transactionId?: any;
    whitelabelId: number;
    static fromApi(data: IOrderApiModel): OrderModel;
    static empty(): OrderModel;
}
export interface OrderModelProps {
    buyerEmail: string;
    buyerName: string;
    id: number;
    price: number;
    resourceUri: string;
    sessionId: string;
    shipToAddress: string;
    shipToCity: string;
    shipToCountry: string;
    shipToPostalCode: string;
    shipToState?: any;
    shippingCost?: any;
    shippingId: string;
    shippingName: string;
    siteId: number;
    status: 'new' | string;
    timestamp: string;
    transactionId?: any;
    whitelabelId: number;
}
