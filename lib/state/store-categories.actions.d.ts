import { CategoryModel } from '../models/Category.model';
import { IUpsertCategoryPayload } from '../repositories/IStore.api';
export declare enum StoreCategoriesActionTypes {
    LoadCategoriesBegin = "[STORE] Load Categories Begin",
    LoadCategoriesSuccess = "[STORE] Load Categories Success",
    LoadCategoriesFail = "[STORE] Load Categories Fail",
    LoadCategoryByIdBegin = "[STORE] Load LoadCategory By Id Begin",
    LoadCategoryByIdSuccess = "[STORE] Load Categorie By Id Success",
    LoadCategoryByIdFail = "[STORE] Load Categorie By Id Fail",
    CreateCategoryBegin = "[STORE] Create Category Begin",
    CreateCategorySuccess = "[STORE] Create Category Success",
    CreateCategoryFail = "[STORE] Create Category Fail",
    DeleteCategoryBegin = "[STORE] Delete Category Begin",
    DeleteCategorySuccess = "[STORE] Delete Category Success",
    DeleteCategoryFail = "[STORE] Delete Category Fail",
    ClearCategoriesSuccessErrorData = "[STORE] Clear Categories Success/Error Data"
}
export declare const LoadCategoriesBeginAction: import("@ngrx/store").ActionCreator<StoreCategoriesActionTypes.LoadCategoriesBegin, () => import("@ngrx/store/src/models").TypedAction<StoreCategoriesActionTypes.LoadCategoriesBegin>>;
export declare const LoadCategoriesSuccessAction: import("@ngrx/store").ActionCreator<StoreCategoriesActionTypes.LoadCategoriesSuccess, (props: {
    payload: CategoryModel[];
}) => {
    payload: CategoryModel[];
} & import("@ngrx/store/src/models").TypedAction<StoreCategoriesActionTypes.LoadCategoriesSuccess>>;
export declare const LoadCategoriesFailAction: import("@ngrx/store").ActionCreator<StoreCategoriesActionTypes.LoadCategoriesFail, (props: {
    errors: any;
}) => {
    errors: any;
} & import("@ngrx/store/src/models").TypedAction<StoreCategoriesActionTypes.LoadCategoriesFail>>;
export declare const LoadCategorieByIdBeginAction: import("@ngrx/store").ActionCreator<StoreCategoriesActionTypes.LoadCategoryByIdBegin, (props: {
    id: number;
}) => {
    id: number;
} & import("@ngrx/store/src/models").TypedAction<StoreCategoriesActionTypes.LoadCategoryByIdBegin>>;
export declare const LoadCategorieByIdSuccessAction: import("@ngrx/store").ActionCreator<StoreCategoriesActionTypes.LoadCategoryByIdSuccess, (props: {
    payload: CategoryModel;
}) => {
    payload: CategoryModel;
} & import("@ngrx/store/src/models").TypedAction<StoreCategoriesActionTypes.LoadCategoryByIdSuccess>>;
export declare const LoadCategorieByIdFailAction: import("@ngrx/store").ActionCreator<StoreCategoriesActionTypes.LoadCategoryByIdFail, (props: {
    errors: any;
}) => {
    errors: any;
} & import("@ngrx/store/src/models").TypedAction<StoreCategoriesActionTypes.LoadCategoryByIdFail>>;
export declare const CreateCategoryBeginAction: import("@ngrx/store").ActionCreator<StoreCategoriesActionTypes.CreateCategoryBegin, (props: {
    payload: IUpsertCategoryPayload;
}) => {
    payload: IUpsertCategoryPayload;
} & import("@ngrx/store/src/models").TypedAction<StoreCategoriesActionTypes.CreateCategoryBegin>>;
export declare const CreateCategorySuccessAction: import("@ngrx/store").ActionCreator<StoreCategoriesActionTypes.CreateCategorySuccess, (props: {
    payload: CategoryModel[];
}) => {
    payload: CategoryModel[];
} & import("@ngrx/store/src/models").TypedAction<StoreCategoriesActionTypes.CreateCategorySuccess>>;
export declare const CreateCategoryFailAction: import("@ngrx/store").ActionCreator<StoreCategoriesActionTypes.CreateCategoryFail, (props: {
    errors: any;
}) => {
    errors: any;
} & import("@ngrx/store/src/models").TypedAction<StoreCategoriesActionTypes.CreateCategoryFail>>;
export declare const DeleteCategoryBeginAction: import("@ngrx/store").ActionCreator<StoreCategoriesActionTypes.DeleteCategoryBegin, (props: {
    id: number;
}) => {
    id: number;
} & import("@ngrx/store/src/models").TypedAction<StoreCategoriesActionTypes.DeleteCategoryBegin>>;
export declare const DeleteCategorySuccessAction: import("@ngrx/store").ActionCreator<StoreCategoriesActionTypes.DeleteCategorySuccess, (props: {
    payload: CategoryModel[];
}) => {
    payload: CategoryModel[];
} & import("@ngrx/store/src/models").TypedAction<StoreCategoriesActionTypes.DeleteCategorySuccess>>;
export declare const DeleteCategoryFailAction: import("@ngrx/store").ActionCreator<StoreCategoriesActionTypes.DeleteCategoryFail, (props: {
    errors: any;
}) => {
    errors: any;
} & import("@ngrx/store/src/models").TypedAction<StoreCategoriesActionTypes.DeleteCategoryFail>>;
export declare const ClearCategoriesSuccessErrorDataAction: import("@ngrx/store").ActionCreator<StoreCategoriesActionTypes.ClearCategoriesSuccessErrorData, () => import("@ngrx/store/src/models").TypedAction<StoreCategoriesActionTypes.ClearCategoriesSuccessErrorData>>;
