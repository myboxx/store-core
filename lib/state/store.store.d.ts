import { Store } from '@ngrx/store';
import { ICreateVariationPayload, IUpsertCategoryPayload, IUpsertProductPayload } from '../repositories/IStore.api';
import * as fromReducer from './store.reducer';
export declare class StorePageStore {
    store: Store<fromReducer.IStoreState>;
    constructor(store: Store<fromReducer.IStoreState>);
    LoadCategories: () => void;
    loadCategoryById: (id: number) => void;
    CreateCategory: (payload: IUpsertCategoryPayload) => void;
    DeleteCategory: (id: number) => void;
    get IsLoadingCategories$(): import("rxjs").Observable<boolean>;
    get CategoriesData$(): import("rxjs").Observable<import("../models/Category.model").CategoryModel[]>;
    get CategoriesHasBeenFetched$(): import("rxjs").Observable<boolean>;
    get CategoriesError$(): import("rxjs").Observable<import("../core/IStateErrorSuccess").IStoreCategoriesStateError>;
    get CategoriesSuccess$(): import("rxjs").Observable<import("../core/IStateErrorSuccess").IStoreCategoriesStateSuccess>;
    LoadOrders: () => void;
    loadOrderById: (id: number) => void;
    updateOrder: (id: number, payload: any) => void;
    get IsLoadingOrders$(): import("rxjs").Observable<boolean>;
    get OrdersData$(): import("rxjs").Observable<import("../models/Order.model").OrderModel[]>;
    get OrdersHasBeenFetched$(): import("rxjs").Observable<boolean>;
    get OrdersError$(): import("rxjs").Observable<import("../core/IStateErrorSuccess").IStoreOrdersStateError>;
    get OrdersSuccess$(): import("rxjs").Observable<import("../core/IStateErrorSuccess").IStoreOrdersStateSuccess>;
    LoadProducts: () => void;
    CreateProduct: (payload: IUpsertProductPayload) => void;
    UpdateProduct: (id: number, payload: IUpsertProductPayload) => void;
    DeleteProduct: (id: number) => void;
    get IsLoadingProducts$(): import("rxjs").Observable<boolean>;
    get ProductsData$(): import("rxjs").Observable<import("../models/Product.model").ProductModel[]>;
    get ProductsHasBeenFetched$(): import("rxjs").Observable<boolean>;
    get ProductsError$(): import("rxjs").Observable<import("../core/IStateErrorSuccess").IStoreProductsStateError>;
    get ProductsSuccess$(): import("rxjs").Observable<import("../core/IStateErrorSuccess").IStoreProductsStateSuccess>;
    LoadVariations: () => void;
    CreateVariation: (payload: ICreateVariationPayload) => void;
    DeleteVariation: (payload: number) => void;
    get IsLoadingVariations$(): import("rxjs").Observable<boolean>;
    get VariationsData$(): import("rxjs").Observable<import("../models/Variation.model").VariationModel[]>;
    get VariationsHasBeenFetched$(): import("rxjs").Observable<boolean>;
    get VariationsError$(): import("rxjs").Observable<import("../core/IStateErrorSuccess").IStoreVariationsStateError>;
    get VariationsSuccess$(): import("rxjs").Observable<import("../core/IStateErrorSuccess").IStoreVariationsStateSuccess>;
    LoadImages: () => void;
    LoadImageById: (id: number) => void;
    CreateImage: (payload: any) => void;
    DeleteImage: (id: number) => void;
    get IsLoadingImages$(): import("rxjs").Observable<boolean>;
    get ImagesData$(): import("rxjs").Observable<import("../models/Image.model").ImageModel[]>;
    get ImagesHasBeenFetched$(): import("rxjs").Observable<boolean>;
    get ImagesError$(): import("rxjs").Observable<import("../core/IStateErrorSuccess").IStoreImagesStateError>;
    get ImagesSuccess$(): import("rxjs").Observable<import("../core/IStateErrorSuccess").IStoreImagesStateSuccess>;
}
