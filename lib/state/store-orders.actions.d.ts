import { OrderModel } from '../models/Order.model';
export declare enum StoreOrdersActionTypes {
    LoadOrdersBegin = "[STORE] Load Orders Begin",
    LoadOrdersSuccess = "[STORE] Load Orders Success",
    LoadOrdersFail = "[STORE] Load Orders Fail",
    LoadOrderByIdBegin = "[STORE] Load Order By Id Begin",
    LoadOrderByIdSuccess = "[STORE] Load Order By Id Success",
    LoadOrderByIdFail = "[STORE] Load Order By Id Fail",
    UpdateOrderBegin = "[STORE] Update Order Begin",
    UpdateOrderSuccess = "[STORE] Update Order Success",
    UpdateOrderFail = "[STORE] Update Order Fail",
    ClearOrdersSuccessErrorData = "[STORE] Clear Orders Success/Error Data"
}
export declare const LoadOrdersBeginAction: import("@ngrx/store").ActionCreator<StoreOrdersActionTypes.LoadOrdersBegin, () => import("@ngrx/store/src/models").TypedAction<StoreOrdersActionTypes.LoadOrdersBegin>>;
export declare const LoadOrdersSuccessAction: import("@ngrx/store").ActionCreator<StoreOrdersActionTypes.LoadOrdersSuccess, (props: {
    payload: OrderModel[];
}) => {
    payload: OrderModel[];
} & import("@ngrx/store/src/models").TypedAction<StoreOrdersActionTypes.LoadOrdersSuccess>>;
export declare const LoadOrdersFailAction: import("@ngrx/store").ActionCreator<StoreOrdersActionTypes.LoadOrdersFail, (props: {
    errors: any;
}) => {
    errors: any;
} & import("@ngrx/store/src/models").TypedAction<StoreOrdersActionTypes.LoadOrdersFail>>;
export declare const LoadOrderByIdBeginAction: import("@ngrx/store").ActionCreator<StoreOrdersActionTypes.LoadOrderByIdBegin, (props: {
    id: number;
}) => {
    id: number;
} & import("@ngrx/store/src/models").TypedAction<StoreOrdersActionTypes.LoadOrderByIdBegin>>;
export declare const LoadOrderByIdSuccessAction: import("@ngrx/store").ActionCreator<StoreOrdersActionTypes.LoadOrderByIdSuccess, (props: {
    payload: OrderModel;
}) => {
    payload: OrderModel;
} & import("@ngrx/store/src/models").TypedAction<StoreOrdersActionTypes.LoadOrderByIdSuccess>>;
export declare const LoadOrderByIdFailAction: import("@ngrx/store").ActionCreator<StoreOrdersActionTypes.LoadOrderByIdFail, (props: {
    errors: any;
}) => {
    errors: any;
} & import("@ngrx/store/src/models").TypedAction<StoreOrdersActionTypes.LoadOrderByIdFail>>;
export declare const UpdateOrderBeginAction: import("@ngrx/store").ActionCreator<StoreOrdersActionTypes.UpdateOrderBegin, (props: {
    id: number;
    payload: any;
}) => {
    id: number;
    payload: any;
} & import("@ngrx/store/src/models").TypedAction<StoreOrdersActionTypes.UpdateOrderBegin>>;
export declare const UpdateOrderSuccessAction: import("@ngrx/store").ActionCreator<StoreOrdersActionTypes.UpdateOrderSuccess, (props: {
    payload: OrderModel[];
}) => {
    payload: OrderModel[];
} & import("@ngrx/store/src/models").TypedAction<StoreOrdersActionTypes.UpdateOrderSuccess>>;
export declare const UpdateOrderFailAction: import("@ngrx/store").ActionCreator<StoreOrdersActionTypes.UpdateOrderFail, (props: {
    errors: any;
}) => {
    errors: any;
} & import("@ngrx/store/src/models").TypedAction<StoreOrdersActionTypes.UpdateOrderFail>>;
export declare const ClearOrdersSuccessErrorDataAction: import("@ngrx/store").ActionCreator<StoreOrdersActionTypes.ClearOrdersSuccessErrorData, () => import("@ngrx/store/src/models").TypedAction<StoreOrdersActionTypes.ClearOrdersSuccessErrorData>>;
