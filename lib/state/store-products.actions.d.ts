import { IUpsertProductPayload } from '../../public-api';
import { ProductModel } from '../models/Product.model';
export declare enum StoreProductsActionTypes {
    LoadProductsBegin = "[STORE] Load Products Begin",
    LoadProductsSuccess = "[STORE] Load Products Success",
    LoadProductsFail = "[STORE] Load Products Fail",
    LoadProductByIdBegin = "[STORE] Load Product By Id Begin",
    LoadProductByIdSuccess = "[STORE] Load Product By Id Success",
    LoadProductByIdFail = "[STORE] Load Product By Id Fail",
    CreateProductBegin = "[STORE] Create Product Begin",
    CreateProductSuccess = "[STORE] Create Product Success",
    CreateProductFail = "[STORE] Create Product Fail",
    UpdateProductBegin = "[STORE] Update Product Begin",
    UpdateProductSuccess = "[STORE] Update Product Success",
    UpdateProductFail = "[STORE] Update Product Fail",
    DeleteProductBegin = "[STORE] Delete Product Begin",
    DeleteProductSuccess = "[STORE] Delete Product Success",
    DeleteProductFail = "[STORE] Delete Product Fail",
    ClearProductsSuccessErrorData = "[STORE] Clear Products Success/Error Data"
}
export declare const LoadProductsBeginAction: import("@ngrx/store").ActionCreator<StoreProductsActionTypes.LoadProductsBegin, () => import("@ngrx/store/src/models").TypedAction<StoreProductsActionTypes.LoadProductsBegin>>;
export declare const LoadProductsSuccessAction: import("@ngrx/store").ActionCreator<StoreProductsActionTypes.LoadProductsSuccess, (props: {
    payload: ProductModel[];
}) => {
    payload: ProductModel[];
} & import("@ngrx/store/src/models").TypedAction<StoreProductsActionTypes.LoadProductsSuccess>>;
export declare const LoadProductsFailAction: import("@ngrx/store").ActionCreator<StoreProductsActionTypes.LoadProductsFail, (props: {
    errors: any;
}) => {
    errors: any;
} & import("@ngrx/store/src/models").TypedAction<StoreProductsActionTypes.LoadProductsFail>>;
export declare const LoadProductByIdBeginAction: import("@ngrx/store").ActionCreator<StoreProductsActionTypes.LoadProductByIdBegin, (props: {
    id: number;
}) => {
    id: number;
} & import("@ngrx/store/src/models").TypedAction<StoreProductsActionTypes.LoadProductByIdBegin>>;
export declare const LoadProductByIdSuccessAction: import("@ngrx/store").ActionCreator<StoreProductsActionTypes.LoadProductByIdSuccess, (props: {
    payload: ProductModel;
}) => {
    payload: ProductModel;
} & import("@ngrx/store/src/models").TypedAction<StoreProductsActionTypes.LoadProductByIdSuccess>>;
export declare const LoadProductByIdFailAction: import("@ngrx/store").ActionCreator<StoreProductsActionTypes.LoadProductByIdFail, (props: {
    errors: any;
}) => {
    errors: any;
} & import("@ngrx/store/src/models").TypedAction<StoreProductsActionTypes.LoadProductByIdFail>>;
export declare const CreateProductBeginAction: import("@ngrx/store").ActionCreator<StoreProductsActionTypes.CreateProductBegin, (props: {
    payload: IUpsertProductPayload;
}) => {
    payload: IUpsertProductPayload;
} & import("@ngrx/store/src/models").TypedAction<StoreProductsActionTypes.CreateProductBegin>>;
export declare const CreateProductSuccessAction: import("@ngrx/store").ActionCreator<StoreProductsActionTypes.CreateProductSuccess, (props: {
    payload: ProductModel[];
}) => {
    payload: ProductModel[];
} & import("@ngrx/store/src/models").TypedAction<StoreProductsActionTypes.CreateProductSuccess>>;
export declare const CreateProductFailAction: import("@ngrx/store").ActionCreator<StoreProductsActionTypes.CreateProductFail, (props: {
    errors: any;
}) => {
    errors: any;
} & import("@ngrx/store/src/models").TypedAction<StoreProductsActionTypes.CreateProductFail>>;
export declare const UpdateProductBeginAction: import("@ngrx/store").ActionCreator<StoreProductsActionTypes.UpdateProductBegin, (props: {
    id: number;
    payload: IUpsertProductPayload;
}) => {
    id: number;
    payload: IUpsertProductPayload;
} & import("@ngrx/store/src/models").TypedAction<StoreProductsActionTypes.UpdateProductBegin>>;
export declare const UpdateProductSuccessAction: import("@ngrx/store").ActionCreator<StoreProductsActionTypes.UpdateProductSuccess, (props: {
    payload: ProductModel[];
}) => {
    payload: ProductModel[];
} & import("@ngrx/store/src/models").TypedAction<StoreProductsActionTypes.UpdateProductSuccess>>;
export declare const UpdateProductFailAction: import("@ngrx/store").ActionCreator<StoreProductsActionTypes.UpdateProductFail, (props: {
    errors: any;
}) => {
    errors: any;
} & import("@ngrx/store/src/models").TypedAction<StoreProductsActionTypes.UpdateProductFail>>;
export declare const DeleteProductBeginAction: import("@ngrx/store").ActionCreator<StoreProductsActionTypes.DeleteProductBegin, (props: {
    id: number;
}) => {
    id: number;
} & import("@ngrx/store/src/models").TypedAction<StoreProductsActionTypes.DeleteProductBegin>>;
export declare const DeleteProductSuccessAction: import("@ngrx/store").ActionCreator<StoreProductsActionTypes.DeleteProductSuccess, (props: {
    payload: ProductModel[];
}) => {
    payload: ProductModel[];
} & import("@ngrx/store/src/models").TypedAction<StoreProductsActionTypes.DeleteProductSuccess>>;
export declare const DeleteProductFailAction: import("@ngrx/store").ActionCreator<StoreProductsActionTypes.DeleteProductFail, (props: {
    errors: any;
}) => {
    errors: any;
} & import("@ngrx/store/src/models").TypedAction<StoreProductsActionTypes.DeleteProductFail>>;
export declare const ClearProductsSuccessErrorDataAction: import("@ngrx/store").ActionCreator<StoreProductsActionTypes.ClearProductsSuccessErrorData, () => import("@ngrx/store/src/models").TypedAction<StoreProductsActionTypes.ClearProductsSuccessErrorData>>;
