import { ImageModel } from '../models/Image.model';
export declare enum StoreImagesActionTypes {
    LoadImagesBegin = "[STORE] Load Images Begin",
    LoadImagesSuccess = "[STORE] Load Images Success",
    LoadImagesFail = "[STORE] Load Images Fail",
    LoadImageByIdBegin = "[STORE] Load Image By Id Begin",
    LoadImageByIdSuccess = "[STORE] Load Image By Id Success",
    LoadImageByIdFail = "[STORE] Load Image By Id Fail",
    CreateImageBegin = "[STORE] Create Image Begin",
    CreateImageSuccess = "[STORE] Create Image Success",
    CreateImageFail = "[STORE] Create Image Fail",
    DelteImageBegin = "[STORE] Load Delete  Begin",
    DelteImageSuccess = "[STORE] Load Delete Success",
    DelteImageFail = "[STORE] Delete Image Fail",
    ClearImagesSuccessErrorData = "[STORE] Clear Images Success/Error Data"
}
export declare const LoadImagesBeginAction: import("@ngrx/store").ActionCreator<StoreImagesActionTypes.LoadImagesBegin, () => import("@ngrx/store/src/models").TypedAction<StoreImagesActionTypes.LoadImagesBegin>>;
export declare const LoadImagesSuccessAction: import("@ngrx/store").ActionCreator<StoreImagesActionTypes.LoadImagesSuccess, (props: {
    payload: ImageModel[];
}) => {
    payload: ImageModel[];
} & import("@ngrx/store/src/models").TypedAction<StoreImagesActionTypes.LoadImagesSuccess>>;
export declare const LoadImagesFailAction: import("@ngrx/store").ActionCreator<StoreImagesActionTypes.LoadImagesFail, (props: {
    errors: any;
}) => {
    errors: any;
} & import("@ngrx/store/src/models").TypedAction<StoreImagesActionTypes.LoadImagesFail>>;
export declare const LoadImageByIdBeginAction: import("@ngrx/store").ActionCreator<StoreImagesActionTypes.LoadImageByIdBegin, (props: {
    id: number;
}) => {
    id: number;
} & import("@ngrx/store/src/models").TypedAction<StoreImagesActionTypes.LoadImageByIdBegin>>;
export declare const LoadImageByIdSuccessAction: import("@ngrx/store").ActionCreator<StoreImagesActionTypes.LoadImageByIdSuccess, (props: {
    payload: ImageModel;
}) => {
    payload: ImageModel;
} & import("@ngrx/store/src/models").TypedAction<StoreImagesActionTypes.LoadImageByIdSuccess>>;
export declare const LoadImageByIdFailAction: import("@ngrx/store").ActionCreator<StoreImagesActionTypes.LoadImageByIdFail, (props: {
    errors: any;
}) => {
    errors: any;
} & import("@ngrx/store/src/models").TypedAction<StoreImagesActionTypes.LoadImageByIdFail>>;
export declare const CreateImageBeginAction: import("@ngrx/store").ActionCreator<StoreImagesActionTypes.CreateImageBegin, (props: {
    payload: any;
}) => {
    payload: any;
} & import("@ngrx/store/src/models").TypedAction<StoreImagesActionTypes.CreateImageBegin>>;
export declare const CreateImageSuccessAction: import("@ngrx/store").ActionCreator<StoreImagesActionTypes.CreateImageSuccess, (props: {
    payload: ImageModel[];
}) => {
    payload: ImageModel[];
} & import("@ngrx/store/src/models").TypedAction<StoreImagesActionTypes.CreateImageSuccess>>;
export declare const CreateImageFailAction: import("@ngrx/store").ActionCreator<StoreImagesActionTypes.CreateImageFail, (props: {
    errors: any;
}) => {
    errors: any;
} & import("@ngrx/store/src/models").TypedAction<StoreImagesActionTypes.CreateImageFail>>;
export declare const DeleteImageBeginAction: import("@ngrx/store").ActionCreator<StoreImagesActionTypes.DelteImageBegin, (props: {
    id: number;
}) => {
    id: number;
} & import("@ngrx/store/src/models").TypedAction<StoreImagesActionTypes.DelteImageBegin>>;
export declare const DeleteImageSuccessAction: import("@ngrx/store").ActionCreator<StoreImagesActionTypes.DelteImageSuccess, (props: {
    payload: ImageModel[];
}) => {
    payload: ImageModel[];
} & import("@ngrx/store/src/models").TypedAction<StoreImagesActionTypes.DelteImageSuccess>>;
export declare const DeleteImageFailAction: import("@ngrx/store").ActionCreator<StoreImagesActionTypes.DelteImageFail, (props: {
    errors: any;
}) => {
    errors: any;
} & import("@ngrx/store/src/models").TypedAction<StoreImagesActionTypes.DelteImageFail>>;
export declare const ClearImagesSuccessErrorDataAction: import("@ngrx/store").ActionCreator<StoreImagesActionTypes.ClearImagesSuccessErrorData, () => import("@ngrx/store/src/models").TypedAction<StoreImagesActionTypes.ClearImagesSuccessErrorData>>;
