import * as fromReducer from './store.reducer';
export declare const getStoreState: import("@ngrx/store").MemoizedSelector<object, fromReducer.IStoreState, import("@ngrx/store").DefaultProjectorFn<fromReducer.IStoreState>>;
export declare const getStorePageState: import("@ngrx/store").MemoizedSelector<object, fromReducer.IStoreState, import("@ngrx/store").DefaultProjectorFn<fromReducer.IStoreState>>;
export declare const getStoreCategoriesState: import("@ngrx/store").MemoizedSelector<object, {
    isLoading: boolean;
    data: import("../models/Category.model").CategoryModel[];
    selectedCategory: import("../models/Category.model").CategoryModel;
    hasBeenFetched: boolean;
    error: import("../core/IStateErrorSuccess").IStoreCategoriesStateError;
    success: import("../core/IStateErrorSuccess").IStoreCategoriesStateSuccess;
}, import("@ngrx/store").DefaultProjectorFn<{
    isLoading: boolean;
    data: import("../models/Category.model").CategoryModel[];
    selectedCategory: import("../models/Category.model").CategoryModel;
    hasBeenFetched: boolean;
    error: import("../core/IStateErrorSuccess").IStoreCategoriesStateError;
    success: import("../core/IStateErrorSuccess").IStoreCategoriesStateSuccess;
}>>;
export declare const getCategoriesIsloading: import("@ngrx/store").MemoizedSelector<object, boolean, import("@ngrx/store").DefaultProjectorFn<boolean>>;
export declare const getCategoriesData: import("@ngrx/store").MemoizedSelector<object, import("../models/Category.model").CategoryModel[], import("@ngrx/store").DefaultProjectorFn<import("../models/Category.model").CategoryModel[]>>;
export declare const getCategoriesHasBeenFetched: import("@ngrx/store").MemoizedSelector<object, boolean, import("@ngrx/store").DefaultProjectorFn<boolean>>;
export declare const getCategoriesError: import("@ngrx/store").MemoizedSelector<object, import("../core/IStateErrorSuccess").IStoreCategoriesStateError, import("@ngrx/store").DefaultProjectorFn<import("../core/IStateErrorSuccess").IStoreCategoriesStateError>>;
export declare const getCategoriesSuccess: import("@ngrx/store").MemoizedSelector<object, import("../core/IStateErrorSuccess").IStoreCategoriesStateSuccess, import("@ngrx/store").DefaultProjectorFn<import("../core/IStateErrorSuccess").IStoreCategoriesStateSuccess>>;
export declare const getStoreOrdersState: import("@ngrx/store").MemoizedSelector<object, {
    isLoading: boolean;
    data: import("../models/Order.model").OrderModel[];
    selectedOrder: import("../models/Order.model").OrderModel;
    hasBeenFetched: boolean;
    error: import("../core/IStateErrorSuccess").IStoreOrdersStateError;
    success: import("../core/IStateErrorSuccess").IStoreOrdersStateSuccess;
}, import("@ngrx/store").DefaultProjectorFn<{
    isLoading: boolean;
    data: import("../models/Order.model").OrderModel[];
    selectedOrder: import("../models/Order.model").OrderModel;
    hasBeenFetched: boolean;
    error: import("../core/IStateErrorSuccess").IStoreOrdersStateError;
    success: import("../core/IStateErrorSuccess").IStoreOrdersStateSuccess;
}>>;
export declare const getOrdersIsLoading: import("@ngrx/store").MemoizedSelector<object, boolean, import("@ngrx/store").DefaultProjectorFn<boolean>>;
export declare const getOrdersData: import("@ngrx/store").MemoizedSelector<object, import("../models/Order.model").OrderModel[], import("@ngrx/store").DefaultProjectorFn<import("../models/Order.model").OrderModel[]>>;
export declare const getOrdersHasBeenFetched: import("@ngrx/store").MemoizedSelector<object, boolean, import("@ngrx/store").DefaultProjectorFn<boolean>>;
export declare const getOrdersError: import("@ngrx/store").MemoizedSelector<object, import("../core/IStateErrorSuccess").IStoreOrdersStateError, import("@ngrx/store").DefaultProjectorFn<import("../core/IStateErrorSuccess").IStoreOrdersStateError>>;
export declare const getOrdersSuccess: import("@ngrx/store").MemoizedSelector<object, import("../core/IStateErrorSuccess").IStoreOrdersStateSuccess, import("@ngrx/store").DefaultProjectorFn<import("../core/IStateErrorSuccess").IStoreOrdersStateSuccess>>;
export declare const getStoreProductsState: import("@ngrx/store").MemoizedSelector<object, {
    isLoading: boolean;
    data: import("../models/Product.model").ProductModel[];
    selectedProduct: import("../models/Product.model").ProductModel;
    hasBeenFetched: boolean;
    error: import("../core/IStateErrorSuccess").IStoreProductsStateError;
    success: import("../core/IStateErrorSuccess").IStoreProductsStateSuccess;
}, import("@ngrx/store").DefaultProjectorFn<{
    isLoading: boolean;
    data: import("../models/Product.model").ProductModel[];
    selectedProduct: import("../models/Product.model").ProductModel;
    hasBeenFetched: boolean;
    error: import("../core/IStateErrorSuccess").IStoreProductsStateError;
    success: import("../core/IStateErrorSuccess").IStoreProductsStateSuccess;
}>>;
export declare const getStoreProductsIsLoading: import("@ngrx/store").MemoizedSelector<object, boolean, import("@ngrx/store").DefaultProjectorFn<boolean>>;
export declare const getStoreProductsData: import("@ngrx/store").MemoizedSelector<object, import("../models/Product.model").ProductModel[], import("@ngrx/store").DefaultProjectorFn<import("../models/Product.model").ProductModel[]>>;
export declare const getStoreProductsHasBeenFetched: import("@ngrx/store").MemoizedSelector<object, boolean, import("@ngrx/store").DefaultProjectorFn<boolean>>;
export declare const getStoreProductsError: import("@ngrx/store").MemoizedSelector<object, import("../core/IStateErrorSuccess").IStoreProductsStateError, import("@ngrx/store").DefaultProjectorFn<import("../core/IStateErrorSuccess").IStoreProductsStateError>>;
export declare const getStoreProductsSuccess: import("@ngrx/store").MemoizedSelector<object, import("../core/IStateErrorSuccess").IStoreProductsStateSuccess, import("@ngrx/store").DefaultProjectorFn<import("../core/IStateErrorSuccess").IStoreProductsStateSuccess>>;
export declare const getStoreVariationsState: import("@ngrx/store").MemoizedSelector<object, {
    isLoading: boolean;
    data: import("../models/Variation.model").VariationModel[];
    selectedVariation: import("../models/Variation.model").VariationModel;
    hasBeenFetched: boolean;
    error: import("../core/IStateErrorSuccess").IStoreVariationsStateError;
    success: import("../core/IStateErrorSuccess").IStoreVariationsStateSuccess;
}, import("@ngrx/store").DefaultProjectorFn<{
    isLoading: boolean;
    data: import("../models/Variation.model").VariationModel[];
    selectedVariation: import("../models/Variation.model").VariationModel;
    hasBeenFetched: boolean;
    error: import("../core/IStateErrorSuccess").IStoreVariationsStateError;
    success: import("../core/IStateErrorSuccess").IStoreVariationsStateSuccess;
}>>;
export declare const getStoreVariationsIsLoading: import("@ngrx/store").MemoizedSelector<object, boolean, import("@ngrx/store").DefaultProjectorFn<boolean>>;
export declare const getStoreVariationsData: import("@ngrx/store").MemoizedSelector<object, import("../models/Variation.model").VariationModel[], import("@ngrx/store").DefaultProjectorFn<import("../models/Variation.model").VariationModel[]>>;
export declare const getStoreVariationsHasBeenFetched: import("@ngrx/store").MemoizedSelector<object, boolean, import("@ngrx/store").DefaultProjectorFn<boolean>>;
export declare const getStoreVariationsError: import("@ngrx/store").MemoizedSelector<object, import("../core/IStateErrorSuccess").IStoreVariationsStateError, import("@ngrx/store").DefaultProjectorFn<import("../core/IStateErrorSuccess").IStoreVariationsStateError>>;
export declare const getStoreVariationsSuccess: import("@ngrx/store").MemoizedSelector<object, import("../core/IStateErrorSuccess").IStoreVariationsStateSuccess, import("@ngrx/store").DefaultProjectorFn<import("../core/IStateErrorSuccess").IStoreVariationsStateSuccess>>;
export declare const getStoreImagesState: import("@ngrx/store").MemoizedSelector<object, {
    isLoading: boolean;
    data: import("../models/Image.model").ImageModel[];
    selectedImage: import("../models/Image.model").ImageModel;
    hasBeenFetched: boolean;
    error: import("../core/IStateErrorSuccess").IStoreImagesStateError;
    success: import("../core/IStateErrorSuccess").IStoreImagesStateSuccess;
}, import("@ngrx/store").DefaultProjectorFn<{
    isLoading: boolean;
    data: import("../models/Image.model").ImageModel[];
    selectedImage: import("../models/Image.model").ImageModel;
    hasBeenFetched: boolean;
    error: import("../core/IStateErrorSuccess").IStoreImagesStateError;
    success: import("../core/IStateErrorSuccess").IStoreImagesStateSuccess;
}>>;
export declare const getStoreImagesIsLoading: import("@ngrx/store").MemoizedSelector<object, boolean, import("@ngrx/store").DefaultProjectorFn<boolean>>;
export declare const getStoreImagesData: import("@ngrx/store").MemoizedSelector<object, import("../models/Image.model").ImageModel[], import("@ngrx/store").DefaultProjectorFn<import("../models/Image.model").ImageModel[]>>;
export declare const getStoreImagesHasBeenFetched: import("@ngrx/store").MemoizedSelector<object, boolean, import("@ngrx/store").DefaultProjectorFn<boolean>>;
export declare const getStoreImagesError: import("@ngrx/store").MemoizedSelector<object, import("../core/IStateErrorSuccess").IStoreImagesStateError, import("@ngrx/store").DefaultProjectorFn<import("../core/IStateErrorSuccess").IStoreImagesStateError>>;
export declare const getStoreImagesSuccess: import("@ngrx/store").MemoizedSelector<object, import("../core/IStateErrorSuccess").IStoreImagesStateSuccess, import("@ngrx/store").DefaultProjectorFn<import("../core/IStateErrorSuccess").IStoreImagesStateSuccess>>;
