import { Actions } from '@ngrx/effects';
import { CategoryModel } from '../models/Category.model';
import { ImageModel } from '../models/Image.model';
import { OrderModel } from '../models/Order.model';
import { ProductModel } from '../models/Product.model';
import { VariationModel } from '../models/Variation.model';
import { IStoreService } from '../services/IStore.service';
import * as fromCategoriesActions from './store-categories.actions';
import * as fromImagesActions from './store-images.action';
import * as fromOrdersActions from './store-orders.actions';
import * as fromProductsActions from './store-products.actions';
import * as fromVariationsActions from './store-variations.actions';
import { StorePageStore } from './store.store';
export declare class StoreEffects {
    private actions;
    private storePageStore;
    private service;
    LoadCategories$: import("rxjs").Observable<({
        payload: CategoryModel[];
    } & import("@ngrx/store/src/models").TypedAction<fromCategoriesActions.StoreCategoriesActionTypes.LoadCategoriesSuccess>) | ({
        errors: any;
    } & import("@ngrx/store/src/models").TypedAction<fromCategoriesActions.StoreCategoriesActionTypes.LoadCategoriesFail>) | import("@ngrx/store/src/models").TypedAction<fromCategoriesActions.StoreCategoriesActionTypes.ClearCategoriesSuccessErrorData>> & import("@ngrx/effects").CreateEffectMetadata;
    LoadCategoryById$: import("rxjs").Observable<({
        payload: CategoryModel;
    } & import("@ngrx/store/src/models").TypedAction<fromCategoriesActions.StoreCategoriesActionTypes.LoadCategoryByIdSuccess>) | ({
        errors: any;
    } & import("@ngrx/store/src/models").TypedAction<fromCategoriesActions.StoreCategoriesActionTypes.LoadCategoryByIdFail>) | import("@ngrx/store/src/models").TypedAction<fromCategoriesActions.StoreCategoriesActionTypes.ClearCategoriesSuccessErrorData>> & import("@ngrx/effects").CreateEffectMetadata;
    CreateCategory$: import("rxjs").Observable<({
        payload: CategoryModel[];
    } & import("@ngrx/store/src/models").TypedAction<fromCategoriesActions.StoreCategoriesActionTypes.CreateCategorySuccess>) | ({
        errors: any;
    } & import("@ngrx/store/src/models").TypedAction<fromCategoriesActions.StoreCategoriesActionTypes.CreateCategoryFail>) | import("@ngrx/store/src/models").TypedAction<fromCategoriesActions.StoreCategoriesActionTypes.ClearCategoriesSuccessErrorData>> & import("@ngrx/effects").CreateEffectMetadata;
    DeleteCategory$: import("rxjs").Observable<({
        payload: CategoryModel[];
    } & import("@ngrx/store/src/models").TypedAction<fromCategoriesActions.StoreCategoriesActionTypes.DeleteCategorySuccess>) | ({
        errors: any;
    } & import("@ngrx/store/src/models").TypedAction<fromCategoriesActions.StoreCategoriesActionTypes.DeleteCategoryFail>) | import("@ngrx/store/src/models").TypedAction<fromCategoriesActions.StoreCategoriesActionTypes.ClearCategoriesSuccessErrorData>> & import("@ngrx/effects").CreateEffectMetadata;
    LoadOrders$: import("rxjs").Observable<({
        payload: OrderModel[];
    } & import("@ngrx/store/src/models").TypedAction<fromOrdersActions.StoreOrdersActionTypes.LoadOrdersSuccess>) | ({
        errors: any;
    } & import("@ngrx/store/src/models").TypedAction<fromOrdersActions.StoreOrdersActionTypes.LoadOrdersFail>) | import("@ngrx/store/src/models").TypedAction<fromOrdersActions.StoreOrdersActionTypes.ClearOrdersSuccessErrorData>> & import("@ngrx/effects").CreateEffectMetadata;
    LoadOrderById$: import("rxjs").Observable<({
        payload: OrderModel;
    } & import("@ngrx/store/src/models").TypedAction<fromOrdersActions.StoreOrdersActionTypes.LoadOrderByIdSuccess>) | import("@ngrx/store/src/models").TypedAction<fromOrdersActions.StoreOrdersActionTypes.ClearOrdersSuccessErrorData> | ({
        errors: any;
    } & import("@ngrx/store/src/models").TypedAction<fromOrdersActions.StoreOrdersActionTypes.LoadOrderByIdFail>)> & import("@ngrx/effects").CreateEffectMetadata;
    UpdateOrderById$: import("rxjs").Observable<({
        payload: OrderModel[];
    } & import("@ngrx/store/src/models").TypedAction<fromOrdersActions.StoreOrdersActionTypes.UpdateOrderSuccess>) | ({
        errors: any;
    } & import("@ngrx/store/src/models").TypedAction<fromOrdersActions.StoreOrdersActionTypes.UpdateOrderFail>) | import("@ngrx/store/src/models").TypedAction<fromOrdersActions.StoreOrdersActionTypes.ClearOrdersSuccessErrorData>> & import("@ngrx/effects").CreateEffectMetadata;
    LoadProducts$: import("rxjs").Observable<({
        payload: ProductModel[];
    } & import("@ngrx/store/src/models").TypedAction<fromProductsActions.StoreProductsActionTypes.LoadProductsSuccess>) | ({
        errors: any;
    } & import("@ngrx/store/src/models").TypedAction<fromProductsActions.StoreProductsActionTypes.LoadProductsFail>) | import("@ngrx/store/src/models").TypedAction<fromProductsActions.StoreProductsActionTypes.ClearProductsSuccessErrorData>> & import("@ngrx/effects").CreateEffectMetadata;
    LoadProductById$: import("rxjs").Observable<({
        payload: ProductModel;
    } & import("@ngrx/store/src/models").TypedAction<fromProductsActions.StoreProductsActionTypes.LoadProductByIdSuccess>) | ({
        errors: any;
    } & import("@ngrx/store/src/models").TypedAction<fromProductsActions.StoreProductsActionTypes.LoadProductByIdFail>) | import("@ngrx/store/src/models").TypedAction<fromProductsActions.StoreProductsActionTypes.ClearProductsSuccessErrorData>> & import("@ngrx/effects").CreateEffectMetadata;
    CreateProduct$: import("rxjs").Observable<({
        payload: ProductModel[];
    } & import("@ngrx/store/src/models").TypedAction<fromProductsActions.StoreProductsActionTypes.CreateProductSuccess>) | ({
        errors: any;
    } & import("@ngrx/store/src/models").TypedAction<fromProductsActions.StoreProductsActionTypes.CreateProductFail>) | import("@ngrx/store/src/models").TypedAction<fromProductsActions.StoreProductsActionTypes.ClearProductsSuccessErrorData>> & import("@ngrx/effects").CreateEffectMetadata;
    UpdateProduct$: import("rxjs").Observable<({
        payload: ProductModel[];
    } & import("@ngrx/store/src/models").TypedAction<fromProductsActions.StoreProductsActionTypes.UpdateProductSuccess>) | ({
        errors: any;
    } & import("@ngrx/store/src/models").TypedAction<fromProductsActions.StoreProductsActionTypes.UpdateProductFail>) | import("@ngrx/store/src/models").TypedAction<fromProductsActions.StoreProductsActionTypes.ClearProductsSuccessErrorData>> & import("@ngrx/effects").CreateEffectMetadata;
    DeleteProduct$: import("rxjs").Observable<({
        payload: ProductModel[];
    } & import("@ngrx/store/src/models").TypedAction<fromProductsActions.StoreProductsActionTypes.DeleteProductSuccess>) | ({
        errors: any;
    } & import("@ngrx/store/src/models").TypedAction<fromProductsActions.StoreProductsActionTypes.DeleteProductFail>) | import("@ngrx/store/src/models").TypedAction<fromProductsActions.StoreProductsActionTypes.ClearProductsSuccessErrorData>> & import("@ngrx/effects").CreateEffectMetadata;
    LoadVariations$: import("rxjs").Observable<({
        payload: VariationModel[];
    } & import("@ngrx/store/src/models").TypedAction<fromVariationsActions.StoreVariationsActionTypes.LoadVariationsSuccess>) | ({
        errors: any;
    } & import("@ngrx/store/src/models").TypedAction<fromVariationsActions.StoreVariationsActionTypes.LoadVariationsFail>) | import("@ngrx/store/src/models").TypedAction<fromVariationsActions.StoreVariationsActionTypes.ClearVariationsSuccessErrorData>> & import("@ngrx/effects").CreateEffectMetadata;
    LoadVariationById$: import("rxjs").Observable<({
        payload: VariationModel;
    } & import("@ngrx/store/src/models").TypedAction<fromVariationsActions.StoreVariationsActionTypes.LoadVariationByIdSuccess>) | ({
        errors: any;
    } & import("@ngrx/store/src/models").TypedAction<fromVariationsActions.StoreVariationsActionTypes.LoadVariationByIdFail>) | import("@ngrx/store/src/models").TypedAction<fromVariationsActions.StoreVariationsActionTypes.ClearVariationsSuccessErrorData>> & import("@ngrx/effects").CreateEffectMetadata;
    CreateVariation$: import("rxjs").Observable<({
        payload: VariationModel[];
    } & import("@ngrx/store/src/models").TypedAction<fromVariationsActions.StoreVariationsActionTypes.CreateVariationSuccess>) | ({
        errors: any;
    } & import("@ngrx/store/src/models").TypedAction<fromVariationsActions.StoreVariationsActionTypes.CreateVariationFail>) | import("@ngrx/store/src/models").TypedAction<fromVariationsActions.StoreVariationsActionTypes.ClearVariationsSuccessErrorData>> & import("@ngrx/effects").CreateEffectMetadata;
    DeleteVariation$: import("rxjs").Observable<({
        payload: VariationModel[];
    } & import("@ngrx/store/src/models").TypedAction<fromVariationsActions.StoreVariationsActionTypes.DeleteVariationSuccess>) | ({
        errors: any;
    } & import("@ngrx/store/src/models").TypedAction<fromVariationsActions.StoreVariationsActionTypes.DeleteVariationFail>) | import("@ngrx/store/src/models").TypedAction<fromVariationsActions.StoreVariationsActionTypes.ClearVariationsSuccessErrorData>> & import("@ngrx/effects").CreateEffectMetadata;
    LoadImages$: import("rxjs").Observable<({
        payload: ImageModel[];
    } & import("@ngrx/store/src/models").TypedAction<fromImagesActions.StoreImagesActionTypes.LoadImagesSuccess>) | ({
        errors: any;
    } & import("@ngrx/store/src/models").TypedAction<fromImagesActions.StoreImagesActionTypes.LoadImagesFail>) | import("@ngrx/store/src/models").TypedAction<fromImagesActions.StoreImagesActionTypes.ClearImagesSuccessErrorData>> & import("@ngrx/effects").CreateEffectMetadata;
    LoadImageById$: import("rxjs").Observable<({
        payload: ImageModel;
    } & import("@ngrx/store/src/models").TypedAction<fromImagesActions.StoreImagesActionTypes.LoadImageByIdSuccess>) | ({
        errors: any;
    } & import("@ngrx/store/src/models").TypedAction<fromImagesActions.StoreImagesActionTypes.LoadImageByIdFail>) | import("@ngrx/store/src/models").TypedAction<fromImagesActions.StoreImagesActionTypes.ClearImagesSuccessErrorData>> & import("@ngrx/effects").CreateEffectMetadata;
    CreateImage$: import("rxjs").Observable<({
        payload: ImageModel[];
    } & import("@ngrx/store/src/models").TypedAction<fromImagesActions.StoreImagesActionTypes.CreateImageSuccess>) | ({
        errors: any;
    } & import("@ngrx/store/src/models").TypedAction<fromImagesActions.StoreImagesActionTypes.CreateImageFail>) | import("@ngrx/store/src/models").TypedAction<fromImagesActions.StoreImagesActionTypes.ClearImagesSuccessErrorData>> & import("@ngrx/effects").CreateEffectMetadata;
    DeleteImage$: import("rxjs").Observable<({
        payload: ImageModel[];
    } & import("@ngrx/store/src/models").TypedAction<fromImagesActions.StoreImagesActionTypes.DelteImageSuccess>) | ({
        errors: any;
    } & import("@ngrx/store/src/models").TypedAction<fromImagesActions.StoreImagesActionTypes.DelteImageFail>) | import("@ngrx/store/src/models").TypedAction<fromImagesActions.StoreImagesActionTypes.ClearImagesSuccessErrorData>> & import("@ngrx/effects").CreateEffectMetadata;
    constructor(actions: Actions, storePageStore: StorePageStore, service: IStoreService);
}
