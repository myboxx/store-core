import { VariationModel } from '../models/Variation.model';
import { ICreateVariationPayload } from '../repositories/IStore.api';
export declare enum StoreVariationsActionTypes {
    LoadVariationsBegin = "[STORE] Load Variations Begin",
    LoadVariationsSuccess = "[STORE] Load Variations Success",
    LoadVariationsFail = "[STORE] Load Variations Fail",
    LoadVariationByIdBegin = "[STORE] Load Variation By Id Begin",
    LoadVariationByIdSuccess = "[STORE] Load Variation By Id Success",
    LoadVariationByIdFail = "[STORE] Load Variation By Id Fail",
    CreateVariationBegin = "[STORE] Create Variation Begin",
    CreateVariationSuccess = "[STORE] Create Variation Success",
    CreateVariationFail = "[STORE] Create Variation Fail",
    DeleteVariationBegin = "[STORE] Delete Variation Begin",
    DeleteVariationSuccess = "[STORE] Delete Variation Success",
    DeleteVariationFail = "[STORE] Delete Variation Fail",
    ClearVariationsSuccessErrorData = "[STORE] Clear Variations Success/Error Data"
}
export declare const LoadVariationsBeginAction: import("@ngrx/store").ActionCreator<StoreVariationsActionTypes.LoadVariationsBegin, () => import("@ngrx/store/src/models").TypedAction<StoreVariationsActionTypes.LoadVariationsBegin>>;
export declare const LoadVariationsSuccessAction: import("@ngrx/store").ActionCreator<StoreVariationsActionTypes.LoadVariationsSuccess, (props: {
    payload: VariationModel[];
}) => {
    payload: VariationModel[];
} & import("@ngrx/store/src/models").TypedAction<StoreVariationsActionTypes.LoadVariationsSuccess>>;
export declare const LoadVariationsFailAction: import("@ngrx/store").ActionCreator<StoreVariationsActionTypes.LoadVariationsFail, (props: {
    errors: any;
}) => {
    errors: any;
} & import("@ngrx/store/src/models").TypedAction<StoreVariationsActionTypes.LoadVariationsFail>>;
export declare const LoadVariationByIdBeginAction: import("@ngrx/store").ActionCreator<StoreVariationsActionTypes.LoadVariationByIdBegin, (props: {
    id: number;
}) => {
    id: number;
} & import("@ngrx/store/src/models").TypedAction<StoreVariationsActionTypes.LoadVariationByIdBegin>>;
export declare const LoadVariationByIdSuccessAction: import("@ngrx/store").ActionCreator<StoreVariationsActionTypes.LoadVariationByIdSuccess, (props: {
    payload: VariationModel;
}) => {
    payload: VariationModel;
} & import("@ngrx/store/src/models").TypedAction<StoreVariationsActionTypes.LoadVariationByIdSuccess>>;
export declare const LoadVariationByIdFailAction: import("@ngrx/store").ActionCreator<StoreVariationsActionTypes.LoadVariationByIdFail, (props: {
    errors: any;
}) => {
    errors: any;
} & import("@ngrx/store/src/models").TypedAction<StoreVariationsActionTypes.LoadVariationByIdFail>>;
export declare const CreteVariationBeginAction: import("@ngrx/store").ActionCreator<StoreVariationsActionTypes.CreateVariationBegin, (props: {
    payload: ICreateVariationPayload;
}) => {
    payload: ICreateVariationPayload;
} & import("@ngrx/store/src/models").TypedAction<StoreVariationsActionTypes.CreateVariationBegin>>;
export declare const CreteVariationSuccessAction: import("@ngrx/store").ActionCreator<StoreVariationsActionTypes.CreateVariationSuccess, (props: {
    payload: VariationModel[];
}) => {
    payload: VariationModel[];
} & import("@ngrx/store/src/models").TypedAction<StoreVariationsActionTypes.CreateVariationSuccess>>;
export declare const CreteVariataionFailAction: import("@ngrx/store").ActionCreator<StoreVariationsActionTypes.CreateVariationFail, (props: {
    errors: any;
}) => {
    errors: any;
} & import("@ngrx/store/src/models").TypedAction<StoreVariationsActionTypes.CreateVariationFail>>;
export declare const DelteVariationBeginAction: import("@ngrx/store").ActionCreator<StoreVariationsActionTypes.DeleteVariationBegin, (props: {
    payload: number;
}) => {
    payload: number;
} & import("@ngrx/store/src/models").TypedAction<StoreVariationsActionTypes.DeleteVariationBegin>>;
export declare const DelteVariationSuccessAction: import("@ngrx/store").ActionCreator<StoreVariationsActionTypes.DeleteVariationSuccess, (props: {
    payload: VariationModel[];
}) => {
    payload: VariationModel[];
} & import("@ngrx/store/src/models").TypedAction<StoreVariationsActionTypes.DeleteVariationSuccess>>;
export declare const DelteVariationFailAction: import("@ngrx/store").ActionCreator<StoreVariationsActionTypes.DeleteVariationFail, (props: {
    errors: any;
}) => {
    errors: any;
} & import("@ngrx/store/src/models").TypedAction<StoreVariationsActionTypes.DeleteVariationFail>>;
export declare const ClearVariationsSuccessErrorDataAction: import("@ngrx/store").ActionCreator<StoreVariationsActionTypes.ClearVariationsSuccessErrorData, () => import("@ngrx/store/src/models").TypedAction<StoreVariationsActionTypes.ClearVariationsSuccessErrorData>>;
