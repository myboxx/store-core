import { Action } from '@ngrx/store';
import { IStoreCategoriesStateSuccess, IStoreCategoriesStateError, IStoreOrdersStateSuccess, IStoreOrdersStateError, IStoreProductsStateError, IStoreProductsStateSuccess, IStoreVariationsStateError, IStoreVariationsStateSuccess, IStoreImagesStateError, IStoreImagesStateSuccess } from '../core/IStateErrorSuccess';
import { CategoryModel } from '../models/Category.model';
import { ImageModel } from '../models/Image.model';
import { OrderModel } from '../models/Order.model';
import { ProductModel } from '../models/Product.model';
import { VariationModel } from '../models/Variation.model';
export interface IStoreState {
    categories: {
        isLoading: boolean;
        data: CategoryModel[];
        selectedCategory: CategoryModel;
        hasBeenFetched: boolean;
        error: IStoreCategoriesStateError;
        success: IStoreCategoriesStateSuccess;
    };
    orders: {
        isLoading: boolean;
        data: OrderModel[];
        selectedOrder: OrderModel;
        hasBeenFetched: boolean;
        error: IStoreOrdersStateError;
        success: IStoreOrdersStateSuccess;
    };
    products: {
        isLoading: boolean;
        data: ProductModel[];
        selectedProduct: ProductModel;
        hasBeenFetched: boolean;
        error: IStoreProductsStateError;
        success: IStoreProductsStateSuccess;
    };
    variations: {
        isLoading: boolean;
        data: VariationModel[];
        selectedVariation: VariationModel;
        hasBeenFetched: boolean;
        error: IStoreVariationsStateError;
        success: IStoreVariationsStateSuccess;
    };
    images: {
        isLoading: boolean;
        data: ImageModel[];
        selectedImage: ImageModel;
        hasBeenFetched: boolean;
        error: IStoreImagesStateError;
        success: IStoreImagesStateSuccess;
    };
}
export declare const initialState: IStoreState;
export declare function storeReducer(state: IStoreState | undefined, action: Action): IStoreState;
export interface StoreState {
    store: IStoreState;
}
