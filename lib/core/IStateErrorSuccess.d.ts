import { IStateErrorBase, IStateSuccessBase } from '@boxx/core';
export declare type StoreCategoriesStateType = 'LOAD_CATEGORIES' | 'CREATE_CATEGORY' | 'UPDATE_CATEGORY' | 'DELETE_CATEGORY' | 'UNKNOWN';
export interface IStoreCategoriesStateError extends IStateErrorBase {
    after: StoreCategoriesStateType;
}
export interface IStoreCategoriesStateSuccess extends IStateSuccessBase {
    after: StoreCategoriesStateType;
}
export declare type StoreOrdersStateType = 'LOAD_ORDERS' | 'UPDATE_ORDER' | 'UNKNOWN';
export interface IStoreOrdersStateError extends IStateErrorBase {
    after: StoreOrdersStateType;
}
export interface IStoreOrdersStateSuccess extends IStateSuccessBase {
    after: StoreOrdersStateType;
}
export declare type StoreProductsStateType = 'LOAD_PRODUCTS' | 'CREATE_PRODUCT' | 'UPDATE_PRODUCT' | 'DELETE_PRODUCT' | 'UNKNOWN';
export interface IStoreProductsStateError extends IStateErrorBase {
    after: StoreProductsStateType;
}
export interface IStoreProductsStateSuccess extends IStateSuccessBase {
    after: StoreProductsStateType;
}
export declare type StoreVariationsStateType = 'LOAD_VARIATIONS' | 'CREATE_VARIATION' | 'DELETE_VARIATION' | 'UNKNOWN';
export interface IStoreVariationsStateError extends IStateErrorBase {
    after: StoreVariationsStateType;
}
export interface IStoreVariationsStateSuccess extends IStateSuccessBase {
    after: StoreVariationsStateType;
}
export declare type StoreImagesStateType = 'LOAD_IMAGES' | 'CREATE_IMAGE' | 'DELETE_IMAGE' | 'UNKNOWN';
export interface IStoreImagesStateError extends IStateErrorBase {
    after: StoreImagesStateType;
}
export interface IStoreImagesStateSuccess extends IStateSuccessBase {
    after: StoreImagesStateType;
}
