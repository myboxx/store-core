import { ModuleWithProviders, Provider } from '@angular/core';
interface ModuleOptionsInterface {
    providers: Provider[];
}
export declare class StoreCoreModule {
    static forRoot(config: ModuleOptionsInterface): ModuleWithProviders<StoreCoreModule>;
}
export {};
