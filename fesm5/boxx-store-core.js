import { InjectionToken, Inject, Injectable, ɵɵdefineInjectable, ɵɵinject, NgModule } from '@angular/core';
import { __decorate, __param, __read, __spread, __assign } from 'tslib';
import { HttpParams, HttpClient, HttpClientModule } from '@angular/common/http';
import { AbstractAppConfigService, APP_CONFIG_SERVICE } from '@boxx/core';
import { map, catchError, switchMap, flatMap, withLatestFrom } from 'rxjs/operators';
import { createAction, props, createFeatureSelector, createSelector, Store, createReducer, on, StoreModule } from '@ngrx/store';
import { createEffect, ofType, Actions, EffectsModule } from '@ngrx/effects';
import { of } from 'rxjs';

var CategoryModel = /** @class */ (function () {
    function CategoryModel(data) {
        this.active = data.active;
        this.categoryImages = data.categoryImages;
        this.description = data.description;
        this.id = data.id;
        this.name = data.name;
        this.parentId = data.parentId;
        this.resourceUri = data.resourceUri;
        this.shopId = data.shopId;
        this.slug = data.slug;
    }
    CategoryModel.fromApi = function (data) {
        var categoryImages = [];
        if (Array.isArray(data.category_images)) {
            categoryImages = data.category_images;
        }
        else if (typeof data.category_images === 'object') {
            Object.keys(data.category_images).forEach(function (key) {
                categoryImages.push(data.category_images[key].url);
            });
        }
        return new CategoryModel({
            active: data.active,
            categoryImages: categoryImages,
            description: data.description,
            id: data.id,
            name: data.name,
            parentId: data.parent_id,
            resourceUri: data.resource_uri,
            shopId: data.shop_id,
            slug: data.slug
        });
    };
    CategoryModel.empty = function () {
        return new CategoryModel({
            active: null,
            categoryImages: [],
            description: null,
            id: null,
            name: null,
            parentId: null,
            resourceUri: null,
            shopId: null,
            slug: null,
        });
    };
    return CategoryModel;
}());

var ImageModel = /** @class */ (function () {
    function ImageModel(data) {
        this.absolutePath = data.absolutePath;
        this.added = data.added;
        this.alt = data.alt;
        this.baseUrl = data.baseUrl;
        this.checksum = data.checksum;
        this.filesize = data.filesize;
        this.height = data.height;
        this.id = data.id;
        this.image = data.image;
        this.isGlobal = data.isGlobal;
        this.lastIncrement = data.lastIncrement;
        this.mimetype = data.mimetype;
        this.modified = data.modified;
        this.resourceUri = data.resourceUri;
        this.siteId = data.siteId;
        this.sizes = data.sizes;
        this.type = data.type;
        this.userId = data.userId;
        this.versions = data.versions;
        this.whitelabelId = data.whitelabelId;
        this.width = data.width;
    }
    ImageModel.fromApi = function (data) {
        var versions = data.versions.map(function (v) { return ({
            absolutePath: v.absolute_path,
            filesize: v.filesize,
            height: v.height,
            id: v.id,
            image: v.image,
            imageId: v.image_id,
            isGlobal: v.is_global,
            mimetype: v.mimetype,
            resourceUri: v.resource_uri,
            type: v.type,
            width: v.width,
        }); });
        return new ImageModel({
            absolutePath: data.absolute_path,
            added: data.added,
            alt: data.alt,
            baseUrl: data.base_url,
            checksum: data.checksum,
            filesize: data.filesize,
            height: data.height,
            id: data.id,
            image: data.image,
            isGlobal: data.is_global,
            lastIncrement: data.last_increment,
            mimetype: data.mimetype,
            modified: data.modified,
            resourceUri: data.resource_uri,
            siteId: data.site_id,
            sizes: data.sizes,
            type: data.type,
            userId: data.user_id,
            versions: versions,
            whitelabelId: data.whitelabel_id,
            width: data.width,
        });
    };
    ImageModel.empty = function () {
        return new ImageModel({
            absolutePath: null,
            added: null,
            alt: null,
            baseUrl: null,
            checksum: null,
            filesize: null,
            height: null,
            id: null,
            image: null,
            isGlobal: null,
            lastIncrement: null,
            mimetype: null,
            modified: null,
            resourceUri: null,
            siteId: null,
            sizes: null,
            type: null,
            userId: null,
            versions: null,
            whitelabelId: null,
            width: null,
        });
    };
    return ImageModel;
}());

var OrderModel = /** @class */ (function () {
    function OrderModel(data) {
        this.buyerEmail = data.buyerEmail;
        this.buyerName = data.buyerName;
        this.id = data.id;
        this.price = data.price;
        this.resourceUri = data.resourceUri;
        this.sessionId = data.sessionId;
        this.shipToAddress = data.shipToAddress;
        this.shipToCity = data.shipToCity;
        this.shipToCountry = data.shipToCountry;
        this.shipToPostalCode = data.shipToPostalCode;
        this.shipToState = data.shipToState;
        this.shippingCost = data.shippingCost;
        this.shippingId = data.shippingId;
        this.shippingName = data.shippingName;
        this.siteId = data.siteId;
        this.status = data.status;
        this.timestamp = data.timestamp;
        this.transactionId = data.transactionId;
        this.whitelabelId = data.whitelabelId;
    }
    OrderModel.fromApi = function (data) {
        return new OrderModel({
            buyerEmail: data.buyer_email,
            buyerName: data.buyer_name,
            id: data.id,
            price: data.price,
            resourceUri: data.resource_uri,
            sessionId: data.session_id,
            shipToAddress: data.ship_to_address,
            shipToCity: data.ship_to_city,
            shipToCountry: data.ship_to_country,
            shipToPostalCode: data.ship_to_postal_code,
            shipToState: data.ship_to_state,
            shippingCost: data.shipping_cost,
            shippingId: data.shipping_id,
            shippingName: data.shipping_name,
            siteId: data.site_id,
            status: data.status,
            timestamp: data.timestamp,
            transactionId: data.transaction_id,
            whitelabelId: data.whitelabel_id
        });
    };
    OrderModel.empty = function () {
        return new OrderModel({
            buyerEmail: null,
            buyerName: null,
            id: null,
            price: null,
            resourceUri: null,
            sessionId: null,
            shipToAddress: null,
            shipToCity: null,
            shipToCountry: null,
            shipToPostalCode: null,
            shipToState: null,
            shippingCost: null,
            shippingId: null,
            shippingName: null,
            siteId: null,
            status: null,
            timestamp: null,
            transactionId: null,
            whitelabelId: null
        });
    };
    return OrderModel;
}());

var ProductModel = /** @class */ (function () {
    function ProductModel(data) {
        this.active = data.active;
        this.added = data.added;
        this.asin = data.asin;
        this.autoStock = data.autoStock;
        this.brand = data.brand;
        this.categories = data.categories;
        this.description = data.description;
        this.excerpt = data.excerpt;
        this.height = data.height;
        this.id = data.id;
        this.image = data.image;
        this.imageId = data.imageId;
        this.length = data.length;
        this.msrpPrice = data.msrpPrice;
        this.outgoingUrl = data.outgoingUrl;
        this.price = data.price;
        this.primaryImageUrl = data.primaryImageUrl;
        this.productImages = data.productImages;
        this.published = data.published;
        this.quantity = data.quantity;
        this.resourceUri = data.resourceUri;
        this.salePrice = data.salePrice;
        this.shopId = data.shopId;
        this.sku = data.sku;
        this.slug = data.slug;
        this.status = data.status;
        this.title = data.title;
        this.upc = data.upc;
        this.variations = data.variations;
        this.vendor = data.vendor;
        this.weight = data.weight;
        this.weightUnit = data.weightUnit;
        this.wholesalePrice = data.wholesalePrice;
        this.width = data.width;
    }
    ProductModel.fromApi = function (data) {
        var categories = data.categories.map(function (c) { return CategoryModel.fromApi(c); });
        var productImages;
        if (data.product_images) {
            if (!Array.isArray(data.product_images)) {
                productImages = data.product_images;
            }
            else {
                data.product_images.forEach(function (img, idx) { return productImages[idx] = img; });
            }
        }
        var variations = data.variations.map(function (v) { return ProductVariationModel.fromApi(v); });
        return new ProductModel({
            active: data.active,
            added: data.added,
            asin: data.asin,
            autoStock: data.auto_stock,
            brand: data.brand,
            categories: data.categories.map(function (c) { return CategoryModel.fromApi(c); }),
            description: data.description,
            excerpt: data.excerpt,
            height: data.height,
            id: data.id,
            image: data.image ? { id: data.image.id, url: data.image.base_url + '/' + data.image.absolute_path } : null,
            imageId: data.image_id,
            length: data.length,
            msrpPrice: data.msrp_price,
            outgoingUrl: data.outgoing_url,
            price: data.price,
            primaryImageUrl: data.primary_image_url,
            productImages: productImages,
            published: data.published,
            quantity: data.quantity,
            resourceUri: data.resource_uri,
            salePrice: data.sale_price,
            shopId: data.shop_id,
            sku: data.sku,
            slug: data.slug,
            status: data.status,
            title: data.title,
            upc: data.upc,
            variations: variations,
            vendor: data.vendor,
            weight: data.weight,
            weightUnit: data.weight_unit,
            wholesalePrice: data.wholesale_price,
            width: data.width,
        });
    };
    ProductModel.empty = function () {
        return new ProductModel({
            active: null,
            added: null,
            asin: null,
            autoStock: null,
            brand: null,
            categories: null,
            description: null,
            excerpt: null,
            height: null,
            id: null,
            image: null,
            imageId: null,
            length: null,
            msrpPrice: null,
            outgoingUrl: null,
            price: null,
            primaryImageUrl: null,
            productImages: null,
            published: null,
            quantity: null,
            resourceUri: null,
            salePrice: null,
            shopId: null,
            sku: null,
            slug: null,
            status: null,
            title: null,
            upc: null,
            variations: null,
            vendor: null,
            weight: null,
            weightUnit: null,
            wholesalePrice: null,
            width: null,
        });
    };
    return ProductModel;
}());
var ProductVariationModel = /** @class */ (function () {
    function ProductVariationModel(data) {
        this.id = data.id;
        this.name = data.name;
        this.options = data.options;
        this.required = data.required;
        this.resourceUri = data.resourceUri;
        this.shop = data.shop;
        this.shopId = data.shopId;
    }
    ProductVariationModel.fromApi = function (data) {
        var options = data.options.map(function (o) { return ({
            id: o.id,
            name: o.name,
            order: o.order,
            price: o.price,
            variationId: o.variation_id
        }); });
        return new ProductVariationModel({
            id: data.id,
            name: data.name,
            options: options,
            required: data.required,
            resourceUri: data.resource_uri,
            shop: data.shop,
            shopId: data.shop_id
        });
    };
    ProductVariationModel.empty = function () {
        return new ProductVariationModel({
            id: null,
            name: null,
            options: null,
            required: null,
            resourceUri: null,
            shop: null,
            shopId: null
        });
    };
    return ProductVariationModel;
}());

var VariationModel = /** @class */ (function () {
    function VariationModel(data) {
        this.id = data.id;
        this.name = data.name;
        this.options = data.options;
        this.required = data.required;
        this.shop = data.shop;
        this.shopId = data.shopId;
    }
    VariationModel.fromApi = function (data) {
        var options = data.options.map(function (o) { return ({
            id: o.id,
            name: o.name,
            order: o.order,
            price: o.price,
            variationId: o.variation_id
        }); });
        return new VariationModel({
            id: data.id,
            name: data.name,
            options: options,
            required: data.required,
            resourceUri: data.resource_uri,
            shop: data.shop,
            shopId: data.shop_id,
        });
    };
    VariationModel.empty = function () {
        return new VariationModel({
            id: null,
            name: null,
            options: null,
            required: null,
            resourceUri: null,
            shop: null,
            shopId: null,
        });
    };
    return VariationModel;
}());

var STORE_REPOSITORY = new InjectionToken('StoreRepository');

var StoreRepository = /** @class */ (function () {
    function StoreRepository(appSettings, httpClient) {
        this.appSettings = appSettings;
        this.httpClient = httpClient;
    }
    // --------------------- CATEGORIES: -------------------------
    StoreRepository.prototype.loadCategories = function (pagination) {
        if (pagination === void 0) { pagination = ''; }
        return this.httpClient.get(this.getBaseUrl() + "/categories" + pagination);
    };
    StoreRepository.prototype.loadCategoryById = function (id) {
        return this.httpClient.get(this.getBaseUrl() + "/categories/" + id);
    };
    StoreRepository.prototype.createCategory = function (payload) {
        var body = this.createHttParams(payload);
        return this.httpClient.post(this.getBaseUrl() + "/create_category", body);
    };
    StoreRepository.prototype.deleteCategory = function (id) {
        return this.httpClient.delete(this.getBaseUrl() + "/delete_category/" + id);
    };
    // --------------------- ORDERS: -------------------------
    StoreRepository.prototype.loadOrders = function (pagination) {
        if (pagination === void 0) { pagination = ''; }
        return this.httpClient.get(this.getBaseUrl() + "/orders" + pagination);
    };
    StoreRepository.prototype.loadOrderById = function (id) {
        return this.httpClient.get(this.getBaseUrl() + "/orders/" + id);
    };
    StoreRepository.prototype.updateOrder = function (id, payload) {
        var body = this.createHttParams(payload);
        return this.httpClient.post(this.getBaseUrl() + "/update_order/" + id, body);
    };
    // --------------------- PRODUCTS: -------------------------
    StoreRepository.prototype.loadProducts = function () {
        return this.httpClient.get(this.getBaseUrl() + "/products");
    };
    StoreRepository.prototype.loadProductById = function (id) {
        return this.httpClient.get(this.getBaseUrl() + "/products/" + id);
    };
    StoreRepository.prototype.createProduct = function (payload) {
        var body = this.createHttParams(payload);
        return this.httpClient.post(this.getBaseUrl() + "/create_product", body);
    };
    StoreRepository.prototype.updateProduct = function (id, payload) {
        var body = this.createHttParams(payload);
        return this.httpClient.put(this.getBaseUrl() + "/update_product/" + id, body);
    };
    StoreRepository.prototype.deleteProduct = function (id) {
        return this.httpClient.delete(this.getBaseUrl() + "/delete_product/" + id);
    };
    // ---------------- PRODUCT VARIATIONS: ---------------------
    StoreRepository.prototype.loadProductsVariations = function (pagination) {
        if (pagination === void 0) { pagination = ''; }
        return this.httpClient.get(this.getBaseUrl() + "/product_variations" + pagination);
    };
    StoreRepository.prototype.loadProductVariationByVariationId = function (id) {
        return this.httpClient.get(this.getBaseUrl() + "/product_variations/" + id);
    };
    StoreRepository.prototype.createProductVariation = function (payload) {
        var body = this.createHttParams(payload);
        return this.httpClient.post(this.getBaseUrl() + "/create_product_variation", body);
    };
    StoreRepository.prototype.deleteProductVariation = function (id) {
        return this.httpClient.delete(this.getBaseUrl() + "/delete_product_variation/" + id);
    };
    // --------------------- IMAGES: -------------------------
    StoreRepository.prototype.loadSiteImages = function (pagination) {
        if (pagination === void 0) { pagination = ''; }
        return this.httpClient.get(this.getBaseUrl() + "/site_images" + pagination);
    };
    StoreRepository.prototype.loadSiteImageById = function (id) {
        return this.httpClient.get(this.getBaseUrl() + "/site_images/" + id);
    };
    StoreRepository.prototype.createSiteImage = function (payload) {
        var body = this.createHttParams(payload);
        return this.httpClient.post(this.getBaseUrl() + "/create_site_image", body);
    };
    StoreRepository.prototype.deleteSiteImage = function (id) {
        return this.httpClient.delete(this.getBaseUrl() + "/delete_site_image/" + id);
    };
    StoreRepository.prototype.createHttParams = function (payload) {
        var params = new HttpParams();
        for (var key in payload) {
            if (payload.hasOwnProperty(key)) {
                (typeof payload[key] === 'object') ?
                    params = params.append(key, JSON.stringify(payload[key]))
                    :
                        params = params.append(key, payload[key]);
            }
        }
        return params.toString();
    };
    StoreRepository.prototype.getBaseUrl = function () {
        return this.appSettings.baseUrl() + "/api/" + this.appSettings.instance() + "/v1/shop";
    };
    StoreRepository.ctorParameters = function () { return [
        { type: AbstractAppConfigService, decorators: [{ type: Inject, args: [APP_CONFIG_SERVICE,] }] },
        { type: HttpClient }
    ]; };
    StoreRepository = __decorate([
        Injectable(),
        __param(0, Inject(APP_CONFIG_SERVICE))
    ], StoreRepository);
    return StoreRepository;
}());

var STORE_SERVICE = new InjectionToken('StoreService');

var StoreService = /** @class */ (function () {
    function StoreService(repository) {
        this.repository = repository;
    }
    // --------------------- CATEGORIES: -------------------------
    StoreService.prototype.loadCategories = function () {
        return this.repository.loadCategories().pipe(map(function (response) { return response.data.map(function (c) { return CategoryModel.fromApi(c); }); }), catchError(function (error) { throw error; }));
    };
    StoreService.prototype.loadCategoryById = function (id) {
        return this.repository.loadCategoryById(id).pipe(map(function (response) { return CategoryModel.fromApi(response.data); }), catchError(function (error) { throw error; }));
    };
    StoreService.prototype.createCategory = function (payload) {
        return this.repository.createCategory(payload).pipe(map(function (response) { return CategoryModel.fromApi(response.data); }), catchError(function (error) { throw error; }));
    };
    StoreService.prototype.deleteCategory = function (payload) {
        return this.repository.deleteCategory(payload).pipe(map(function (resp) { return resp.status === 'success'; }), catchError(function (error) { throw error; }));
    };
    // --------------------- ORDERS: -------------------------
    StoreService.prototype.loadOrders = function () {
        return this.repository.loadOrders().pipe(map(function (response) { return response.data.map(function (o) { return OrderModel.fromApi(o); }); }), catchError(function (error) { throw error; }));
    };
    StoreService.prototype.loadOrderById = function (id) {
        return this.repository.loadOrderById(id).pipe(map(function (response) { return OrderModel.fromApi(response.data); }), catchError(function (error) { throw error; }));
    };
    StoreService.prototype.updateOrder = function (id, payload) {
        return this.repository.updateOrder(id, payload).pipe(map(function (response) { return OrderModel.fromApi(response.data); }), catchError(function (error) { throw error; }));
    };
    // --------------------- PRODUCTS: -------------------------
    StoreService.prototype.loadProducts = function () {
        return this.repository.loadProducts().pipe(map(function (response) { return response.data.map(function (p) { return ProductModel.fromApi(p); }); }), catchError(function (error) { throw error; }));
    };
    StoreService.prototype.loadProductById = function (id) {
        return this.repository.loadProductById(id).pipe(map(function (response) { return ProductModel.fromApi(response.data); }), catchError(function (error) { throw error; }));
    };
    StoreService.prototype.createProduct = function (payload) {
        return this.repository.createProduct(payload).pipe(map(function (response) { return ProductModel.fromApi(response.data); }), catchError(function (error) { throw error; }));
    };
    StoreService.prototype.updateProduct = function (id, payload) {
        return this.repository.updateProduct(id, payload).pipe(map(function (response) { return ProductModel.fromApi(response.data); }), catchError(function (error) { throw error; }));
    };
    StoreService.prototype.deleteProduct = function (id) {
        return this.repository.deleteProduct(id).pipe(map(function (resp) { return resp.status === 'success'; }), catchError(function (error) { throw error; }));
    };
    // ---------------- PRODUCT VARIATIONS: ---------------------
    StoreService.prototype.loadProductsVariations = function () {
        return this.repository.loadProductsVariations().pipe(map(function (resp) { return resp.data.map(function (v) { return VariationModel.fromApi(v); }); }), catchError(function (error) { throw error; }));
    };
    StoreService.prototype.loadProductVariationById = function (id) {
        return this.repository.loadProductVariationByVariationId(id).pipe(map(function (resp) { return VariationModel.fromApi(resp.data); }), catchError(function (error) { throw error; }));
    };
    StoreService.prototype.createProductVariation = function (payload) {
        return this.repository.createProductVariation(payload).pipe(map(function (resp) { return VariationModel.fromApi(resp.data); }), catchError(function (error) { throw error; }));
    };
    StoreService.prototype.deleteProductVariation = function (id) {
        return this.repository.deleteProductVariation(id).pipe(map(function (resp) { return resp.status === 'success'; }), catchError(function (error) { throw error; }));
    };
    // --------------------- IMAGES: -------------------------
    StoreService.prototype.loadSiteImages = function () {
        return this.repository.loadSiteImages().pipe(map(function (resp) { return resp.data.map(function (i) { return ImageModel.fromApi(i); }); }), catchError(function (error) { throw error; }));
    };
    StoreService.prototype.loadSiteImageById = function (id) {
        throw new Error('Method not implemented');
    };
    StoreService.prototype.createSiteImage = function (payload) {
        throw new Error('Method not implemented');
    };
    StoreService.prototype.deleteSiteImage = function (id) {
        throw new Error('Method not implemented');
    };
    StoreService.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [STORE_REPOSITORY,] }] }
    ]; };
    StoreService.ɵprov = ɵɵdefineInjectable({ factory: function StoreService_Factory() { return new StoreService(ɵɵinject(STORE_REPOSITORY)); }, token: StoreService, providedIn: "root" });
    StoreService = __decorate([
        Injectable({
            providedIn: 'root'
        }),
        __param(0, Inject(STORE_REPOSITORY))
    ], StoreService);
    return StoreService;
}());

var StoreImagesActionTypes;
(function (StoreImagesActionTypes) {
    StoreImagesActionTypes["LoadImagesBegin"] = "[STORE] Load Images Begin";
    StoreImagesActionTypes["LoadImagesSuccess"] = "[STORE] Load Images Success";
    StoreImagesActionTypes["LoadImagesFail"] = "[STORE] Load Images Fail";
    StoreImagesActionTypes["LoadImageByIdBegin"] = "[STORE] Load Image By Id Begin";
    StoreImagesActionTypes["LoadImageByIdSuccess"] = "[STORE] Load Image By Id Success";
    StoreImagesActionTypes["LoadImageByIdFail"] = "[STORE] Load Image By Id Fail";
    StoreImagesActionTypes["CreateImageBegin"] = "[STORE] Create Image Begin";
    StoreImagesActionTypes["CreateImageSuccess"] = "[STORE] Create Image Success";
    StoreImagesActionTypes["CreateImageFail"] = "[STORE] Create Image Fail";
    StoreImagesActionTypes["DelteImageBegin"] = "[STORE] Load Delete  Begin";
    StoreImagesActionTypes["DelteImageSuccess"] = "[STORE] Load Delete Success";
    StoreImagesActionTypes["DelteImageFail"] = "[STORE] Delete Image Fail";
    StoreImagesActionTypes["ClearImagesSuccessErrorData"] = "[STORE] Clear Images Success/Error Data";
})(StoreImagesActionTypes || (StoreImagesActionTypes = {}));
var LoadImagesBeginAction = createAction(StoreImagesActionTypes.LoadImagesBegin);
var LoadImagesSuccessAction = createAction(StoreImagesActionTypes.LoadImagesSuccess, props());
var LoadImagesFailAction = createAction(StoreImagesActionTypes.LoadImagesFail, props());
var LoadImageByIdBeginAction = createAction(StoreImagesActionTypes.LoadImageByIdBegin, props());
var LoadImageByIdSuccessAction = createAction(StoreImagesActionTypes.LoadImageByIdSuccess, props());
var LoadImageByIdFailAction = createAction(StoreImagesActionTypes.LoadImageByIdFail, props());
var CreateImageBeginAction = createAction(StoreImagesActionTypes.CreateImageBegin, props());
var CreateImageSuccessAction = createAction(StoreImagesActionTypes.CreateImageSuccess, props());
var CreateImageFailAction = createAction(StoreImagesActionTypes.CreateImageFail, props());
var DeleteImageBeginAction = createAction(StoreImagesActionTypes.DelteImageBegin, props());
var DeleteImageSuccessAction = createAction(StoreImagesActionTypes.DelteImageSuccess, props());
var DeleteImageFailAction = createAction(StoreImagesActionTypes.DelteImageFail, props());
var ClearImagesSuccessErrorDataAction = createAction(StoreImagesActionTypes.ClearImagesSuccessErrorData);

var StoreOrdersActionTypes;
(function (StoreOrdersActionTypes) {
    StoreOrdersActionTypes["LoadOrdersBegin"] = "[STORE] Load Orders Begin";
    StoreOrdersActionTypes["LoadOrdersSuccess"] = "[STORE] Load Orders Success";
    StoreOrdersActionTypes["LoadOrdersFail"] = "[STORE] Load Orders Fail";
    StoreOrdersActionTypes["LoadOrderByIdBegin"] = "[STORE] Load Order By Id Begin";
    StoreOrdersActionTypes["LoadOrderByIdSuccess"] = "[STORE] Load Order By Id Success";
    StoreOrdersActionTypes["LoadOrderByIdFail"] = "[STORE] Load Order By Id Fail";
    StoreOrdersActionTypes["UpdateOrderBegin"] = "[STORE] Update Order Begin";
    StoreOrdersActionTypes["UpdateOrderSuccess"] = "[STORE] Update Order Success";
    StoreOrdersActionTypes["UpdateOrderFail"] = "[STORE] Update Order Fail";
    StoreOrdersActionTypes["ClearOrdersSuccessErrorData"] = "[STORE] Clear Orders Success/Error Data";
})(StoreOrdersActionTypes || (StoreOrdersActionTypes = {}));
var LoadOrdersBeginAction = createAction(StoreOrdersActionTypes.LoadOrdersBegin);
var LoadOrdersSuccessAction = createAction(StoreOrdersActionTypes.LoadOrdersSuccess, props());
var LoadOrdersFailAction = createAction(StoreOrdersActionTypes.LoadOrdersFail, props());
var LoadOrderByIdBeginAction = createAction(StoreOrdersActionTypes.LoadOrderByIdBegin, props());
var LoadOrderByIdSuccessAction = createAction(StoreOrdersActionTypes.LoadOrderByIdSuccess, props());
var LoadOrderByIdFailAction = createAction(StoreOrdersActionTypes.LoadOrderByIdFail, props());
var UpdateOrderBeginAction = createAction(StoreOrdersActionTypes.UpdateOrderBegin, props());
var UpdateOrderSuccessAction = createAction(StoreOrdersActionTypes.UpdateOrderSuccess, props());
var UpdateOrderFailAction = createAction(StoreOrdersActionTypes.UpdateOrderFail, props());
var ClearOrdersSuccessErrorDataAction = createAction(StoreOrdersActionTypes.ClearOrdersSuccessErrorData);

var StoreProductsActionTypes;
(function (StoreProductsActionTypes) {
    StoreProductsActionTypes["LoadProductsBegin"] = "[STORE] Load Products Begin";
    StoreProductsActionTypes["LoadProductsSuccess"] = "[STORE] Load Products Success";
    StoreProductsActionTypes["LoadProductsFail"] = "[STORE] Load Products Fail";
    StoreProductsActionTypes["LoadProductByIdBegin"] = "[STORE] Load Product By Id Begin";
    StoreProductsActionTypes["LoadProductByIdSuccess"] = "[STORE] Load Product By Id Success";
    StoreProductsActionTypes["LoadProductByIdFail"] = "[STORE] Load Product By Id Fail";
    StoreProductsActionTypes["CreateProductBegin"] = "[STORE] Create Product Begin";
    StoreProductsActionTypes["CreateProductSuccess"] = "[STORE] Create Product Success";
    StoreProductsActionTypes["CreateProductFail"] = "[STORE] Create Product Fail";
    StoreProductsActionTypes["UpdateProductBegin"] = "[STORE] Update Product Begin";
    StoreProductsActionTypes["UpdateProductSuccess"] = "[STORE] Update Product Success";
    StoreProductsActionTypes["UpdateProductFail"] = "[STORE] Update Product Fail";
    StoreProductsActionTypes["DeleteProductBegin"] = "[STORE] Delete Product Begin";
    StoreProductsActionTypes["DeleteProductSuccess"] = "[STORE] Delete Product Success";
    StoreProductsActionTypes["DeleteProductFail"] = "[STORE] Delete Product Fail";
    StoreProductsActionTypes["ClearProductsSuccessErrorData"] = "[STORE] Clear Products Success/Error Data";
})(StoreProductsActionTypes || (StoreProductsActionTypes = {}));
var LoadProductsBeginAction = createAction(StoreProductsActionTypes.LoadProductsBegin);
var LoadProductsSuccessAction = createAction(StoreProductsActionTypes.LoadProductsSuccess, props());
var LoadProductsFailAction = createAction(StoreProductsActionTypes.LoadProductsFail, props());
var LoadProductByIdBeginAction = createAction(StoreProductsActionTypes.LoadProductByIdBegin, props());
var LoadProductByIdSuccessAction = createAction(StoreProductsActionTypes.LoadProductByIdSuccess, props());
var LoadProductByIdFailAction = createAction(StoreProductsActionTypes.LoadProductByIdFail, props());
var CreateProductBeginAction = createAction(StoreProductsActionTypes.CreateProductBegin, props());
var CreateProductSuccessAction = createAction(StoreProductsActionTypes.CreateProductSuccess, props());
var CreateProductFailAction = createAction(StoreProductsActionTypes.CreateProductFail, props());
var UpdateProductBeginAction = createAction(StoreProductsActionTypes.UpdateProductBegin, props());
var UpdateProductSuccessAction = createAction(StoreProductsActionTypes.UpdateProductSuccess, props());
var UpdateProductFailAction = createAction(StoreProductsActionTypes.UpdateProductFail, props());
var DeleteProductBeginAction = createAction(StoreProductsActionTypes.DeleteProductBegin, props());
var DeleteProductSuccessAction = createAction(StoreProductsActionTypes.DeleteProductSuccess, props());
var DeleteProductFailAction = createAction(StoreProductsActionTypes.DeleteProductFail, props());
var ClearProductsSuccessErrorDataAction = createAction(StoreProductsActionTypes.ClearProductsSuccessErrorData);

var StoreVariationsActionTypes;
(function (StoreVariationsActionTypes) {
    StoreVariationsActionTypes["LoadVariationsBegin"] = "[STORE] Load Variations Begin";
    StoreVariationsActionTypes["LoadVariationsSuccess"] = "[STORE] Load Variations Success";
    StoreVariationsActionTypes["LoadVariationsFail"] = "[STORE] Load Variations Fail";
    StoreVariationsActionTypes["LoadVariationByIdBegin"] = "[STORE] Load Variation By Id Begin";
    StoreVariationsActionTypes["LoadVariationByIdSuccess"] = "[STORE] Load Variation By Id Success";
    StoreVariationsActionTypes["LoadVariationByIdFail"] = "[STORE] Load Variation By Id Fail";
    StoreVariationsActionTypes["CreateVariationBegin"] = "[STORE] Create Variation Begin";
    StoreVariationsActionTypes["CreateVariationSuccess"] = "[STORE] Create Variation Success";
    StoreVariationsActionTypes["CreateVariationFail"] = "[STORE] Create Variation Fail";
    StoreVariationsActionTypes["DeleteVariationBegin"] = "[STORE] Delete Variation Begin";
    StoreVariationsActionTypes["DeleteVariationSuccess"] = "[STORE] Delete Variation Success";
    StoreVariationsActionTypes["DeleteVariationFail"] = "[STORE] Delete Variation Fail";
    StoreVariationsActionTypes["ClearVariationsSuccessErrorData"] = "[STORE] Clear Variations Success/Error Data";
})(StoreVariationsActionTypes || (StoreVariationsActionTypes = {}));
var LoadVariationsBeginAction = createAction(StoreVariationsActionTypes.LoadVariationsBegin);
var LoadVariationsSuccessAction = createAction(StoreVariationsActionTypes.LoadVariationsSuccess, props());
var LoadVariationsFailAction = createAction(StoreVariationsActionTypes.LoadVariationsFail, props());
var LoadVariationByIdBeginAction = createAction(StoreVariationsActionTypes.LoadVariationByIdBegin, props());
var LoadVariationByIdSuccessAction = createAction(StoreVariationsActionTypes.LoadVariationByIdSuccess, props());
var LoadVariationByIdFailAction = createAction(StoreVariationsActionTypes.LoadVariationByIdFail, props());
var CreteVariationBeginAction = createAction(StoreVariationsActionTypes.CreateVariationBegin, props());
var CreteVariationSuccessAction = createAction(StoreVariationsActionTypes.CreateVariationSuccess, props());
var CreteVariataionFailAction = createAction(StoreVariationsActionTypes.CreateVariationFail, props());
var DelteVariationBeginAction = createAction(StoreVariationsActionTypes.DeleteVariationBegin, props());
var DelteVariationSuccessAction = createAction(StoreVariationsActionTypes.DeleteVariationSuccess, props());
var DelteVariationFailAction = createAction(StoreVariationsActionTypes.DeleteVariationFail, props());
var ClearVariationsSuccessErrorDataAction = createAction(StoreVariationsActionTypes.ClearVariationsSuccessErrorData);

var StoreCategoriesActionTypes;
(function (StoreCategoriesActionTypes) {
    StoreCategoriesActionTypes["LoadCategoriesBegin"] = "[STORE] Load Categories Begin";
    StoreCategoriesActionTypes["LoadCategoriesSuccess"] = "[STORE] Load Categories Success";
    StoreCategoriesActionTypes["LoadCategoriesFail"] = "[STORE] Load Categories Fail";
    StoreCategoriesActionTypes["LoadCategoryByIdBegin"] = "[STORE] Load LoadCategory By Id Begin";
    StoreCategoriesActionTypes["LoadCategoryByIdSuccess"] = "[STORE] Load Categorie By Id Success";
    StoreCategoriesActionTypes["LoadCategoryByIdFail"] = "[STORE] Load Categorie By Id Fail";
    StoreCategoriesActionTypes["CreateCategoryBegin"] = "[STORE] Create Category Begin";
    StoreCategoriesActionTypes["CreateCategorySuccess"] = "[STORE] Create Category Success";
    StoreCategoriesActionTypes["CreateCategoryFail"] = "[STORE] Create Category Fail";
    StoreCategoriesActionTypes["DeleteCategoryBegin"] = "[STORE] Delete Category Begin";
    StoreCategoriesActionTypes["DeleteCategorySuccess"] = "[STORE] Delete Category Success";
    StoreCategoriesActionTypes["DeleteCategoryFail"] = "[STORE] Delete Category Fail";
    StoreCategoriesActionTypes["ClearCategoriesSuccessErrorData"] = "[STORE] Clear Categories Success/Error Data";
})(StoreCategoriesActionTypes || (StoreCategoriesActionTypes = {}));
var LoadCategoriesBeginAction = createAction(StoreCategoriesActionTypes.LoadCategoriesBegin);
var LoadCategoriesSuccessAction = createAction(StoreCategoriesActionTypes.LoadCategoriesSuccess, props());
var LoadCategoriesFailAction = createAction(StoreCategoriesActionTypes.LoadCategoriesFail, props());
var LoadCategorieByIdBeginAction = createAction(StoreCategoriesActionTypes.LoadCategoryByIdBegin, props());
var LoadCategorieByIdSuccessAction = createAction(StoreCategoriesActionTypes.LoadCategoryByIdSuccess, props());
var LoadCategorieByIdFailAction = createAction(StoreCategoriesActionTypes.LoadCategoryByIdFail, props());
var CreateCategoryBeginAction = createAction(StoreCategoriesActionTypes.CreateCategoryBegin, props());
var CreateCategorySuccessAction = createAction(StoreCategoriesActionTypes.CreateCategorySuccess, props());
var CreateCategoryFailAction = createAction(StoreCategoriesActionTypes.CreateCategoryFail, props());
var DeleteCategoryBeginAction = createAction(StoreCategoriesActionTypes.DeleteCategoryBegin, props());
var DeleteCategorySuccessAction = createAction(StoreCategoriesActionTypes.DeleteCategorySuccess, props());
var DeleteCategoryFailAction = createAction(StoreCategoriesActionTypes.DeleteCategoryFail, props());
var ClearCategoriesSuccessErrorDataAction = createAction(StoreCategoriesActionTypes.ClearCategoriesSuccessErrorData);

var getStoreState = createFeatureSelector('store');
var ɵ0 = function (state) { return state; };
// ------- STORE GLOBAL STATE:
var getStorePageState = createSelector(getStoreState, ɵ0);
var ɵ1 = function (state) { return state.categories; };
// --------------------- CATEGORIES: -------------------------
var getStoreCategoriesState = createSelector(getStorePageState, ɵ1);
var ɵ2 = function (categories) { return categories.isLoading; };
var getCategoriesIsloading = createSelector(getStoreCategoriesState, ɵ2);
var ɵ3 = function (categories) { return categories.data; };
var getCategoriesData = createSelector(getStoreCategoriesState, ɵ3);
var ɵ4 = function (categories) { return categories.hasBeenFetched; };
var getCategoriesHasBeenFetched = createSelector(getStoreCategoriesState, ɵ4);
var ɵ5 = function (categories) { return categories.error; };
var getCategoriesError = createSelector(getStoreCategoriesState, ɵ5);
var ɵ6 = function (categories) { return categories.success; };
var getCategoriesSuccess = createSelector(getStoreCategoriesState, ɵ6);
var ɵ7 = function (state) { return state.orders; };
// --------------------- ORDERS: -------------------------
var getStoreOrdersState = createSelector(getStorePageState, ɵ7);
var ɵ8 = function (orders) { return orders.isLoading; };
var getOrdersIsLoading = createSelector(getStoreOrdersState, ɵ8);
var ɵ9 = function (orders) { return orders.data; };
var getOrdersData = createSelector(getStoreOrdersState, ɵ9);
var ɵ10 = function (orders) { return orders.hasBeenFetched; };
var getOrdersHasBeenFetched = createSelector(getStoreOrdersState, ɵ10);
var ɵ11 = function (orders) { return orders.error; };
var getOrdersError = createSelector(getStoreOrdersState, ɵ11);
var ɵ12 = function (orders) { return orders.success; };
var getOrdersSuccess = createSelector(getStoreOrdersState, ɵ12);
var ɵ13 = function (state) { return state.products; };
// --------------------- PRODUCTS: -------------------------
var getStoreProductsState = createSelector(getStorePageState, ɵ13);
var ɵ14 = function (products) { return products.isLoading; };
var getStoreProductsIsLoading = createSelector(getStoreProductsState, ɵ14);
var ɵ15 = function (products) { return products.data; };
var getStoreProductsData = createSelector(getStoreProductsState, ɵ15);
var ɵ16 = function (products) { return products.hasBeenFetched; };
var getStoreProductsHasBeenFetched = createSelector(getStoreProductsState, ɵ16);
var ɵ17 = function (products) { return products.error; };
var getStoreProductsError = createSelector(getStoreProductsState, ɵ17);
var ɵ18 = function (products) { return products.success; };
var getStoreProductsSuccess = createSelector(getStoreProductsState, ɵ18);
var ɵ19 = function (state) { return state.variations; };
// --------------------- VARIATIONS: -------------------------
var getStoreVariationsState = createSelector(getStorePageState, ɵ19);
var ɵ20 = function (variations) { return variations.isLoading; };
var getStoreVariationsIsLoading = createSelector(getStoreVariationsState, ɵ20);
var ɵ21 = function (variations) { return variations.data; };
var getStoreVariationsData = createSelector(getStoreVariationsState, ɵ21);
var ɵ22 = function (variations) { return variations.hasBeenFetched; };
var getStoreVariationsHasBeenFetched = createSelector(getStoreVariationsState, ɵ22);
var ɵ23 = function (variations) { return variations.error; };
var getStoreVariationsError = createSelector(getStoreVariationsState, ɵ23);
var ɵ24 = function (variations) { return variations.success; };
var getStoreVariationsSuccess = createSelector(getStoreVariationsState, ɵ24);
var ɵ25 = function (state) { return state.images; };
// --------------------- IMAGES: -------------------------
var getStoreImagesState = createSelector(getStorePageState, ɵ25);
var ɵ26 = function (images) { return images.isLoading; };
var getStoreImagesIsLoading = createSelector(getStoreImagesState, ɵ26);
var ɵ27 = function (images) { return images.data; };
var getStoreImagesData = createSelector(getStoreImagesState, ɵ27);
var ɵ28 = function (images) { return images.hasBeenFetched; };
var getStoreImagesHasBeenFetched = createSelector(getStoreImagesState, ɵ28);
var ɵ29 = function (images) { return images.error; };
var getStoreImagesError = createSelector(getStoreImagesState, ɵ29);
var ɵ30 = function (images) { return images.success; };
var getStoreImagesSuccess = createSelector(getStoreImagesState, ɵ30);

var StorePageStore = /** @class */ (function () {
    function StorePageStore(store) {
        var _this = this;
        this.store = store;
        // ----------- CATEGORIES METHODS:
        this.LoadCategories = function () { return _this.store.dispatch(LoadCategoriesBeginAction()); };
        this.loadCategoryById = function (id) { return _this.store.dispatch(LoadCategorieByIdBeginAction({ id: id })); };
        this.CreateCategory = function (payload) { return _this.store.dispatch(CreateCategoryBeginAction({ payload: payload })); };
        this.DeleteCategory = function (id) { return _this.store.dispatch(DeleteCategoryBeginAction({ id: id })); };
        // ----------- ORDERS METHODS:
        this.LoadOrders = function () { return _this.store.dispatch(LoadOrdersBeginAction()); };
        this.loadOrderById = function (id) { return _this.store.dispatch(LoadOrderByIdBeginAction({ id: id })); };
        this.updateOrder = function (id, payload) { return _this.store.dispatch(UpdateOrderBeginAction({ id: id, payload: payload })); };
        // ----------- PRODUCTS METHODS:
        this.LoadProducts = function () { return _this.store.dispatch(LoadProductsBeginAction()); };
        this.CreateProduct = function (payload) { return _this.store.dispatch(CreateProductBeginAction({ payload: payload })); };
        this.UpdateProduct = function (id, payload) { return _this.store.dispatch(UpdateProductBeginAction({ id: id, payload: payload })); };
        this.DeleteProduct = function (id) { return _this.store.dispatch(DeleteProductBeginAction({ id: id })); };
        // ----------- VARIATIONS METHODS:
        this.LoadVariations = function () { return _this.store.dispatch(LoadVariationsBeginAction()); };
        this.CreateVariation = function (payload) { return _this.store.dispatch(CreteVariationBeginAction({ payload: payload })); };
        this.DeleteVariation = function (payload) { return _this.store.dispatch(DelteVariationBeginAction({ payload: payload })); };
        // ----------- IMAGE METHODS:
        this.LoadImages = function () { return _this.store.dispatch(LoadImagesBeginAction()); };
        this.LoadImageById = function (id) { return _this.store.dispatch(LoadImageByIdBeginAction({ id: id })); };
        this.CreateImage = function (payload) { return _this.store.dispatch(CreateImageBeginAction({ payload: payload })); };
        this.DeleteImage = function (id) { return _this.store.dispatch(DeleteImageBeginAction({ id: id })); };
    }
    Object.defineProperty(StorePageStore.prototype, "IsLoadingCategories$", {
        get: function () { return this.store.select(getCategoriesIsloading); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StorePageStore.prototype, "CategoriesData$", {
        get: function () { return this.store.select(getCategoriesData); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StorePageStore.prototype, "CategoriesHasBeenFetched$", {
        get: function () { return this.store.select(getCategoriesHasBeenFetched); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StorePageStore.prototype, "CategoriesError$", {
        get: function () { return this.store.select(getCategoriesError); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StorePageStore.prototype, "CategoriesSuccess$", {
        get: function () { return this.store.select(getCategoriesSuccess); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StorePageStore.prototype, "IsLoadingOrders$", {
        get: function () { return this.store.select(getOrdersIsLoading); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StorePageStore.prototype, "OrdersData$", {
        get: function () { return this.store.select(getOrdersData); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StorePageStore.prototype, "OrdersHasBeenFetched$", {
        get: function () { return this.store.select(getOrdersHasBeenFetched); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StorePageStore.prototype, "OrdersError$", {
        get: function () { return this.store.select(getOrdersError); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StorePageStore.prototype, "OrdersSuccess$", {
        get: function () { return this.store.select(getOrdersSuccess); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StorePageStore.prototype, "IsLoadingProducts$", {
        get: function () { return this.store.select(getStoreProductsIsLoading); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StorePageStore.prototype, "ProductsData$", {
        get: function () { return this.store.select(getStoreProductsData); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StorePageStore.prototype, "ProductsHasBeenFetched$", {
        get: function () { return this.store.select(getStoreProductsHasBeenFetched); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StorePageStore.prototype, "ProductsError$", {
        get: function () { return this.store.select(getStoreProductsError); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StorePageStore.prototype, "ProductsSuccess$", {
        get: function () { return this.store.select(getStoreProductsSuccess); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StorePageStore.prototype, "IsLoadingVariations$", {
        get: function () { return this.store.select(getStoreVariationsIsLoading); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StorePageStore.prototype, "VariationsData$", {
        get: function () { return this.store.select(getStoreVariationsData); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StorePageStore.prototype, "VariationsHasBeenFetched$", {
        get: function () { return this.store.select(getStoreVariationsHasBeenFetched); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StorePageStore.prototype, "VariationsError$", {
        get: function () { return this.store.select(getStoreVariationsError); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StorePageStore.prototype, "VariationsSuccess$", {
        get: function () { return this.store.select(getStoreVariationsSuccess); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StorePageStore.prototype, "IsLoadingImages$", {
        get: function () { return this.store.select(getStoreImagesIsLoading); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StorePageStore.prototype, "ImagesData$", {
        get: function () { return this.store.select(getStoreImagesData); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StorePageStore.prototype, "ImagesHasBeenFetched$", {
        get: function () { return this.store.select(getStoreImagesHasBeenFetched); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StorePageStore.prototype, "ImagesError$", {
        get: function () { return this.store.select(getStoreImagesError); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StorePageStore.prototype, "ImagesSuccess$", {
        get: function () { return this.store.select(getStoreImagesSuccess); },
        enumerable: true,
        configurable: true
    });
    StorePageStore.ctorParameters = function () { return [
        { type: Store }
    ]; };
    StorePageStore = __decorate([
        Injectable()
    ], StorePageStore);
    return StorePageStore;
}());

var StoreEffects = /** @class */ (function () {
    function StoreEffects(actions, storePageStore, service) {
        var _this = this;
        this.actions = actions;
        this.storePageStore = storePageStore;
        this.service = service;
        // --------------------------- CATEGORIES: ---------------------------------
        this.LoadCategories$ = createEffect(function () { return _this.actions.pipe(ofType(LoadCategoriesBeginAction), switchMap(function () {
            return _this.service.loadCategories().pipe(flatMap(function (payload) { return [
                LoadCategoriesSuccessAction({ payload: payload }),
                ClearCategoriesSuccessErrorDataAction()
            ]; }), catchError(function (errors) {
                console.error('StoreEffects.LoadCategories$ ERROR', errors);
                return of([
                    LoadCategoriesFailAction({ errors: errors }),
                    ClearCategoriesSuccessErrorDataAction()
                ]).pipe(switchMap(function (d) { return d; }));
            }));
        })); });
        this.LoadCategoryById$ = createEffect(function () { return _this.actions.pipe(ofType(LoadCategorieByIdBeginAction), switchMap(function (action) {
            return _this.service.loadCategoryById(action.id).pipe(flatMap(function (payload) { return [
                LoadCategorieByIdSuccessAction({ payload: payload }),
                ClearCategoriesSuccessErrorDataAction()
            ]; }), catchError(function (errors) {
                console.error('StoreEffects.LoadCategoryById$ ERROR', errors);
                return of([
                    LoadCategorieByIdFailAction({ errors: errors }),
                    ClearCategoriesSuccessErrorDataAction()
                ]).pipe(switchMap(function (d) { return d; }));
            }));
        })); });
        this.CreateCategory$ = createEffect(function () { return _this.actions.pipe(ofType(CreateCategoryBeginAction), withLatestFrom(_this.storePageStore.CategoriesData$), switchMap(function (_a) {
            var _b = __read(_a, 2), action = _b[0], categoriesData = _b[1];
            return _this.service.createCategory(action.payload).pipe(flatMap(function (data) {
                var payload = __spread([data], categoriesData);
                return [
                    CreateCategorySuccessAction({ payload: payload }),
                    ClearCategoriesSuccessErrorDataAction()
                ];
            }), catchError(function (errors) {
                console.error('StoreEffects.CreateCategory$ ERROR', errors);
                return of([
                    CreateCategoryFailAction({ errors: errors }),
                    ClearCategoriesSuccessErrorDataAction()
                ]).pipe(switchMap(function (d) { return d; }));
            }));
        })); });
        this.DeleteCategory$ = createEffect(function () { return _this.actions.pipe(ofType(DeleteCategoryBeginAction), withLatestFrom(_this.storePageStore.CategoriesData$), switchMap(function (_a) {
            var _b = __read(_a, 2), action = _b[0], categoriesData = _b[1];
            return _this.service.deleteCategory(action.id).pipe(flatMap(function () {
                var payload = categoriesData.filter(function (c) { return c.id !== action.id; });
                return [
                    DeleteCategorySuccessAction({ payload: payload }),
                    ClearCategoriesSuccessErrorDataAction()
                ];
            }), catchError(function (errors) {
                console.error('StoreEffects.DeleteCategory$ ERROR', errors);
                return of([
                    DeleteCategoryFailAction({ errors: errors }),
                    ClearCategoriesSuccessErrorDataAction()
                ]).pipe(switchMap(function (d) { return d; }));
            }));
        })); });
        // --------------------------- ORDERS: ---------------------------------
        this.LoadOrders$ = createEffect(function () { return _this.actions.pipe(ofType(LoadOrdersBeginAction), switchMap(function () {
            return _this.service.loadOrders().pipe(flatMap(function (data) { return [
                LoadOrdersSuccessAction({ payload: data }),
                ClearOrdersSuccessErrorDataAction()
            ]; }), catchError(function (errors) {
                console.error('StoreEffects.LoadOrders$ ERROR', errors);
                return of([
                    LoadOrdersFailAction({ errors: errors }),
                    ClearOrdersSuccessErrorDataAction()
                ]).pipe(switchMap(function (d) { return d; }));
            }));
        })); });
        this.LoadOrderById$ = createEffect(function () { return _this.actions.pipe(ofType(LoadOrderByIdBeginAction), switchMap(function (action) {
            return _this.service.loadOrderById(action.id).pipe(flatMap(function (payload) { return [
                LoadOrderByIdSuccessAction({ payload: payload }),
                ClearOrdersSuccessErrorDataAction()
            ]; }), catchError(function (errors) {
                console.error('StoreEffects.LoadOrderById$ ERROR', errors);
                return of([
                    LoadOrderByIdFailAction({ errors: errors }),
                    ClearOrdersSuccessErrorDataAction()
                ]).pipe(switchMap(function (d) { return d; }));
            }));
        })); });
        this.UpdateOrderById$ = createEffect(function () { return _this.actions.pipe(ofType(UpdateOrderBeginAction), withLatestFrom(_this.storePageStore.OrdersData$), switchMap(function (_a) {
            var _b = __read(_a, 2), action = _b[0], ordersData = _b[1];
            return _this.service.updateOrder(action.id, action.payload).pipe(flatMap(function (data) {
                var payload = __spread([data], ordersData.filter(function (o) { return o.id === action.id; }));
                return [
                    UpdateOrderSuccessAction({ payload: payload }),
                    ClearOrdersSuccessErrorDataAction()
                ];
            }), catchError(function (errors) {
                console.error('StoreEffects.UpdateOrderById$ ERROR', errors);
                return of([
                    UpdateOrderFailAction({ errors: errors }),
                    ClearOrdersSuccessErrorDataAction()
                ]).pipe(switchMap(function (d) { return d; }));
            }));
        })); });
        // --------------------------- PRODUCTS: ---------------------------------
        this.LoadProducts$ = createEffect(function () { return _this.actions.pipe(ofType(LoadProductsBeginAction), switchMap(function () {
            return _this.service.loadProducts().pipe(flatMap(function (payload) { return [
                LoadProductsSuccessAction({ payload: payload }),
                ClearProductsSuccessErrorDataAction()
            ]; }), catchError(function (errors) {
                console.error('StoreEffects.LoadProducts$ ERROR', errors);
                return of([
                    LoadProductsFailAction({ errors: errors }),
                    ClearProductsSuccessErrorDataAction()
                ]).pipe(switchMap(function (d) { return d; }));
            }));
        })); });
        this.LoadProductById$ = createEffect(function () { return _this.actions.pipe(ofType(LoadProductByIdBeginAction), switchMap(function (action) {
            return _this.service.loadProductById(action.id).pipe(flatMap(function (payload) { return [
                LoadProductByIdSuccessAction({ payload: payload }),
                ClearProductsSuccessErrorDataAction()
            ]; }), catchError(function (errors) {
                console.error('StoreEffects.LoadProductById$ ERROR', errors);
                return of([
                    LoadProductByIdFailAction({ errors: errors }),
                    ClearProductsSuccessErrorDataAction()
                ]).pipe(switchMap(function (d) { return d; }));
            }));
        })); });
        this.CreateProduct$ = createEffect(function () { return _this.actions.pipe(ofType(CreateProductBeginAction), withLatestFrom(_this.storePageStore.ProductsData$), switchMap(function (_a) {
            var _b = __read(_a, 2), action = _b[0], productsData = _b[1];
            return _this.service.createProduct(action.payload).pipe(flatMap(function (data) {
                var payload = __spread([data], productsData);
                return [
                    CreateProductSuccessAction({ payload: payload }),
                    ClearProductsSuccessErrorDataAction()
                ];
            }), catchError(function (errors) {
                console.error('StoreEffects.CreateProducts$ ERROR', errors);
                return of([
                    CreateProductFailAction({ errors: errors }),
                    ClearProductsSuccessErrorDataAction()
                ]).pipe(switchMap(function (d) { return d; }));
            }));
        })); });
        this.UpdateProduct$ = createEffect(function () { return _this.actions.pipe(ofType(UpdateProductBeginAction), withLatestFrom(_this.storePageStore.ProductsData$), switchMap(function (_a) {
            var _b = __read(_a, 2), action = _b[0], productsData = _b[1];
            return _this.service.updateProduct(action.id, action.payload).pipe(flatMap(function (data) {
                var payload = __spread([data], productsData.filter(function (p) { return p.id !== action.id; }));
                return [
                    UpdateProductSuccessAction({ payload: payload }),
                    ClearProductsSuccessErrorDataAction()
                ];
            }), catchError(function (errors) {
                console.error('StoreEffects.UpdateProducts$ ERROR', errors);
                return of([
                    UpdateProductFailAction({ errors: errors }),
                    ClearProductsSuccessErrorDataAction()
                ]).pipe(switchMap(function (d) { return d; }));
            }));
        })); });
        this.DeleteProduct$ = createEffect(function () { return _this.actions.pipe(ofType(DeleteProductBeginAction), withLatestFrom(_this.storePageStore.ProductsData$), switchMap(function (_a) {
            var _b = __read(_a, 2), action = _b[0], productsData = _b[1];
            return _this.service.deleteProduct(action.id).pipe(flatMap(function () {
                var payload = productsData.filter(function (p) { return p.id !== action.id; });
                return [
                    DeleteProductSuccessAction({ payload: payload }),
                    ClearProductsSuccessErrorDataAction()
                ];
            }), catchError(function (errors) {
                console.error('StoreEffects.DeleteProduct$ ERROR', errors);
                return of([
                    DeleteProductFailAction({ errors: errors }),
                    ClearProductsSuccessErrorDataAction()
                ]).pipe(switchMap(function (d) { return d; }));
            }));
        })); });
        // --------------------------- PRODUCTS: ---------------------------------
        this.LoadVariations$ = createEffect(function () { return _this.actions.pipe(ofType(LoadVariationsBeginAction), switchMap(function () {
            return _this.service.loadProductsVariations().pipe(flatMap(function (data) { return [
                LoadVariationsSuccessAction({ payload: data }),
                ClearVariationsSuccessErrorDataAction()
            ]; }), catchError(function (errors) {
                console.error('StoreEffects.LoadVariations$ ERROR', errors);
                return of([
                    LoadVariationsFailAction({ errors: errors }),
                    ClearVariationsSuccessErrorDataAction()
                ]).pipe(switchMap(function (d) { return d; }));
            }));
        })); });
        this.LoadVariationById$ = createEffect(function () { return _this.actions.pipe(ofType(LoadVariationByIdBeginAction), switchMap(function (action) {
            return _this.service.loadProductVariationById(action.id).pipe(flatMap(function (payload) { return [
                LoadVariationByIdSuccessAction({ payload: payload }),
                ClearVariationsSuccessErrorDataAction()
            ]; }), catchError(function (errors) {
                console.error('StoreEffects.LoadVariationById$ ERROR', errors);
                return of([
                    LoadVariationByIdFailAction({ errors: errors }),
                    ClearVariationsSuccessErrorDataAction()
                ]).pipe(switchMap(function (d) { return d; }));
            }));
        })); });
        this.CreateVariation$ = createEffect(function () { return _this.actions.pipe(ofType(CreteVariationBeginAction), withLatestFrom(_this.storePageStore.VariationsData$), switchMap(function (_a) {
            var _b = __read(_a, 2), action = _b[0], variationsData = _b[1];
            return _this.service.createProductVariation(action.payload).pipe(flatMap(function (data) {
                var payload = __spread([data], variationsData);
                return [
                    CreteVariationSuccessAction({ payload: payload }),
                    ClearVariationsSuccessErrorDataAction()
                ];
            }), catchError(function (errors) {
                console.error('StoreEffects.CreateVariation$ ERROR', errors);
                return of([
                    CreteVariataionFailAction({ errors: errors }),
                    ClearVariationsSuccessErrorDataAction()
                ]).pipe(switchMap(function (d) { return d; }));
            }));
        })); });
        this.DeleteVariation$ = createEffect(function () { return _this.actions.pipe(ofType(DelteVariationBeginAction), withLatestFrom(_this.storePageStore.VariationsData$), switchMap(function (_a) {
            var _b = __read(_a, 2), action = _b[0], variationsData = _b[1];
            return _this.service.deleteProductVariation(action.id).pipe(flatMap(function () {
                var payload = variationsData.filter(function (v) { return v.id !== action.id; });
                return [
                    DelteVariationSuccessAction({ payload: payload }),
                    ClearVariationsSuccessErrorDataAction()
                ];
            }), catchError(function (errors) {
                console.error('StoreEffects.DeleteVariation$ ERROR', errors);
                return of([
                    DelteVariationFailAction({ errors: errors }),
                    ClearVariationsSuccessErrorDataAction()
                ]).pipe(switchMap(function (d) { return d; }));
            }));
        })); });
        // --------------------------- IMAGES: ---------------------------------
        this.LoadImages$ = createEffect(function () { return _this.actions.pipe(ofType(LoadImagesBeginAction), switchMap(function () {
            return _this.service.loadSiteImages().pipe(flatMap(function (payload) { return [
                LoadImagesSuccessAction({ payload: payload }),
                ClearImagesSuccessErrorDataAction()
            ]; }), catchError(function (errors) {
                console.error('StoreEffects.LoadImages$ ERROR', errors);
                return of([
                    LoadImagesFailAction({ errors: errors }),
                    ClearImagesSuccessErrorDataAction()
                ]).pipe(switchMap(function (d) { return d; }));
            }));
        })); });
        this.LoadImageById$ = createEffect(function () { return _this.actions.pipe(ofType(LoadImageByIdBeginAction), switchMap(function (action) {
            return _this.service.loadSiteImageById(action.id).pipe(flatMap(function (payload) { return [
                LoadImageByIdSuccessAction({ payload: payload }),
                ClearImagesSuccessErrorDataAction()
            ]; }), catchError(function (errors) {
                console.error('StoreEffects.LoadImageById$ ERROR', errors);
                return of([
                    LoadImageByIdFailAction({ errors: errors }),
                    ClearImagesSuccessErrorDataAction()
                ]).pipe(switchMap(function (d) { return d; }));
            }));
        })); });
        this.CreateImage$ = createEffect(function () { return _this.actions.pipe(ofType(CreateImageBeginAction), withLatestFrom(_this.storePageStore.ImagesData$), switchMap(function (_a) {
            var _b = __read(_a, 2), action = _b[0], imagesData = _b[1];
            return _this.service.createSiteImage(action.payload).pipe(flatMap(function (data) {
                var payload = __spread([data], imagesData);
                return [
                    CreateImageSuccessAction({ payload: payload }),
                    ClearImagesSuccessErrorDataAction()
                ];
            }), catchError(function (errors) {
                console.error('StoreEffects.CreateImage$ ERROR', errors);
                return of([
                    CreateImageFailAction({ errors: errors }),
                    ClearImagesSuccessErrorDataAction()
                ]).pipe(switchMap(function (d) { return d; }));
            }));
        })); });
        this.DeleteImage$ = createEffect(function () { return _this.actions.pipe(ofType(DeleteImageBeginAction), withLatestFrom(_this.storePageStore.ImagesData$), switchMap(function (_a) {
            var _b = __read(_a, 2), action = _b[0], imagesData = _b[1];
            return _this.service.deleteSiteImage(action.id).pipe(flatMap(function () {
                var payload = imagesData.filter(function (i) { return i.id !== action.id; });
                return [
                    DeleteImageSuccessAction({ payload: payload }),
                    ClearImagesSuccessErrorDataAction()
                ];
            }), catchError(function (errors) {
                console.error('StoreEffects.DeleteImage$ ERROR', errors);
                return of([
                    DeleteImageFailAction({ errors: errors }),
                    ClearImagesSuccessErrorDataAction()
                ]).pipe(switchMap(function (d) { return d; }));
            }));
        })); });
    }
    StoreEffects.ctorParameters = function () { return [
        { type: Actions },
        { type: StorePageStore },
        { type: undefined, decorators: [{ type: Inject, args: [STORE_SERVICE,] }] }
    ]; };
    StoreEffects = __decorate([
        Injectable(),
        __param(2, Inject(STORE_SERVICE))
    ], StoreEffects);
    return StoreEffects;
}());

var ɵ0$1 = [], ɵ1$1 = [], ɵ2$1 = [], ɵ3$1 = [], ɵ4$1 = [];
var initialState = {
    categories: {
        isLoading: false,
        data: ɵ0$1,
        selectedCategory: null,
        hasBeenFetched: false,
        error: null,
        success: null
    },
    orders: {
        isLoading: false,
        data: ɵ1$1,
        selectedOrder: null,
        hasBeenFetched: false,
        error: null,
        success: null
    },
    products: {
        isLoading: false,
        data: ɵ2$1,
        selectedProduct: null,
        hasBeenFetched: false,
        error: null,
        success: null
    },
    variations: {
        isLoading: false,
        data: ɵ3$1,
        selectedVariation: null,
        hasBeenFetched: false,
        error: null,
        success: null
    },
    images: {
        isLoading: false,
        data: ɵ4$1,
        selectedImage: null,
        hasBeenFetched: false,
        error: null,
        success: null
    }
};
var ɵ5$1 = function (state, action) { return (__assign(__assign({}, state), { categories: __assign(__assign({}, state.categories), { isLoading: true, error: null, success: null }) })); }, ɵ6$1 = function (state, action) { return (__assign(__assign({}, state), { categories: __assign(__assign({}, state.categories), { isLoading: false, hasBeenFetched: true, data: action.payload, error: null, success: { after: getStoreCategoriesActionType(action.type) } }) })); }, ɵ7$1 = function (state, action) { return (__assign(__assign({}, state), { categories: __assign(__assign({}, state.categories), { isLoading: false, hasBeenFetched: true, selectedCategory: action.payload, error: null, success: { after: getStoreCategoriesActionType(action.type) } }) })); }, ɵ8$1 = function (state, action) { return (__assign(__assign({}, state), { categories: __assign(__assign({}, state.categories), { isLoading: false, error: { after: getStoreCategoriesActionType(action.type), error: action.errors } }) })); }, ɵ9$1 = function (state) { return (__assign(__assign({}, state), { categories: __assign(__assign({}, state.categories), { error: null, success: null }) })); }, ɵ10$1 = function (state, action) { return (__assign(__assign({}, state), { orders: __assign(__assign({}, state.orders), { isLoading: true, error: null, success: null }) })); }, ɵ11$1 = function (state, action) { return (__assign(__assign({}, state), { orders: __assign(__assign({}, state.orders), { isLoading: false, hasBeenFetched: true, data: action.payload, error: null, success: { after: getStoreOrdersActionType(action.type) } }) })); }, ɵ12$1 = function (state, action) { return (__assign(__assign({}, state), { orders: __assign(__assign({}, state.orders), { isLoading: false, hasBeenFetched: true, selectedOrder: action.payload, error: null, success: { after: getStoreOrdersActionType(action.type) } }) })); }, ɵ13$1 = function (state, action) { return (__assign(__assign({}, state), { orders: __assign(__assign({}, state.orders), { isLoading: false, error: { after: getStoreOrdersActionType(action.type), error: action.errors }, success: null }) })); }, ɵ14$1 = function (state) { return (__assign(__assign({}, state), { orders: __assign(__assign({}, state.orders), { error: null, success: null }) })); }, ɵ15$1 = function (state, action) { return (__assign(__assign({}, state), { products: __assign(__assign({}, state.products), { isLoading: true, error: null, success: null }) })); }, ɵ16$1 = function (state, action) { return (__assign(__assign({}, state), { products: __assign(__assign({}, state.products), { isLoading: false, hasBeenFetched: true, data: action.payload, error: null, success: { after: getStoreProductsActionType(action.type) } }) })); }, ɵ17$1 = function (state, action) { return (__assign(__assign({}, state), { products: __assign(__assign({}, state.products), { isLoading: false, selectedProduct: action.payload, error: null, success: { after: getStoreProductsActionType(action.type) } }) })); }, ɵ18$1 = function (state, action) { return (__assign(__assign({}, state), { products: __assign(__assign({}, state.products), { isLoading: false, error: { after: getStoreProductsActionType(action.type), error: action.errors }, success: null }) })); }, ɵ19$1 = function (state) { return (__assign(__assign({}, state), { products: __assign(__assign({}, state.products), { error: null, success: null }) })); }, ɵ20$1 = function (state, action) { return (__assign(__assign({}, state), { variations: __assign(__assign({}, state.variations), { isLoading: true, error: null, success: null }) })); }, ɵ21$1 = function (state, action) { return (__assign(__assign({}, state), { variations: __assign(__assign({}, state.variations), { isLoading: false, hasBeenFetched: true, data: action.payload, error: null, success: { after: getStoreVariationsActionType(action.type) } }) })); }, ɵ22$1 = function (state, action) { return (__assign(__assign({}, state), { variations: __assign(__assign({}, state.variations), { isLoading: false, hasBeenFetched: true, selectedVariation: action.payload, error: null, success: { after: getStoreVariationsActionType(action.type) } }) })); }, ɵ23$1 = function (state, action) { return (__assign(__assign({}, state), { variations: __assign(__assign({}, state.variations), { isLoading: false, error: { after: getStoreVariationsActionType(action.type), error: action.errors }, success: null }) })); }, ɵ24$1 = function (state) { return (__assign(__assign({}, state), { variations: __assign(__assign({}, state.variations), { error: null, success: null }) })); }, ɵ25$1 = function (state, action) { return (__assign(__assign({}, state), { images: __assign(__assign({}, state.images), { isLoading: true, error: null, success: null }) })); }, ɵ26$1 = function (state, action) { return (__assign(__assign({}, state), { images: __assign(__assign({}, state.images), { isLoading: false, hasBeenFetched: true, data: action.payload, error: null, success: { after: getStoreImagesActionType(action.type) } }) })); }, ɵ27$1 = function (state, action) { return (__assign(__assign({}, state), { images: __assign(__assign({}, state.images), { isLoading: false, selectedImage: action.payload, error: null, success: { after: getStoreImagesActionType(action.type) } }) })); }, ɵ28$1 = function (state, action) { return (__assign(__assign({}, state), { images: __assign(__assign({}, state.images), { isLoading: false, error: { after: getStoreImagesActionType(action.type), error: action.errors }, success: null }) })); }, ɵ29$1 = function (state) { return (__assign(__assign({}, state), { images: __assign(__assign({}, state.images), { error: null, success: null }) })); };
var reducer = createReducer(initialState, 
// ------------------ CATEGORIES: ---------------------
on(LoadCategoriesBeginAction, LoadCategorieByIdBeginAction, CreateCategoryBeginAction, DeleteCategoryBeginAction, ɵ5$1), on(LoadCategoriesSuccessAction, CreateCategorySuccessAction, DeleteCategorySuccessAction, ɵ6$1), on(LoadCategorieByIdSuccessAction, ɵ7$1), on(LoadCategoriesFailAction, LoadCategorieByIdFailAction, CreateCategoryFailAction, DeleteCategoryFailAction, ɵ8$1), on(ClearCategoriesSuccessErrorDataAction, ɵ9$1), 
// ------------------ ORDERS: ---------------------
on(LoadOrdersBeginAction, LoadOrderByIdBeginAction, UpdateOrderBeginAction, ɵ10$1), on(LoadOrdersSuccessAction, UpdateOrderSuccessAction, ɵ11$1), on(LoadOrderByIdSuccessAction, ɵ12$1), on(LoadOrdersFailAction, LoadOrdersFailAction, UpdateOrderFailAction, ɵ13$1), on(ClearOrdersSuccessErrorDataAction, ɵ14$1), 
// ------------------ PRODUCTS: ---------------------
on(LoadProductsBeginAction, LoadProductByIdBeginAction, CreateProductBeginAction, UpdateProductBeginAction, DeleteProductBeginAction, ɵ15$1), on(LoadProductsSuccessAction, CreateProductSuccessAction, UpdateProductSuccessAction, DeleteProductSuccessAction, ɵ16$1), on(LoadProductByIdSuccessAction, ɵ17$1), on(LoadProductsFailAction, LoadProductByIdFailAction, CreateProductFailAction, UpdateProductFailAction, DeleteProductFailAction, ɵ18$1), on(ClearProductsSuccessErrorDataAction, ɵ19$1), 
// ------------------ VARIATIONS: ---------------------
on(LoadVariationsBeginAction, LoadVariationByIdBeginAction, CreteVariationBeginAction, DelteVariationBeginAction, ɵ20$1), on(LoadVariationsSuccessAction, CreteVariationSuccessAction, DelteVariationSuccessAction, ɵ21$1), on(LoadVariationByIdSuccessAction, ɵ22$1), on(LoadVariationsFailAction, LoadVariationByIdFailAction, CreteVariataionFailAction, DelteVariationFailAction, ɵ23$1), on(ClearVariationsSuccessErrorDataAction, ɵ24$1), 
// ------------------ IMAGES: ---------------------
on(LoadImagesBeginAction, LoadImageByIdBeginAction, CreateImageBeginAction, DeleteImageBeginAction, ɵ25$1), on(LoadImagesSuccessAction, CreateImageSuccessAction, DeleteImageSuccessAction, ɵ26$1), on(LoadImageByIdSuccessAction, ɵ27$1), on(LoadImagesFailAction, LoadImageByIdFailAction, CreateImageFailAction, DeleteImageFailAction, ɵ28$1), on(ClearImagesSuccessErrorDataAction, ɵ29$1));
function getStoreCategoriesActionType(type) {
    var action;
    switch (type) {
        case StoreCategoriesActionTypes.LoadCategoriesSuccess:
        case StoreCategoriesActionTypes.LoadCategoriesFail:
            action = 'LOAD_CATEGORIES';
            break;
        case StoreCategoriesActionTypes.CreateCategorySuccess:
        case StoreCategoriesActionTypes.CreateCategoryFail:
            action = 'CREATE_CATEGORY';
            break;
        case StoreCategoriesActionTypes.DeleteCategorySuccess:
        case StoreCategoriesActionTypes.DeleteCategoryFail:
            action = 'DELETE_CATEGORY';
            break;
        default:
            action = 'UNKNOWN';
    }
    return action;
}
function getStoreOrdersActionType(type) {
    var action;
    switch (type) {
        case StoreOrdersActionTypes.LoadOrdersSuccess:
        case StoreOrdersActionTypes.LoadOrdersFail:
            action = 'LOAD_ORDERS';
            break;
        case StoreOrdersActionTypes.UpdateOrderSuccess:
        case StoreOrdersActionTypes.UpdateOrderFail:
            action = 'UPDATE_ORDER';
            break;
        default:
            action = 'UNKNOWN';
    }
    return action;
}
function getStoreProductsActionType(type) {
    var action;
    switch (type) {
        case StoreProductsActionTypes.LoadProductsSuccess:
        case StoreProductsActionTypes.LoadProductsFail:
            action = 'LOAD_PRODUCTS';
            break;
        case StoreProductsActionTypes.CreateProductSuccess:
        case StoreProductsActionTypes.CreateProductFail:
            action = 'CREATE_PRODUCT';
            break;
        case StoreProductsActionTypes.UpdateProductSuccess:
        case StoreProductsActionTypes.UpdateProductFail:
            action = 'UPDATE_PRODUCT';
            break;
        case StoreProductsActionTypes.DeleteProductSuccess:
        case StoreProductsActionTypes.DeleteProductFail:
            action = 'DELETE_PRODUCT';
            break;
        default:
            action = 'UNKNOWN';
    }
    return action;
}
function getStoreVariationsActionType(type) {
    var action;
    switch (type) {
        case StoreVariationsActionTypes.LoadVariationsSuccess:
        case StoreVariationsActionTypes.LoadVariationsFail:
            action = 'LOAD_VARIATIONS';
            break;
        case StoreVariationsActionTypes.CreateVariationSuccess:
        case StoreVariationsActionTypes.CreateVariationFail:
            action = 'CREATE_VARIATION';
            break;
        case StoreVariationsActionTypes.DeleteVariationSuccess:
        case StoreVariationsActionTypes.DeleteVariationFail:
            action = 'DELETE_VARIATION';
            break;
        default:
            action = 'UNKNOWN';
    }
    return action;
}
function getStoreImagesActionType(type) {
    var action;
    switch (type) {
        case StoreImagesActionTypes.LoadImagesSuccess:
        case StoreImagesActionTypes.LoadImagesFail:
        case StoreImagesActionTypes.LoadImageByIdSuccess:
        case StoreImagesActionTypes.LoadImageByIdFail:
            action = 'LOAD_IMAGES';
            break;
        case StoreImagesActionTypes.CreateImageBegin:
        case StoreImagesActionTypes.CreateImageFail:
            action = 'CREATE_IMAGE';
            break;
        case StoreImagesActionTypes.DelteImageSuccess:
        case StoreImagesActionTypes.DelteImageFail:
            action = 'DELETE_IMAGE';
            break;
        default:
            action = 'UNKNOWN';
    }
    return action;
}
function storeReducer(state, action) {
    return reducer(state, action);
}

var StoreCoreModule = /** @class */ (function () {
    function StoreCoreModule() {
    }
    StoreCoreModule_1 = StoreCoreModule;
    StoreCoreModule.forRoot = function (config) {
        return {
            ngModule: StoreCoreModule_1,
            providers: __spread([
                { provide: STORE_SERVICE, useClass: StoreService },
                { provide: STORE_REPOSITORY, useClass: StoreRepository }
            ], config.providers, [
                StorePageStore
            ])
        };
    };
    var StoreCoreModule_1;
    StoreCoreModule = StoreCoreModule_1 = __decorate([
        NgModule({
            declarations: [],
            imports: [
                HttpClientModule,
                StoreModule.forFeature('store', storeReducer),
                EffectsModule.forFeature([StoreEffects]),
            ],
            exports: []
        })
    ], StoreCoreModule);
    return StoreCoreModule;
}());

/*
 * Public API Surface of store-core
 */

/**
 * Generated bundle index. Do not edit.
 */

export { CategoryModel, ClearImagesSuccessErrorDataAction, ClearOrdersSuccessErrorDataAction, ClearProductsSuccessErrorDataAction, ClearVariationsSuccessErrorDataAction, CreateImageBeginAction, CreateImageFailAction, CreateImageSuccessAction, CreateProductBeginAction, CreateProductFailAction, CreateProductSuccessAction, CreteVariataionFailAction, CreteVariationBeginAction, CreteVariationSuccessAction, DeleteImageBeginAction, DeleteImageFailAction, DeleteImageSuccessAction, DeleteProductBeginAction, DeleteProductFailAction, DeleteProductSuccessAction, DelteVariationBeginAction, DelteVariationFailAction, DelteVariationSuccessAction, ImageModel, LoadImageByIdBeginAction, LoadImageByIdFailAction, LoadImageByIdSuccessAction, LoadImagesBeginAction, LoadImagesFailAction, LoadImagesSuccessAction, LoadOrderByIdBeginAction, LoadOrderByIdFailAction, LoadOrderByIdSuccessAction, LoadOrdersBeginAction, LoadOrdersFailAction, LoadOrdersSuccessAction, LoadProductByIdBeginAction, LoadProductByIdFailAction, LoadProductByIdSuccessAction, LoadProductsBeginAction, LoadProductsFailAction, LoadProductsSuccessAction, LoadVariationByIdBeginAction, LoadVariationByIdFailAction, LoadVariationByIdSuccessAction, LoadVariationsBeginAction, LoadVariationsFailAction, LoadVariationsSuccessAction, OrderModel, ProductModel, ProductVariationModel, STORE_REPOSITORY, STORE_SERVICE, StoreCoreModule, StoreEffects, StoreImagesActionTypes, StoreOrdersActionTypes, StorePageStore, StoreProductsActionTypes, StoreRepository, StoreService, StoreVariationsActionTypes, UpdateOrderBeginAction, UpdateOrderFailAction, UpdateOrderSuccessAction, UpdateProductBeginAction, UpdateProductFailAction, UpdateProductSuccessAction, VariationModel, getCategoriesData, getCategoriesError, getCategoriesHasBeenFetched, getCategoriesIsloading, getCategoriesSuccess, getOrdersData, getOrdersError, getOrdersHasBeenFetched, getOrdersIsLoading, getOrdersSuccess, getStoreCategoriesState, getStoreImagesData, getStoreImagesError, getStoreImagesHasBeenFetched, getStoreImagesIsLoading, getStoreImagesState, getStoreImagesSuccess, getStoreOrdersState, getStorePageState, getStoreProductsData, getStoreProductsError, getStoreProductsHasBeenFetched, getStoreProductsIsLoading, getStoreProductsState, getStoreProductsSuccess, getStoreState, getStoreVariationsData, getStoreVariationsError, getStoreVariationsHasBeenFetched, getStoreVariationsIsLoading, getStoreVariationsState, getStoreVariationsSuccess, initialState, storeReducer, ɵ0, ɵ1, ɵ10, ɵ11, ɵ12, ɵ13, ɵ14, ɵ15, ɵ16, ɵ17, ɵ18, ɵ19, ɵ2, ɵ20, ɵ21, ɵ22, ɵ23, ɵ24, ɵ25, ɵ26, ɵ27, ɵ28, ɵ29, ɵ3, ɵ30, ɵ4, ɵ5, ɵ6, ɵ7, ɵ8, ɵ9, StoreCategoriesActionTypes as ɵa, LoadCategoriesBeginAction as ɵb, LoadCategoriesSuccessAction as ɵc, LoadCategoriesFailAction as ɵd, LoadCategorieByIdBeginAction as ɵe, LoadCategorieByIdSuccessAction as ɵf, LoadCategorieByIdFailAction as ɵg, CreateCategoryBeginAction as ɵh, CreateCategorySuccessAction as ɵi, CreateCategoryFailAction as ɵj, DeleteCategoryBeginAction as ɵk, DeleteCategorySuccessAction as ɵl, DeleteCategoryFailAction as ɵm, ClearCategoriesSuccessErrorDataAction as ɵn };
//# sourceMappingURL=boxx-store-core.js.map
